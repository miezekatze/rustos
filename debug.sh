#!/bin/sh
#

sudo ./umount

./build.sh debug || exit 1 
qemu-system-x86_64 -s img -audio pa,model=sb16 -no-reboot -m 320M -accel kvm &
gdb -ex "target remote localhost:1234" -ex "symbol-file kernel.elf"

sudo ./mount
