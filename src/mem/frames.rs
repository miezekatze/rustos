use core::arch::asm;
use crate::utils::lock::Lock;

use super::Physical;

const MEM_SIZE: usize = 4 * 1024 * 1024 * 1024; // 4 GiB
pub const FRAME_SIZE: usize = 4096;
const BITSET_ENTRIES: usize = MEM_SIZE / FRAME_SIZE;
const BITSET_BYTES: usize = BITSET_ENTRIES;

static FRAME_BITSET: Lock<[u64; BITSET_BYTES / 8]> = Lock::new([0u64; BITSET_BYTES / 8]);

#[inline(never)]
pub fn bitset_set(frame: usize, value: u8) {
    let mut lock = FRAME_BITSET.write();
    let idx = frame / 8;

    let num = &mut lock[idx];
    let byte = frame % 8;

    let mask = 0xff << (byte * 8);
    *num &= !mask; // clear byte
    *num |= (value as u64) << (byte * 8);
}

pub fn bitset_get(frame: usize) -> u8 {
    let lock = FRAME_BITSET.read();
    let idx = frame / 8;

    let num = lock[idx];
    let byte = frame % 8;

    let mask = 0xff << (byte * 8);
    ((num & mask) >> (byte * 8)) as u8
}

pub fn bitset_inc(frame: usize) {
    let val = bitset_get(frame);
    if val == 0xff {
        panic!("Frame {} reference count reached maximum!", frame);
    }
    bitset_set(frame, val + 1);
}

pub fn bitset_dec(frame: usize) {
    let val = bitset_get(frame);
    if val == 0 {
        panic!("Frame {} reference count already zero!", frame);
    }
    bitset_set(frame, val - 1);
}

pub fn set_frames_mmap(mmap_addr: *const ()) {
//    let mut lock = FRAME_BITSET.write();
    let len = 0x100000 / FRAME_SIZE / 8;
    unsafe{ asm!("rep stosq", in("rdi") &FRAME_BITSET as *const _, in("rax") 0x0101010101010101u64, in("rcx") len); }
//    drop(lock);

    let sz = unsafe{*((mmap_addr as *const u32).add(1))} - 16;
    let entr_sz = unsafe{*((mmap_addr as *const u32).add(2))};
    let ptr = unsafe {(mmap_addr as *const u32).add(4)};
    let mut off = 0;
    while off < sz {
        'x: {unsafe {
            let ptr = (ptr as *const u8).add(off as usize) as *const u32;

            let base = *(ptr as *const u64);
            let length = *(ptr.add(2) as *const u64);
            let typ = *ptr.add(4);

            if base + length > MEM_SIZE as u64 {
                break 'x;
            }

            if typ != 1 || base < 100000 {
                for i in (base .. base + length).step_by(4096) {
                    bitset_set(i as usize / FRAME_SIZE, 1);
                }
            } else {
                for i in (base .. base + length).step_by(4096) {
                    bitset_set(i as usize / FRAME_SIZE, 0);
                }
            }
        }}
        off += entr_sz;
    }
}

pub fn set_frames_kernel(elf_addr: *const ()) -> (usize, usize) {
    let num = unsafe{*((elf_addr as *const u32).add(2))};
    let entsize = unsafe{*((elf_addr as *const u32).add(3))} as usize;

    let ptr = unsafe {(elf_addr as *const u32).add(5)};
    let mut off = 0;

    let mut min = usize::MAX;
    let mut max = 0;

    for _ in 0 .. num {
        unsafe {
            let ptr = (ptr as *const u8).add(off) as *const u64;

            let base = ptr.add(2).read_unaligned() as usize;
            let size = ptr.add(4).read_unaligned() as usize;

            if base != 0 {
                min = min.min(base);
                max = max.max(base + size);
            }

            off += entsize;
        }
    }
    
    for i in (min .. max).step_by(FRAME_SIZE) {
        bitset_set(i / FRAME_SIZE, 1);
    }
    (min, max)
}

fn next_free_frame() -> usize {
    let lock = FRAME_BITSET.read();
    for (idx, v) in lock.iter().enumerate() {
        for a in 0 .. 8 {
            let mask = 0xff << (a * 8);
            if v & mask == 0 {
                return idx * 8 + a;
            }
        }
    }
    unreachable!("no free memory");
}

pub fn alloc_frame() -> Physical {
    let frame = next_free_frame();
//    writeln!(DEBUGCON.write(), "Allocating frame {:#x}, value: {}", frame, bitset_get(frame)).unwrap();
    bitset_inc(frame);
    Physical(frame * FRAME_SIZE)
}

pub fn dealloc_frame(addr: Physical) {
//    writeln!(DEBUGCON.write(), "Deallocating frame {:#x}, value: {}", addr.0 / FRAME_SIZE, bitset_get(addr.0 / FRAME_SIZE)).unwrap();
    bitset_dec(addr.0 / FRAME_SIZE);
}
