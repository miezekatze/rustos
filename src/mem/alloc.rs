use core::{alloc::{GlobalAlloc, AllocError}, mem::size_of};
use super::{paging::{alloc_page, PAGE_SIZE}, Virtual};

pub const HEAP_START: usize = 0xffff_9000_0000_0000;
const HEAP_INIT_SIZE: usize = PAGE_SIZE;

fn align_to(ptr: usize, align: usize) -> usize {
    if ptr % align == 0 {
        ptr
    } else {
        ptr / align * align + align
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Block {
    free: bool,
    block_len: usize,
    // size_next: usize,
    // size_prev: usize,
    pos_next: usize,
    pos_prev: usize,
}

impl Block {
    fn aligned_len(&self, ptr: usize, align: usize) -> usize {
        let ptr = ptr + size_of::<Block>();
        let len = self.block_len - size_of::<Block>();
        len - (align_to(ptr, align) - ptr)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Allocator {
    pub start: usize,
    pub shared: bool,
}

#[global_allocator]
pub static ALLOCATOR: Allocator = Allocator {
    start: HEAP_START,
    shared: false,
};

pub fn __init_alloc(this: &Allocator) {
    alloc_page(Virtual(this.start), this.shared, true);
    let first_hole = unsafe {&mut *(this.start as *mut Block)};
    *first_hole = Block {
        free: true,
        block_len: HEAP_INIT_SIZE,
        // size_next: 0,
        pos_next: 0,
        pos_prev: 0,
        // size_prev: 0,
    };
}

pub fn init_heap() {
    __init_alloc(&ALLOCATOR);
}

// macro_rules! insert_size {
//     ($self:expr, $len:expr, $hole_ptr:expr, $hole:expr) => {{
//         let mut ptrptr = 0;
//         let mut lock = $self.size_start.write();
//         let mut ptr = &mut *lock;
//         let mut set = false;
//         while *ptr != 0 {
//             let blk = &mut *(*ptr as *mut Block);
//             if $len < blk.block_len {
//                 $hole.size_prev = blk.size_prev;
//                 assert!(*ptr % 8 == 0);
//                 $hole.size_next = *ptr;
//                 blk.size_prev = $hole_ptr;
//                 *ptr = $hole_ptr;
//                 set = true;
//             }
//             ptrptr = *ptr;
//             ptr = &mut blk.size_next;
//             assert!(*ptr % 8 == 0);
//         }

//         if !set {
//             $hole.size_prev = ptrptr;
//             *ptr = $hole_ptr;
//         }
//         drop(lock);
//     }};
// }

macro_rules! merge {
    ($first:expr, $snd:ident, $tar:expr) => {
        if $first != 0 {
            let x = &mut *($first as *mut Block);
            x.$snd = $tar;
        }
    };
}

unsafe impl GlobalAlloc for Allocator {
    unsafe fn alloc(&self, layout: core::alloc::Layout) -> *mut u8 {
        __alloc(self, layout)
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: core::alloc::Layout) {
        __dealloc(ptr, layout)
    }
}

unsafe fn __alloc(a: &Allocator, layout: core::alloc::Layout) -> *mut u8 {
    loop {
        let mut ptr = a.start;
        while ptr != 0 {
            let hole = &mut *(ptr as *mut Block);

            assert_eq!(hole.pos_next % 8, 0);
            // assert_eq!(hole.size_next % 8, 0);
            if ! hole.free {
                ptr = hole.pos_next;
                continue;
            }

            let aligned_len = hole.aligned_len(ptr, layout.align());
            if aligned_len >= layout.size() {
                let start_ptr = ptr + size_of::<Block>();
                assert!(start_ptr % layout.align() == 0);
                hole.free = false;
                
                let mut extra_len = aligned_len - layout.size();
                if extra_len > size_of::<Block>() {
                    let new_hole_ptr = align_to(start_ptr + layout.size(), 0x8);
                    let new_hole = &mut *(new_hole_ptr as *mut Block);
                    extra_len = ptr + hole.block_len - new_hole_ptr;
                    hole.block_len = new_hole_ptr - ptr;

                    *new_hole = Block {
                        free: true,
                        block_len: extra_len,
                        // size_next: 0,
                        // size_prev: 0,
                        pos_next: hole.pos_next,
                        pos_prev: ptr,
                    };

                    if hole.pos_next != 0 {
                        let blk  = &mut *(hole.pos_next as *mut Block);
                        blk.pos_prev = new_hole_ptr;
                    }
                    hole.pos_next = new_hole_ptr;

                    // insert_size!(self, extra_len, new_hole_ptr, new_hole);
                }
                assert!(ptr + hole.block_len - start_ptr >= layout.size());
                let ptr = start_ptr as *mut u8;
                return ptr.add(ptr.align_offset(8));
            }
            ptr = hole.pos_next;
        }
        // expand
        let mut last = a.start;
        loop {
            let blk = &mut *(last as *mut Block);
            if blk.pos_next == 0 {
                break;
            } else {
                last = blk.pos_next;
            }
        }
        let last_blk = &mut *(last as *mut Block);
        let end = last + last_blk.block_len;
        assert!(end % PAGE_SIZE == 0);
        alloc_page(Virtual(end), a.shared, true);

        if last_blk.free {
            last_blk.block_len += PAGE_SIZE;
        } else {
            let new_hole = &mut *(end as *mut Block);
            *new_hole = Block {
                free: true,
                block_len: PAGE_SIZE,
                pos_next: 0,
                pos_prev: last,
            };
            last_blk.pos_next = end;
        }
    }
}

unsafe fn __dealloc(ptr: *mut u8, _layout: core::alloc::Layout) {
    let ptr = ptr as usize - size_of::<Block>();
    let blk = &mut *(ptr as *mut Block);
    blk.free = true;

    'merge_right: {
        if blk.pos_next != 0 {
            let next = &mut *(blk.pos_next as *mut Block);
            if ! next.free {
                break 'merge_right;
            }

            blk.block_len += next.block_len;

            merge!(next.pos_prev, pos_next, next.pos_next);
            merge!(next.pos_next, pos_prev, next.pos_prev);
        }
    }

    'merge_left: {
        if blk.pos_prev != 0 {
            let prev = &mut *(blk.pos_prev as *mut Block);
            if ! prev.free {
                break 'merge_left;
            }

            prev.block_len += blk.block_len;

            merge!(blk.pos_prev, pos_next, blk.pos_next);
            merge!(blk.pos_next, pos_prev, blk.pos_prev);
        }
    }
}

unsafe impl core::alloc::Allocator for Allocator {
    fn allocate(&self, layout: core::alloc::Layout) -> Result<core::ptr::NonNull<[u8]>, core::alloc::AllocError> {
        unsafe{
            let v = self.alloc(layout);
            if v.is_null() {
                Err(AllocError)
            } else {
                Ok(core::ptr::NonNull::new_unchecked(core::slice::from_raw_parts_mut(v, layout.size())))
            }
        }
    }

    unsafe fn deallocate(&self, _ptr: core::ptr::NonNull<u8>, _layout: core::alloc::Layout) {
        todo!()
    }
}
