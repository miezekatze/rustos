use core::arch::asm;

use crate::{mem::frames::{alloc_frame, dealloc_frame, FRAME_SIZE, bitset_inc}, println};

use super::{Physical, Virtual};
pub const PAGE_SIZE: usize = 4096;

#[derive(Debug, Clone, Copy)]
pub struct Page(usize);

pub fn alloc_page(addr: Virtual, shared: bool, writable: bool) {
    let mut page_table = ActivePageTable::get();
    let page = Page::from_address(addr);
    page_table.map(page, PTEntry::WRITABLE * writable as u64, shared);
}

pub fn alloc_user_page(addr: Virtual, shared: bool, writable: bool) {
    let mut page_table = ActivePageTable::get();
    let page = Page::from_address(addr);
    page_table.map(page, PTEntry::USER_ACCESSIBLE | (PTEntry::WRITABLE * writable as u64), shared);
}

pub fn dealloc_page(addr: Virtual) {
    let mut page_table = ActivePageTable::get();
    let page = Page::from_address(addr);
    page_table.unmap(page, true);
}

pub fn identity_map(addr: Physical, shared: bool) {
    let mut page_table = ActivePageTable::get();
    page_table.identity_map(addr, 0, shared);
}

pub struct ActivePageTable {
    p4: &'static mut PTable,
}

impl ActivePageTable {
    pub fn get() -> ActivePageTable {
        unsafe{ActivePageTable {
            p4: &mut *PTable::P4,
        }}
    }

    pub fn as_inactive(&self) -> InactivePageTable {
        InactivePageTable {
            p4_addr: self.translate(Virtual(self.p4 as *const _ as usize)).unwrap(),
        }
    }
    
    fn translate_page(&self, page: Page) -> Option<Physical> {
        let p3 = self.p4.next_table(page.p4_index());

        let huge_page = || {
            p3.and_then(|p3| {
                let p3_entry = &p3.entries[page.p3_index()];
                // 1GiB page?
                if let Some(start_frame) = p3_entry.get_frame() {
                    if p3_entry.0 & PTEntry::HUGE_PAGE != 0 {
                        // address must be 1GiB aligned
                        assert!(start_frame.0 % (ENTRY_COUNT * ENTRY_COUNT) == 0);
                        return Some(Physical(start_frame.0 + page.p2_index() *
                                             ENTRY_COUNT + page.p1_index()));
                    }
                }
                if let Some(p2) = p3.next_table(page.p3_index()) {
                    let p2_entry = &p2.entries[page.p2_index()];
                    // 2MiB page?
                    if let Some(start_frame) = p2_entry.get_frame() {
                        if p2_entry.0 & PTEntry::HUGE_PAGE != 0 {
                            // address must be 2MiB aligned
                            assert!(start_frame.0 % ENTRY_COUNT == 0);
                            return Some(Physical(start_frame.0 + page.p1_index()));
                        }
                    }
                }
                None
            })

        };

        p3.and_then(|p3| p3.next_table(page.p3_index()))
            .and_then(|p2| p2.next_table(page.p2_index()))
            .and_then(|p1| p1.entries[page.p1_index()].get_frame())
            .or_else(huge_page)
    }

    pub fn translate_entry(&self, virtual_address: Virtual) -> Option<PTEntry> {
        let page = Page::from_address(virtual_address);
        let p3 = self.p4.next_table(page.p4_index());

        p3.and_then(|p3| p3.next_table(page.p3_index()))
            .and_then(|p2| p2.next_table(page.p2_index()))
            .map(|p1| p1.entries[page.p1_index()])
    }

    pub fn translate(&self, virtual_address: Virtual) -> Option<Physical> {
        let offset = virtual_address.0 % PAGE_SIZE;
        self.translate_page(Page::from_address(virtual_address))
            .map(|frame| Physical(frame.0 * PAGE_SIZE + offset))
    }

    pub fn map_to_addr(&mut self, page: Page, frame_addr: Physical, flags: u64, just_return: bool, shared: bool) -> (*const usize, usize) {
        let p3 = self.p4.next_table_create(page.p4_index());
        let p2 = p3.next_table_create(page.p3_index());
        let p1 = p2.next_table_create(page.p2_index());

        if ! just_return {
            if ! p1.entries[page.p1_index()].is_unused() {
                println!("Page {:#x?} is already mapped to frame {:?}!", page, p1.entries[page.p1_index()].get_frame().unwrap());
            }
            assert!(p1.entries[page.p1_index()].is_unused());
        }
        let flags = flags | PTEntry::PRESENT | (PTEntry::SHARED * shared as u64);
        if just_return {
            return (&p1.entries[page.p1_index()].0 as *const _ as *const _, frame_addr.0 | flags as usize);
        }
        p1.entries[page.p1_index()].set(frame_addr, flags);
        unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};
//        bitset_inc(frame_addr.0 / PAGE_SIZE);
        (core::ptr::null(), 0)
    }

    pub fn map(&mut self, page: Page, flags: u64, shared: bool) {
        let frame = alloc_frame();
        self.map_to_addr(page, frame, flags, false, shared);
    }

    pub fn identity_map(&mut self, frame_addr: Physical, flags: u64, shared: bool) {
        let page = Page::from_address(Virtual(frame_addr.0));
        self.map_to_addr(page, frame_addr, flags, false, shared);
    }

    pub fn unmap(&mut self, page: Page, unmap_frame: bool) {
        if self.translate(page.start_address()).is_none() {
            println!("not mapped");
            return;
        }

        let p1 = self.p4
            .next_table_mut(page.p4_index())
            .and_then(|p3| p3.next_table_mut(page.p3_index()))
            .and_then(|p2| p2.next_table_mut(page.p2_index()))
            .expect("mapping code does not support huge pages");

        let frame = p1.entries[page.p1_index()].get_frame().unwrap();
        p1.entries[page.p1_index()].0 = 0;

        // TODO free p(1,2,3) table if empty
        unsafe{asm!("invlpg [{a}]", a = in(reg) frame.0)};
        if unmap_frame {
            dealloc_frame(Physical(frame.0 * FRAME_SIZE));
        }
    }

    pub fn with_tmp_table<F>(&mut self, table: &mut InactivePageTable, temporary_page: &mut TemporaryPage, f: F)
    where F: FnOnce(&mut ActivePageTable) {
        let mut backup: Physical = Physical(0);
        unsafe{asm!("mov {r}, cr3", r = out(reg) backup.0)};

        let p4_table = temporary_page.map_table_frame_addr(backup, self);
        
        // overwrite recursive mapping
        self.p4.entries[511].set(table.p4_addr, PTEntry::PRESENT | PTEntry::WRITABLE);
        unsafe{asm!("mov {r}, cr3", "mov cr3, {r}", r = out(reg) _)};

        // execute f in the new context
        f(self);

        // restore recursive mapping to original p4 table
        p4_table.entries[511].set(backup, PTEntry::PRESENT | PTEntry::WRITABLE);
        unsafe{asm!("mov {r}, cr3", "mov cr3, {r}", r = out(reg) _)};
    }

    pub fn switch(&mut self, new_table: InactivePageTable) -> InactivePageTable {
        unsafe {
            let addr: usize;
            asm!("mov {a}, cr3", a = out(reg) addr);
            let old_table = InactivePageTable {
                p4_addr: Physical(addr),
            };
            asm!("mov cr3, {a}", a = in(reg) new_table.p4_addr.0);
            old_table
        }
    }

    pub fn clone_table(&mut self) -> InactivePageTable {
        let frame = alloc_frame();
        let mut tmp_page = TemporaryPage::new(Page::from_address(Virtual(PAGE_BUF)));
        let new_table = InactivePageTable::new(frame, self, &mut tmp_page);

        assert_eq!(new_table.p4_addr.0, frame.0);

        self.p4.entries[510].set(new_table.p4_addr, PTEntry::PRESENT | PTEntry::WRITABLE);
        unsafe{asm!("mov {r}, cr3", "mov cr3, {r}", r = out(reg) _)};

        fn f(lvl: usize, tbl: &mut PTable, ntbl: &mut PTable, _act: &ActivePageTable) {
            // zero out the new table
            let end = if lvl == 4 { 510 } else { 512 };
            for a in 0 .. end {
                ntbl.entries[a].set_unused();
            }
            for a in 0 .. end {
                let entry = tbl.next_table(a);
                if entry.is_some() {
                    if lvl == 1 {
                        let frame = Physical(tbl.entries[a].get_frame().unwrap().0 * FRAME_SIZE);

                        let mut flags = tbl.entries[a].get_flags();
                        if (flags & PTEntry::SHARED) == 0 {
                            // copy-on-write
                            flags &= !PTEntry::WRITABLE;
                            flags |= PTEntry::COPY_ON_WRITE;

                            tbl.entries[a].set(frame, flags);
                            assert_eq!(tbl.entries[a].get_frame().unwrap().0 * FRAME_SIZE, frame.0);
                        }

                        bitset_inc(frame.0 / FRAME_SIZE);

                        ntbl.entries[a].set(frame, flags);
                        unsafe{asm!("mov {r}, cr3", "mov cr3, {r}", r = out(reg) _)};
                    } else if lvl == 4 && !(170..256).contains(&a) { // is in kernel space, just copy
                        let frame = tbl.entries[a].get_frame().unwrap();
                        let flags = tbl.entries[a].get_flags();

                        ntbl.entries[a].set(Physical(frame.0 * FRAME_SIZE), flags);
                        unsafe{asm!("mov {r}, cr3", "mov cr3, {r}", r = out(reg) _)};
                    } else {
                        let val = &mut tbl.entries[a];
                        let flags = val.get_flags();

                        let frame = alloc_frame();
                        assert_ne!(frame.0, val.get_frame().unwrap().0);

                        ntbl.entries[a].set(frame, flags);
                        unsafe{asm!("mov {r}, cr3", "mov cr3, {r}", r = out(reg) _)};

                        let at = tbl.next_table_mut(a).unwrap();
                        let bt = ntbl.next_table_mut(a).unwrap();
                        f(lvl - 1, at, bt, _act);
                    }
                }
            }
        }

        let virt_ptr = 0xffff_ffff_ffff_e000 as *mut PTable;
        unsafe{f(4, &mut *(self.p4 as *mut _),  &mut *virt_ptr, self)};

        self.p4.entries[510].set_unused();
        new_table
    }

}

#[repr(align(4096))]
pub struct PageBuf(pub [u8; PAGE_SIZE]);
pub const PAGE_BUF: usize = 0x0000_0f00_0000_0000;

impl Page {
    pub fn from_address(address: Virtual) -> Self {
        assert!(address.0 < 0x0000_8000_0000_0000 ||
                address.0 >= 0xffff_8000_0000_0000,
                "invalid address: {:#x}", address.0);
        Self(address.0 / PAGE_SIZE)
    }

    fn start_address(&self) -> Virtual {
        Virtual(self.0 * PAGE_SIZE)
    }

    fn p4_index(&self) -> usize {
        (self.0 >> 27) & 0o777
    }
    fn p3_index(&self) -> usize {
        (self.0 >> 18) & 0o777
    }
    fn p2_index(&self) -> usize {
        (self.0 >> 9) & 0o777
    }
    fn p1_index(&self) -> usize {
        self.0 & 0o777
    }
}

#[derive(Debug, Clone, Copy)]
pub struct PTEntry(pub u64);

impl PTEntry {
    pub const PRESENT: u64 =         1 << 0;
    pub const WRITABLE: u64 =        1 << 1;
    pub const USER_ACCESSIBLE: u64 = 1 << 2;
    pub const WRITE_THROUGH: u64 =   1 << 3;
    pub const NO_CACHE: u64 =        1 << 4;
    pub const ACCESSED: u64 =        1 << 5;
    pub const DIRTY: u64 =           1 << 6;
    pub const HUGE_PAGE: u64 =       1 << 7;
    pub const GLOBAL: u64 =          1 << 8;
    pub const COPY_ON_WRITE: u64 =   1 << 9;
    pub const SHARED: u64 =          1 << 10;
    pub const NO_EXECUTE: u64 =      1 << 63;

    fn is_unused(&self) -> bool {
        self.0 == 0
    }

    fn set_unused(&mut self) {
        self.0 = 0;
    }

    pub fn get_frame(&self) -> Option<Physical> {
        if self.0 & Self::PRESENT != 0 {
            Some(Physical((self.0 as usize & 0x000fffff_fffff000) / super::frames::FRAME_SIZE))
        } else {
            None
        }
    }

    fn set(&mut self, frame_addr: Physical, flags: u64) {
        assert_eq!(frame_addr.0 & !0x000fffff_fffff000, 0);
        self.0 = frame_addr.0 as u64 | flags;
    }

    pub fn get_flags(&self) -> u64 {
        self.0 & 0x0000000000000fff
    }
}

const ENTRY_COUNT: usize = 512;

#[derive(Debug)]
pub struct PTable {
    entries: [PTEntry; ENTRY_COUNT],
}

impl PTable {
    pub const P4: *mut PTable = 0xffffffff_fffff000 as *mut _;

    pub fn zero(&mut self) {
        for entry in self.entries.iter_mut() {
            entry.set_unused();
        }
    }

    fn next_table_address(&self, index: usize) -> Option<usize> {
        let entry_flags = self.entries[index].0;
        if entry_flags & PTEntry::PRESENT != 0 && entry_flags & PTEntry::HUGE_PAGE == 0 {
            let table_address = self as *const _ as usize;
            Some((table_address << 9) | (index << 12))
        } else {
            None
        }
    }

    pub fn next_table(&self, index: usize) -> Option<&PTable> {
        self.next_table_address(index)
            .map(|address| unsafe { &*(address as *const _) })
    }

    pub fn next_table_mut(&mut self, index: usize) -> Option<&mut PTable> {
        self.next_table_address(index)
            .map(|address| unsafe { &mut *(address as *mut _) })
    }

    pub fn next_table_create(&mut self, index: usize) -> &mut PTable {
        if self.next_table(index).is_none() {
            assert!(self.entries[index].0 & PTEntry::HUGE_PAGE == 0,
                    "mapping code does not support huge pages");
            let frame = alloc_frame();
            self.entries[index].set(frame, PTEntry::PRESENT | PTEntry::WRITABLE | PTEntry::USER_ACCESSIBLE);
            self.next_table_mut(index).unwrap().zero();
        }
        self.next_table_mut(index).unwrap()
    }
}

#[derive(Debug)]
pub struct TemporaryPage {
    page: Page,
}

impl TemporaryPage {
    pub fn map(&mut self, frame_addr: Physical, active_table: &mut ActivePageTable) -> Virtual {
        assert!(active_table.translate_page(self.page).is_none(),
                "temporary page is already mapped");
        active_table.map_to_addr(self.page, frame_addr, PTEntry::WRITABLE, false, false);
        self.page.start_address()
    }

    /// Unmaps the temporary page in the active table.
    pub fn unmap(&mut self, active_table: &mut ActivePageTable, unmap_frame: bool) {
        active_table.unmap(self.page, unmap_frame);
        unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)}
    }

    pub fn map_table_frame_addr(&mut self, frame_addr: Physical, active_table: &mut ActivePageTable) -> &mut PTable {
        unsafe { &mut *(self.map(frame_addr, active_table).0 as *mut PTable) }
    }

    pub fn new(page: Page) -> Self {
        Self {
            page,
        }
    }
}

#[derive(Debug, Clone)]
pub struct InactivePageTable {
    pub p4_addr: Physical,
}

impl InactivePageTable {
    pub fn new(addr: Physical, active_table: &mut ActivePageTable, temporary_page: &mut TemporaryPage) -> Self {
        {
            let table = temporary_page.map_table_frame_addr(addr, active_table);
            // now we are able to zero the table
            table.zero();
            // set up recursive mapping for the table
            table.entries[511].set(addr, PTEntry::PRESENT | PTEntry::WRITABLE);
            unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)}
        }
        temporary_page.unmap(active_table, false);

        Self{ p4_addr: addr }
    }
}

pub fn remap_kernel(base: usize, top: usize, (fb_start, fb_end): (usize, usize)) {
    let mut temporary_page = TemporaryPage::new(Page::from_address(Virtual(PAGE_BUF)));

    let mut active_table = ActivePageTable::get();
    // unmap null page
    let mut new_table = {
        let frame = alloc_frame();
        InactivePageTable::new(frame, &mut active_table, &mut temporary_page)
    };

    active_table.with_tmp_table(&mut new_table, &mut temporary_page, |mapper| {
        for a in (base .. top).step_by(PAGE_SIZE) {
            assert!(a % PAGE_SIZE == 0,
                    "address need to be page aligned");

            mapper.identity_map(Physical(a), PTEntry::WRITABLE, true);
        }

        for a in (fb_start .. fb_end).step_by(PAGE_SIZE) {
            mapper.identity_map(Physical(a), PTEntry::WRITABLE, true);
        }
    });
    let old_table = active_table.switch(new_table);
    dealloc_frame(old_table.p4_addr);
}
