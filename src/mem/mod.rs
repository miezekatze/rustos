#[derive(Debug, Clone, Copy)]
pub struct Physical(pub usize);
#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct Virtual(pub usize);

pub mod frames;
pub mod paging;
pub mod alloc;
