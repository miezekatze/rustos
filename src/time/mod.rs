use core::arch::asm;

use crate::{drivers::pit, println};

#[derive(Debug, Clone, Copy)]
pub struct Timespec {
    sec: u64,
    nsec: u64,
}

pub fn sleep(time: &Timespec) {
    let current_nanos = pit::get_nanos();
    let target_nanos = current_nanos + time.sec as u128 * 1_000_000_000 + time.nsec as u128;
    while pit::get_nanos() < target_nanos {
        unsafe{asm!("pause")};
    }
}
