use core::fmt::Write;
mod utils;

use core::arch::{global_asm, asm};

use alloc::boxed::Box;
use alloc::{vec, vec::Vec};

use crate::cpu::interrupts::INT_STACK_TOP;
use crate::drivers::debugcon::DebugCon;
use crate::drivers::fs::{Fd, open, close, FileDescriptor, DirEntries, readdir};
use crate::mem::frames::{alloc_frame, FRAME_SIZE, dealloc_frame};
use crate::mem::{Physical, Virtual};
use crate::user::{USERMODE_STACK_START, USERMODE_STACK_SIZE};
use crate::utils::errno::{ErrnoOr, Errno};
use crate::{div_ceil, stack_bottom};
use crate::mem::paging::{PAGE_SIZE, ActivePageTable, Page, PTEntry, PageBuf, InactivePageTable, TemporaryPage};
use crate::utils::lock::Lock;

use self::utils::OneIndexed;

#[derive(Debug, Clone, Copy, PartialEq)]
#[allow(unused)]
pub enum ProcState {
    Running,
    Stopped,
    Zombie(u8),
    WaitingForChild(isize),
} 

#[derive(Debug)]
#[allow(unused)]
pub struct Process {
    pid: usize,
    cr3: usize,
    mmap_ares: Vec<MmapArea>,
    fds: Vec<Option<FileDescriptor>>,
    threads: Vec<usize>,
    parent: Option<usize>,
    children: Vec<usize>,
    pub state: ProcState,
}

impl Clone for Process {
    fn clone(&self) -> Self {
        Self {
            pid: self.pid,
            cr3: self.cr3,
            mmap_ares: self.mmap_ares.clone(),
            fds: self.fds.iter().map(|fd| fd.as_ref().cloned()).collect(),
            threads: self.threads.clone(),
            parent: self.parent,
            children: self.children.clone(),
            state: self.state, 
        }
    }
}

pub fn openfd(path: &str, flags: usize, mode: usize) -> ErrnoOr<usize> {
    let fd = open(path, flags, mode)?;
    
    let tid = *CURRENT_TID.read();
    let pid = THREADS.read()[tid].as_ref().ok_or(Errno::ESRCH)?.pid;

    PROCS.write()[pid].as_mut().ok_or(Errno::ESRCH)?.fds.push(Some(fd));
    Ok(PROCS.read()[pid].as_ref().ok_or(Errno::ESRCH)?.fds.len() - 1)
}

pub fn getfd(fd: usize) -> ErrnoOr<FileDescriptor> {
    let pid = getpid();
    let mut guard = PROCS.write();
    let fds = &mut guard[pid].as_mut().ok_or(Errno::ESRCH)?.fds;
    if fds.len() <= fd {
        return Err(Errno::EBADF);
    }
    let ifd = fds[fd].as_ref().ok_or(Errno::EBADF)?.clone();
    drop(guard);
    Ok(ifd)
}

pub fn setfd(fd: usize, ifd: FileDescriptor) -> ErrnoOr<()> {
    let pid = getpid();
    let mut guard = PROCS.write();
    let fds = &mut guard[pid].as_mut().ok_or(Errno::ESRCH)?.fds;
    if fds.len() <= fd {
        return Err(Errno::EBADF);
    }
    fds[fd] = Some(ifd);
    Ok(())
}

pub fn readfd(fd: usize, buf: &mut [u8]) -> ErrnoOr<usize> {
    let mut ifd = getfd(fd)?;
    let val = ifd.read(buf);
    setfd(fd, ifd)?;
    val
}

pub fn readdirfd(fd: usize) -> ErrnoOr<Box<dyn DirEntries>> {
    let ifd = getfd(fd)?;
    let val = readdir(&ifd);
    setfd(fd, ifd)?;
    val
}

pub fn writefd(fd: usize, buf: &[u8]) -> ErrnoOr<usize> {
    let tid = *CURRENT_TID.read();
    let pid = THREADS.read()[tid].as_ref().ok_or(Errno::ESRCH)?.pid;

    let mut guard = PROCS.write();
    let fds = &mut guard[pid].as_mut().ok_or(Errno::ESRCH)?.fds;
    if fds.len() <= fd {
        return Err(Errno::EBADF);
    }

    let mut ifd = fds[fd].as_ref().ok_or(Errno::EBADF)?.clone();
    drop(guard);
    let val = ifd.write(buf);
    PROCS.write()[pid].as_mut().ok_or(Errno::ESRCH)?.fds[fd] = Some(ifd);
    val
}


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[allow(unused)]
pub struct MmapArea {
    pub start: usize,
    pub end: usize,
    pub prot: u8,
    pub user_alloc: bool,
    shared_fd: Option<usize>,
}

pub const PROT_READ: u8 = 0x01;
pub const PROT_WRITE: u8 = 0x02;
pub const PROT_EXEC: u8 = 0x04;

pub fn add_mmap_area(start: usize, end: usize, prot: u8, user_alloc: bool, shared_fd: Option<usize>) -> ErrnoOr<()> {
    let tid = *CURRENT_TID.read();
    let pid = THREADS.read()[tid].as_ref().ok_or(Errno::ESRCH)?.pid;

    let mut guard = PROCS.write();
    let proc = &mut guard[pid].as_mut().ok_or(Errno::ESRCH)?;
    proc.mmap_ares.push(MmapArea {
        start,
        end,
        prot,
        user_alloc,
        shared_fd,
    });
    drop(guard);
    Ok(())
}

pub fn clear_mmap_areas() -> ErrnoOr<()> {
    let tid = *CURRENT_TID.read();
    let pid = THREADS.read()[tid].as_ref().ok_or(Errno::ESRCH)?.pid;

    let mut guard = PROCS.write();
    let proc = &mut guard[pid].as_mut().ok_or(Errno::ESRCH)?;
    proc.mmap_ares.clear();
    drop(guard);
    Ok(())
}

pub fn get_mmap_areas() -> Vec<MmapArea> {
    let pid = getpid();
    let guard = PROCS.read();
    let proc = &guard[pid].as_ref().unwrap();
    proc.mmap_ares.clone()
}

pub fn remove_mmap_area(area: MmapArea) {
    let pdi = getpid();
    let mut guard = PROCS.write();
    let proc = &mut guard[pdi].as_mut().unwrap();
    proc.mmap_ares.retain(|a| *a != area);
}

#[derive(Debug, Default, Clone, Copy)]
#[repr(C)]
struct Registers {
    rax: u64,
    rbx: u64,
    rcx: u64,
    rdx: u64,

    r8: u64,
    r9: u64,
    r10: u64,
    r11: u64,
    r12: u64,
    r13: u64,
    r14: u64,
    r15: u64,

    rflags: u64,

    rsi: u64,
    rdi: u64,

    fs: u64,
    gs: u64,
    cr3: u64,

    rsp: u64,
    rbp: u64,

    rip: u64,
}

global_asm!(
    "get_rip:",
    "pop rax",
    "jmp rax",
);

impl Registers {
    pub fn zero() -> Self {
        Self::default()
    }
}

pub const STACK_PAGES: usize = div_ceil!(STACK_SIZE, PAGE_SIZE);
const USER_STACK_PAGES: usize = div_ceil!(USERMODE_STACK_SIZE as usize, PAGE_SIZE);

/*static SHARED_ALLOCATOR: Allocator = Allocator {
    start: HEAP_START * 0x10,
    shared: true,
};*/

pub fn exit_current(code: u8) -> ErrnoOr<usize> {
    unsafe{asm!("cli")};
    let mut tguard = THREADS.write();
    let thread = tguard[*CURRENT_TID.read()].as_mut().ok_or(Errno::ESRCH)?;
    let pid = thread.pid;

    let mut pguard = PROCS.write();
    let proc = &mut pguard[pid].as_mut().ok_or(Errno::ESRCH)?;
    drop(tguard);

    let threads = proc.threads.clone();
    drop(pguard);

    for thread in threads {
        exit_cleanup_thread(thread)?;
        THREADS.write()[thread] = None;
    }

    exit_cleanup_process(pid)?;
    let mut pguard = PROCS.write();
    let proc = pguard[pid].as_mut().ok_or(Errno::ESRCH)?;
    proc.state = ProcState::Zombie(code);
    proc.threads.clear();
    let children = proc.children.clone();

    for fd in proc.fds.iter_mut() {
        if let Some(fd) = fd.take() {
            close(fd);
        }
    }

    for c in children {
        if let Some(child) = pguard[c].as_mut() {
            child.parent = None; // transfer ownership to init
            if let Some(ref mut init) = pguard[1].as_mut() {
                init.children.push(c);
            }
        }
    }

    drop(pguard);
    switch_task();
    Err(Errno::EINVAL)
}

fn exit_cleanup_thread(tid: usize) -> ErrnoOr<()> {
    let mut guard = THREADS.write();
    let thread = guard[tid].as_mut().ok_or(Errno::ESRCH)?;

    // dealloc kstack
    for f in thread.kstack.iter() {
        dealloc_frame(Physical(*f as usize));
    }

    // dealloc ustack
    for f in thread.ustack.iter() {
        if *f != 0 {
            dealloc_frame(Physical(*f as usize));
        }
    }

    // dealloc istack
    for f in thread.istack.iter() {
        dealloc_frame(Physical(*f as usize));
    }

    Ok(())
}

fn exit_cleanup_process(pid: usize) -> ErrnoOr<()> {
    let guard = PROCS.write();
    let proc = &guard[pid].as_ref().ok_or(Errno::ESRCH)?;
    let mut apt = ActivePageTable::get();

    let mut tpg = TemporaryPage::new(Page::from_address(Virtual(PAGE_BUF.0.as_ptr() as usize)));
    tpg.unmap(&mut apt, false);
    let mut pt = InactivePageTable { p4_addr: Physical(proc.cr3) };

    apt.with_tmp_table(&mut pt, &mut tpg, |mapper| {
        for mmap_area in proc.mmap_ares.iter() {
            let mut vaddr = mmap_area.start;
            while vaddr < mmap_area.end {
                mapper.unmap(Page::from_address(Virtual(vaddr)), true);
                vaddr += PAGE_SIZE;
            }
        }
    });
    Ok(())
}

#[derive(Debug, Clone)]
#[allow(unused)]
pub struct Thread {
    pub pid: usize, 
    tid: usize,
    regs: Registers,
    kstack: [u64; STACK_PAGES],
    istack: [u64; STACK_PAGES],
    pub ustack: [u64; USER_STACK_PAGES],
}

static CURRENT_TID: Lock<usize> = Lock::new(0);
static THREADS: Lock<OneIndexed<Vec<Option<Thread>>>> = Lock::new(OneIndexed::new(vec![]));
static PROCS: Lock<OneIndexed<Vec<Option<Process>>>> = Lock::new(OneIndexed::new(vec![]));

pub fn init_tasking(stack_btm: u64, istack_pages: &[u64; STACK_PAGES]) {
//    __init_alloc(&SHARED_ALLOCATOR);

    unsafe {asm!("cli")};

    *CURRENT_TID.write() = 1;

    let cr3: usize;
    unsafe {asm!("mov {}, cr3", out(reg) cr3)};

    let proc = Process {
        pid: 1,
        cr3,
        mmap_ares: vec![],
        fds: vec![],
        threads: vec![1],
        parent: None,
        children: vec![],
        state: ProcState::Running, 
    };

    let mut kstack = [0u64; STACK_PAGES];
    let pt = ActivePageTable::get();

    #[allow(clippy::assertions_on_constants)]
    const _: () = assert!(FRAME_SIZE == PAGE_SIZE);

    for (i, a) in (stack_btm .. stack_btm + STACK_SIZE as u64).step_by(PAGE_SIZE).enumerate() {
        let phys = pt.translate(crate::mem::Virtual(a as usize)).unwrap().0;
        kstack[i] = phys as u64;
    }

    let thread = Thread {
        pid: 1, 
        tid: 1,
        regs: Registers::zero(),
        kstack,
        ustack: [0; USER_STACK_PAGES],
        istack: *istack_pages,
    };

    THREADS.write().push(Some(thread));
    PROCS.write().push(Some(proc));

    unsafe {asm!("sti")};
}

fn next_tid() -> usize {
    THREADS.read().len() + 1
//    for i in 0 .. THREADS.read().len() {
//        if THREADS.read()[i+1].is_none() {
        // TODO: check for dead threads and reuse their tids
//            return i+1;
//        }
//    }
//    panic!("no more threads!");
}

fn next_pid() -> usize {
    for i in 1 .. PROCS.read().len()+1 {
        if PROCS.read()[i].is_none() {
            return i;
        }
    }
    PROCS.read().len() + 1
//    panic!("no more processes!");
}

macro_rules! save_state {
    ($rip:ident, $rbp:ident, $rsp:ident, $rbx:ident, $r12:ident, $r13:ident, $r14:ident, $r15:ident, $rflags:ident) => {
        unsafe{asm!("mov {rbx}, rbx; mov {r12}, r12; mov {r13}, r13; mov {r14}, r14; mov {r15}, r15",
               "mov {rbp}, rbp; mov {rsp}, rsp; pushfq; pop {rflags}; call get_rip; mov {rip}, rax",
                rbp = out(reg) $rbp,
                rsp = out(reg) $rsp,
                rip = out(reg) $rip,
                rbx = out(reg) $rbx,
                r12 = out(reg) $r12,
                r13 = out(reg) $r13,
                r14 = out(reg) $r14,
                r15 = out(reg) $r15,
                rflags = out(reg) $rflags,
                out("rax") _)};
    };
}

#[inline(never)]
extern "C" fn save_task() -> bool {
    let tid = *CURRENT_TID.read();
    if tid == 0 {
        return true;
    }

    if THREADS.read().is_empty() {
        panic!("No processes found to run! Init exited?");
    }

    if THREADS.read()[tid].is_none() {
        return false;
    }

    // save state
    let (rip, rbp, rsp, rbx, r12, r13, r14, r15, rflags): (u64, u64, u64, u64, u64, u64, u64, u64, u64);
    save_state!(rip, rbp, rsp, rbx, r12, r13, r14, r15, rflags);
    if rip == 0x42694269 {
        return true;
    }

    let mut lock = THREADS.write();
    let mut guard = lock.get_mut(tid-1);
    let lv = guard.as_mut().unwrap().as_mut().unwrap();

    lv.regs.rip = rip;
    lv.regs.rsp = rsp;
    lv.regs.rbp = rbp;
    lv.regs.rbx = rbx;
    lv.regs.r12 = r12;
    lv.regs.r13 = r13;
    lv.regs.r14 = r14;
    lv.regs.r15 = r15;
    lv.regs.rflags = rflags;

    false
}

extern "C" {
//    fn with_extra_stack(f: extern "C" fn(rsp_ptr: usize, old_rsp: usize) -> usize) -> usize;
    fn perform_task_switch(rip: usize, cr3: usize, rsp: usize, rbp: usize);
}

#[inline(never)]
extern "C" fn load_next_task() {
    let tid = *CURRENT_TID.read();
    if tid == 0 {
        return; 
    }

    if THREADS.read().is_empty() {
        panic!("No processes found to run! Init exited?");
    }

    if THREADS.read()[1].is_none() {
        panic!("--- Init died ---");
    }

    let tid_len = THREADS.read().len();
    let mut next_tid = tid % tid_len + 1;
    let orig_tid = next_tid;

    fn cannot_use(tid: usize) -> bool {
        if THREADS.read()[tid].is_none() {
            true
        } else {
            let pid = THREADS.read()[tid].as_ref().unwrap().pid;
            let guard = PROCS.read();
            match guard[pid].as_ref().unwrap().state {
                ProcState::Running => false,
                ProcState::Stopped => true,
                ProcState::Zombie(_) => true,
                ProcState::WaitingForChild(cpid) => {
                    drop(guard);
                    let res = get_waited_child_result(pid, cpid);
                    if res.is_err() {
                        return false;
                    }
                    res.unwrap().is_none()
                },
            }
        }
    }

    while cannot_use(next_tid) {
        next_tid = next_tid % tid_len + 1;
        if next_tid == orig_tid {
            panic!("No processes found to run! Init exited?");
        }
    } // TODO: check for dead threads and skip them

    let next_thread = THREADS.read()[next_tid].as_ref().unwrap().clone();
    *CURRENT_TID.write() = next_tid;
    assert_ne!(next_tid, 0);

    #[allow(clippy::assertions_on_constants, clippy::let_unit_value)]
    let _: () = assert_eq!(STACK_PAGES, 80);
    let mut vals = [0xdeadbeefcafebabeu64; COMPLETE_PAGES*2];

    // since table is recursive and alwayas mapped to the same address, we can
    // use the active table, instead of the new one
    let mut table = ActivePageTable::get();
    for a in 0 .. STACK_PAGES {
        let v = next_thread.kstack[a];
        let addr = (unsafe{&stack_bottom as *const _} as usize + a * PAGE_SIZE) as *const u8;

        let (ptr, val) = table.map_to_addr(Page::from_address(Virtual(addr as usize)),
                            Physical(v as usize),
                            PTEntry::WRITABLE | PTEntry::PRESENT, true, true);
        vals[a*2] = ptr as u64;
        vals[a*2+1] = val as u64;
    }
    for a in 0 .. USER_STACK_PAGES {
        let v = next_thread.ustack[a];
        let addr = ((USERMODE_STACK_START - USERMODE_STACK_SIZE) as usize + a * PAGE_SIZE) as *const u8;

        let (ptr, val) = table.map_to_addr(Page::from_address(Virtual(addr as usize)),
                            Physical(v as usize),
                            PTEntry::WRITABLE | PTEntry::PRESENT | PTEntry::USER_ACCESSIBLE, true, true);
        vals[STACK_PAGES*2 + a*2] = ptr as u64;
        vals[STACK_PAGES*2 + a*2+1] = val as u64;
    }
    for a in 0 .. STACK_PAGES {
        let v = next_thread.istack[a];
        let addr = ((INT_STACK_TOP - STACK_SIZE) + a * PAGE_SIZE) as *const u8;

        let (ptr, val) = table.map_to_addr(Page::from_address(Virtual(addr as usize)),
                            Physical(v as usize),
                            PTEntry::WRITABLE | PTEntry::PRESENT, true, true);
        vals[STACK_PAGES*2 + USER_STACK_PAGES*2 + a*2] = ptr as u64;
        vals[STACK_PAGES*2 + USER_STACK_PAGES*2 + a*2+1] = val as u64;
    }

    unsafe {
        let rip = next_thread.regs.rip as usize;
        let rsp = next_thread.regs.rsp as usize;
        let rbp = next_thread.regs.rbp as usize;
        let cr3 = PROCS.read()[next_thread.pid].as_ref().unwrap().cr3;
//        writeln!(DebugCon, "Switching to thread {} (pid {})", next_tid, next_thread.pid).unwrap();
        for (i, a) in vals.iter().enumerate() {
            SWITCH_FRAME_DATA[i] = *a;
        }
//      writeln!(DebugCon, "--- switching ---").unwrap();
        asm!("call {switch}",
             in("rdi") rip,
             in("rsi") cr3,
             in("r11") rbp,
             in("rcx") rsp,
             in("rax") next_thread.regs.rbx,
             in("r12") next_thread.regs.r12,
             in("r13") next_thread.regs.r13,
             in("r14") next_thread.regs.r14,
             in("r15") next_thread.regs.r15,
             in("rdx") next_thread.regs.rflags,
             switch = sym perform_task_switch
             );
//        perform_task_switch(rip, cr3, rbp, rsp);
    }
}

const COMPLETE_PAGES: usize = STACK_PAGES + USER_STACK_PAGES + STACK_PAGES;
static FRAME_DATA_LEN: usize = COMPLETE_PAGES;

static mut SWITCH_FRAME_DATA: [u64; COMPLETE_PAGES*2] = [0; COMPLETE_PAGES*2];
global_asm!("perform_task_switch:
            mov rbp, r11

            mov cr3, rsi
            lea r8, {data}
            mov r11, [{stack_pages}]
            .loop:
            mov r9, [r8]
            mov r10, [r8+8]
            mov [r9], r10
            add r8, 16
            dec r11
            jne .loop

            mov rbx, rax
            mov r8, rdi
            mov rsp, rcx
            mov rax, 0x42694269
 
            push rdx
            popfq
            jmp r8", data = sym SWITCH_FRAME_DATA, stack_pages = sym FRAME_DATA_LEN);

/*#[inline(never)]
fn dummy(_x: &str) -> usize {
    0
}*/

pub fn switch_task() -> usize {
    if ! THREADS.write_avail() || ! PROCS.write_avail() {
//        println!("lock not availible");
        return 0;
    }
    if save_task() {
        return 0;
    }
//    writeln!(DebugCon, "-- task switch --").unwrap();
    load_next_task();
//    writeln!(DebugCon, "-- task switch end --").unwrap();
    
//    dummy("-- task switch end --")
    unsafe{asm!("nop;nop;nop")};
    0
}
pub fn remove_child(cpid: usize) {
    let new_tid = *CURRENT_TID.read();
    let new_pid = THREADS.read()[new_tid].as_ref().unwrap().pid;

    let mut pguard = PROCS.write();
    let proc = pguard[new_pid].as_mut().unwrap();
    proc.children.retain(|&p| p != cpid);
}

fn get_waited_child_result(pid: usize, cpid: isize) -> ErrnoOr<Option<(u8, usize)>> {
    let mut pguard = PROCS.write();
    let proc = pguard[pid].as_mut().unwrap();
    
    if cpid < -1 {
        return Err(Errno::ENOTSUP);
    }

    let children = &mut proc.children;
    if children.is_empty() {
        return Err(Errno::ECHILD);
    }

    let child_pid = if cpid == -1 {
        children[0]
    } else {
        let mut found = false;
        for &p in children.iter() {
            if p == cpid as usize {
                found = true;
                break;
            }
        }
        if !found {
            return Err(Errno::ECHILD);
        }
        cpid as usize
    };

    let child = pguard[child_pid].as_mut().ok_or(Errno::ECHILD)?;
    if let ProcState::Zombie(ret) = child.state {
        return Ok(Some((ret, child_pid)));
    }

    Ok(None)
}

pub fn getpid() -> usize {
    let tid = *CURRENT_TID.read();
    if tid == 0 {
        return 0;
    }
    let pid = THREADS.read()[tid].as_ref().unwrap().pid;
    pid
}

pub fn collect_child(pid: isize) -> ErrnoOr<Option<(u8, usize)>> {
    if let Some((ret, child_pid)) = get_waited_child_result(getpid(), pid)? {
        let mut pguard = PROCS.write();
        pguard[child_pid] = None;
        Ok(Some((ret, child_pid)))
    } else {
        Ok(None)
    } 
}

pub const STACK_SIZE: usize = 327680;
static PAGE_BUF: PageBuf = PageBuf([0; PAGE_SIZE]);

pub fn fork_thread() -> usize {
    let x = fork_thread_(false);
    x
}

pub fn fork() -> usize {
    let x = fork_thread_(true);
    x
}

#[inline(never)]
pub extern "C" fn fork_thread_(is_fork: bool) -> usize {
    let tid = *CURRENT_TID.read();
    let pid = PROCS.read()[tid].as_ref().unwrap().pid;

    let mut new_thread = THREADS.read()[tid].as_ref().unwrap().clone();

    let (rip, rbp, rsp, rbx, r12, r13, r14, r15, rflags): (u64, u64, u64, u64, u64, u64, u64, u64, u64);
    save_state!(rip, rbp, rsp, rbx, r12, r13, r14, r15, rflags);

    if tid != *CURRENT_TID.read() {
//        writeln!(DebugCon, "-- task switch end (fork) --").unwrap();
        // disable interrupts again, because they may be active here
        unsafe{asm!("cli")};
        0
    } else {
        let tid = next_tid();
        new_thread.tid = tid;

        new_thread.regs.rip = rip;
        new_thread.regs.rsp = rsp;
        new_thread.regs.rbp = rbp;
        new_thread.regs.rbx = rbx;
        new_thread.regs.r12 = r12;
        new_thread.regs.r13 = r13;
        new_thread.regs.r14 = r14;
        new_thread.regs.r15 = r15;
        new_thread.regs.rflags = rflags;

        if is_fork {
            let mut new_proc = PROCS.read()[new_thread.pid].as_ref().unwrap().clone();

            // copy page table
            let mut table = ActivePageTable::get();
            let new_table = table.clone_table();
            new_proc.cr3 = new_table.p4_addr.0;
            new_proc.pid = next_pid();
            new_proc.threads = vec![tid];
            new_proc.state = ProcState::Running;

            // map new areas
/*            let areas = &new_proc.mmap_ares;
            let mut tmp_page = TemporaryPage::new(Page::from_address(Virtual(PAGE_BUF.0.as_ptr() as usize)));
            tmp_page.unmap(&mut table, false);
            table.with_tmp_table(&mut new_table, &mut tmp_page, |mapper| {
                for a in areas {
                    for addr in (a.start .. a.end).step_by(PAGE_SIZE) {
                        let page = Page::from_address(Virtual(addr));
                        let frame = alloc_frame();
                        let mut flags = PTEntry::USER_ACCESSIBLE;
                        if a.prot & PROT_WRITE != 0 {
                            flags |= PTEntry::WRITABLE;
                        }
                        if a.prot & PROT_EXEC == 0 {
                            flags |= PTEntry::NO_EXECUTE;
                        }

                        let (ptr, val) = mapper.map_to_addr(page, frame, flags, true, false);
                        unsafe{*(ptr as *mut _) = val};
                    }
                }
            });
*/
            new_proc.parent = Some(pid);
            new_thread.pid = new_proc.pid;

            if new_proc.pid == PROCS.read().len() + 1 {
                PROCS.write().push(Some(new_proc));
            } else {
                PROCS.write()[new_proc.pid] = Some(new_proc.clone());
            }
            
            PROCS.write()[pid].as_mut().unwrap().children.push(new_thread.pid);
       } else {
            PROCS.write()[pid].as_mut().unwrap().threads.push(new_thread.tid);
       }

        // copy stack
        for a in 0 .. STACK_PAGES {
            let f = alloc_frame();
            let page = Page::from_address(Virtual(PAGE_BUF.0.as_ptr() as usize));
            let (ptr, v) = ActivePageTable::get().map_to_addr(page, f, PTEntry::WRITABLE, true, true);
            unsafe{*(ptr as *mut _) = v};
            unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};

            let addr = (unsafe{&stack_bottom as *const _} as usize + a * PAGE_SIZE) as *const u8;
            unsafe {asm!("cld;rep movsb", in("rsi") addr, in("rdi") PAGE_BUF.0.as_ptr() as *mut u8, in("rcx") PAGE_SIZE)};
            new_thread.kstack[a] = f.0 as u64;
        }

        // copy ustack
        for a in 0 .. USER_STACK_PAGES {
            let f = alloc_frame();
            let page = Page::from_address(Virtual(PAGE_BUF.0.as_ptr() as usize));
            let (ptr, v) = ActivePageTable::get().map_to_addr(page, f, PTEntry::WRITABLE, true, true);
            unsafe{*(ptr as *mut _) = v};
            unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};

            let addr = ((USERMODE_STACK_START - USERMODE_STACK_SIZE) as usize + a * PAGE_SIZE) as *const u8;
            unsafe {asm!("cld;rep movsb", in("rsi") addr, in("rdi") PAGE_BUF.0.as_ptr() as *mut u8, in("rcx") PAGE_SIZE)};
            new_thread.ustack[a] = f.0 as u64;
        }
        // copy istack
        for a in 0 .. STACK_PAGES {
            let f = alloc_frame();
            let page = Page::from_address(Virtual(PAGE_BUF.0.as_ptr() as usize));
            let (ptr, v) = ActivePageTable::get().map_to_addr(page, f, PTEntry::WRITABLE, true, true);
            unsafe{*(ptr as *mut _) = v};
            unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};

            let addr = ((INT_STACK_TOP - STACK_SIZE) + a * PAGE_SIZE) as *const u8;
            unsafe {asm!("cld;rep movsb", in("rsi") addr, in("rdi") PAGE_BUF.0.as_ptr() as *mut u8, in("rcx") PAGE_SIZE)};
            new_thread.istack[a] = f.0 as u64;
        }

        if tid == THREADS.read().len() + 1 {
            THREADS.write().push(Some(new_thread));
        } else {
            THREADS.write()[tid] = Some(new_thread);
        }

        tid
    }
}

pub fn set_ustack_frame(i: usize, frame: u64) {
    THREADS.write()[*CURRENT_TID.read()].as_mut().unwrap().ustack[i] = frame;
}

pub fn set_current_waiting(pid: isize) {
    let cpid = THREADS.read()[*CURRENT_TID.read()].as_ref().unwrap().pid;
    PROCS.write()[cpid].as_mut().unwrap().state = ProcState::WaitingForChild(pid);
}
