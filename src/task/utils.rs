use core::ops::{Index, IndexMut, Deref, DerefMut, Sub};

#[derive(Debug)]
pub struct OneIndexed<T> {
    inner: T,
}

impl<T> OneIndexed<T> {
    pub const fn new(x: T) -> Self {
        Self {
            inner: x,
        }
    }
}

impl<T> From<T> for OneIndexed<T> {
    fn from(value: T) -> Self {
        Self {
            inner: value,
        }
    }
}

impl<T> Iterator for OneIndexed<T>
where T: Iterator {
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl<O, I: Sub<Output = I>, T: Index<I, Output = O>> Index<I> for OneIndexed<T>
where u8: Into<I> {
    type Output = O;
    fn index(&self, index: I) -> &O {
        self.inner.index(index - 1.into())
    }
}

impl<O, I: Sub<Output = I>, T: IndexMut<I, Output = O>> IndexMut<I> for OneIndexed<T>
where u8: Into<I> {
    fn index_mut(&mut self, index: I) -> &mut O {
        self.inner.index_mut(index - 1.into())
    }
}

impl<T> Deref for OneIndexed<T> {
    type Target= T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> DerefMut for OneIndexed<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}
