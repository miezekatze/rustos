use core::arch::asm;

pub trait IOPort {
    /// reads `Self` from IOPort `port`
    /// # Safety
    /// read can cause unexpected behaviour, crash in ring 3
    unsafe fn pin(port: u16) -> Self;
    /// writes `self` to IOPort `port`
    /// # Safety
    /// write can cause unexpected behaviour, crash in ring 3
    unsafe fn pout(self, port: u16);
}

impl IOPort for u8 {
    unsafe fn pin(port: u16) -> u8 {
        let v;
        asm!("in al, dx",
             out("al") v,
             in("dx") port);
        v
    }

    unsafe fn pout(self, port: u16) {
        asm!("out dx, al",
             in("al") self,
             in("dx") port)
    }
}

impl IOPort for u16 {
    unsafe fn pin(port: u16) -> u16 {
        let v;
        asm!("in ax, dx",
             out("ax") v,
             in("dx") port);
        v
    }

    unsafe fn pout(self, port: u16) {
        asm!("out dx, ax",
             in("ax") self,
             in("dx") port)
    }
}

impl IOPort for u32 {
    unsafe fn pin(port: u16) -> u32 {
        let v;
        asm!("in eax, dx",
             out("eax") v,
             in("dx") port);
        v
    }

    unsafe fn pout(self, port: u16) {
        asm!("out dx, eax",
             in("eax") self,
             in("dx") port)
    }
}
