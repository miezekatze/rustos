#![feature(lang_items, abi_x86_interrupt, format_args_nl, allocator_api)]
#![no_std]

extern crate alloc;

use alloc::{vec:: Vec, string::{String, ToString}};
use drivers::fs::{readdir, O_RDONLY, O_DIRECTORY, Fd};
use mem::frames::{set_frames_mmap, set_frames_kernel};
use utils::errno::ErrnoOr;
use video::{DisplayInfo, setup_video};

use crate::{mem::{paging::remap_kernel, alloc::init_heap},
            video::DISPLAY,
            cpu::interrupts::{init_idt, PIC},
            drivers::{ata::ata_init, fs::{init_vfs, VFS, FileType},
                ext2::ext2_init, tty::tty_init, pit::init_pit}, task::init_tasking};

pub mod rust_utils;
pub mod utils;
pub mod mem;
pub mod video;
pub mod io;
pub mod drivers;
pub mod cpu;
pub mod task;
pub mod user;
pub mod time;

macro_rules! log {
    ($s:expr, $f:expr $(,$a:expr)*) => {{
        print!("{}...", $s);
        let res = $f($($a),*);
        println!("\r{} OK", $s);
        res
    }}
}

extern "C" {
    pub static stack_top: u8;
    pub static stack_bottom: u8;
}


#[no_mangle]
pub extern "C" fn main(s_ptr: *const (), mmap_addr: *const (), elf_addr: *const ()) {
    setup_video(&DisplayInfo::from_ptr(s_ptr));
    set_frames_mmap(mmap_addr);
    let (kbase, ktop) = set_frames_kernel(elf_addr);
    remap_kernel(kbase, ktop, DISPLAY.read().get_address_area());
    init_heap();

    log!("initializing pic", || unsafe {PIC.write().init()});
    let iframes = log!("initializing interrupts", init_idt);
    log!("initializing pit", init_pit);
    log!("initializing vfs", init_vfs);

    let devices = log!("enumerating PCI devices", drivers::pci::enumerate_pci);
    println!("\n===== PCI DEVICES =====");
    for dev in devices.iter() {
        println!("{}", dev);
    }
    println!("=======================\n");

    log!("initializing ata", || ata_init(&devices)).expect("ATA ERROR");
    log!("initializing ext2", ext2_init);
    log!("initializing tty", tty_init);

//    read_file_to("/dev/hda2", |x| if x != 0 {print!("{x:#x} ")}).unwrap();

    log!("mounting /", || {
        VFS.mount("/", "ext2", "/dev/hda2").expect("Could not mount '/': {e}");
        match VFS.mount("/mnt", "ext2", "/dev/hdb1") {
            Ok(_) => {},
            Err(e) => println!("Could not mount '/mnt': {e}"),
        }
    });

    tree("/root", 0).expect("Tree error");

    println!("stack: {:p} - {:p}", unsafe{&stack_bottom}, unsafe{&stack_top});
    println!("kernel: {:#x} - {:#x}", kbase, ktop);

    match log!("initializing sound blaster 16", || drivers::soundblaster16::init_sb16()) {
        Ok(_) => {},
        Err(e) => {
            println!("Could not initialize sound blaster 16: {e}");
        },
    };

    log!("initializing tasking", || init_tasking(unsafe{&stack_bottom as *const _ as u64}, iframes));
    let (entry, rsp) = match log!("loading /init", || drivers::elf::elf_load("/usr/bin/shell", &["/init"])) {
        Ok(x) => x,
        Err(e) => {
            println!("Could not load /init: {e}");
            return;
        },
    };
    log!("initializing syscalls", user::syscall::init_syscalls);
    log!("switching to usermode", || user::switch_user_mode(entry, rsp));

    println!("\n============--- KERNEL END ---===========");
}

#[allow(unused)]
fn read_file_to(path: &str, target: fn(u8)) -> ErrnoOr<()> {
    let mut x = drivers::fs::open(path, O_RDONLY, 0)?;
    
    let mut buf = [b'\0'];
    loop {
        let v = x.read(&mut buf);
        match v {
            Err(x) => break Err(x),
            Ok(0) => break Ok(()),
            Ok(_) => {
                target(buf[0]);
            },
        }
    }
}

#[allow(unused)]
fn list_contents(path: &str) -> ErrnoOr<String> {
    let x = drivers::fs::open(path, O_RDONLY | O_DIRECTORY, 0)?;
    let x = readdir(&x)?
        .iter()
        .map(|(a, _)| a)
        .collect::<Vec<_>>()
        .join(" ");
    
    Ok(x)
}

fn tree(path: &str, indent: usize) -> ErrnoOr<()> {
    let path = path.to_string();
    let print_indent = |indent: usize| {
        if indent != 0 {
            for _ in 0 .. (indent-1) {
                print!("|   ");
            }
            print!("|-- ");
        }
    };
    print_indent(indent);
    println!("{}/", path
             .chars()
             .rev()
             .skip_while(|x| *x == '/')
             .take_while(|x| *x != '/')
             .collect::<Vec<_>>()
             .into_iter().rev().collect::<String>());

    let conts = drivers::fs::open(&path, O_RDONLY | O_DIRECTORY, 0)?;
    for (a, t) in readdir(&conts)?.iter() {
        if a == "." || a == ".." || a.is_empty() {
            continue;
        }

        if t == FileType::Dir {
            let p = path.clone() + &a + "/";
            tree(&p, indent + 1)?;
        } else {
            print_indent(indent + 1);
            println!("{a}");
        }
    }
    Ok(())
}
