use core::{fmt::Write, arch::asm};
use alloc::string::ToString;

use crate::{video::DISPLAY, drivers::debugcon::DebugCon};

#[derive(Clone, Copy)]
pub struct SizedStr<const S: usize>([u8; S]);
impl<const S: usize> core::fmt::Debug for SizedStr<S> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "\"{}\"", self.to_string())
    }
}
impl<const S: usize> core::fmt::Display for SizedStr<S> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}", core::str::from_utf8(&self.0).unwrap())
    }
}

#[derive(Clone, Copy)]
pub struct Unused<const S: usize>([u8; S]);
impl<const S: usize> core::fmt::Debug for Unused<S> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "(unused)")
    }
}

#[derive(Clone, Copy)]
pub struct SizedCStr<const S: usize>([u8; S]);
impl<const S: usize> core::fmt::Debug for SizedCStr<S> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "\"{}\"", self.to_string())
    }
}
impl<const S: usize> core::fmt::Display for SizedCStr<S> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let idx = self.0.iter().take_while(|x| **x != b'\0').count();
        write!(f, "{}", core::str::from_utf8(&self.0[..idx]).unwrap())
    }
}


#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => {{
        use core::fmt::Write;
        write!($crate::video::DISPLAY.write(), $($arg)*).unwrap()
    }};
}

#[macro_export]
macro_rules! println {
    ($($arg:tt)*) => {{
        use core::fmt::Write;
        let x = writeln!($crate::video::DISPLAY.write(), $($arg)*).unwrap();
        x
    }};
}


#[panic_handler]
pub extern fn panic(i: &core::panic::PanicInfo) -> ! {
    let _ = writeln!(DebugCon, "{i}");
    unsafe {DISPLAY.drop_lock()};
    
    let _ = writeln!(DISPLAY.write(), "{i}");
    unsafe {asm!("cli; hlt")};
    unreachable!();
}

#[macro_export]
macro_rules! dbg {
    ($e:expr) => {
        println!("{}: {}", stringify!($e), $e)
    };
}
