use core::fmt::Write;
use core::arch::{asm, global_asm};
use alloc::{vec, vec::Vec};

use crate::drivers::debugcon::DebugCon;
use crate::drivers::elf::elf_load;
use crate::drivers::fs::{FileType, Fd};
use crate::mem::Virtual;
use crate::mem::paging::{ActivePageTable, PAGE_SIZE, Page, PTEntry};
use crate::task::{writefd, fork, collect_child, remove_child, readfd, openfd, readdirfd, getfd, setfd, switch_task};
use crate::time::Timespec;
use crate::video::DISPLAY;
use crate::{task, println};
use crate::user::USERMODE_HEAP_START;
use crate::utils::errno::{ErrnoOr, Errno};
use crate::stack_top;

const  STAR:  usize = 0xC000_0081;
const LSTAR:  usize = 0xC000_0082;
const SFMASK: usize = 0xC000_0084;

#[inline(never)]
pub fn init_syscalls() {
    // enable syscall/sysret
    unsafe{asm!(
        "mov rcx, 0xc0000080",
        "rdmsr",
        "or eax, 1",
        "wrmsr",
        )};

    unsafe{asm!("wrmsr", in("rcx") LSTAR, in("eax") handle_syscall as usize, in("edx") (handle_syscall as usize) >> 32)};
    unsafe{asm!("wrmsr", in("rcx") SFMASK, in("eax") 0x00, in("edx") 0x00)};
    unsafe{asm!("wrmsr", in("rcx") STAR, in("edx") 0x0020_0008usize, in("eax") 0x00)};
}

#[derive(Debug)]
#[repr(C)]
struct SyscallRegs {
    r15: u64,
    r14: u64,
    r13: u64,
    r12: u64,
    r11: u64,
    r10: u64,
    r9:  u64,
    r8:  u64,
    rdi: u64,
    rsi: u64,
    rbp: u64,
    rdx: u64,
    user_rip: u64,
    rbx: u64,
    user_rsp: u64,
}


global_asm!("handle_syscall:
            push rcx
            mov rcx, rsp
            lea rsp, [{st}-8]
//            sub rsp, 8 // align rsp, 16-bit aligned at call
            push rcx // old rsp
            mov rcx, [rcx]
            push rbx
            push rcx
            push rdx
            push rbp
            push rsi
            push rdi
            push r8
            push r9
            push r10
            push r11
            push r12
            push r13
            push r14
            push r15

            push rsp
            push r9

            // arguments to cdecl
            mov r9, r8
            mov r8, r10
            mov rcx, rdx
            mov rdx, rsi
            mov rsi, rdi
            mov rdi, rax
            
            call {handler}
            // end arguments

            add rsp, 16

            pop r15
            pop r14
            pop r13
            pop r12
            pop r11
            pop r10
            pop r9
            pop r8
            pop rdi
            pop rsi
            pop rbp
            pop rdx
            pop rcx
            pop rbx
            pop rsp
            sysretq", handler = sym __syscall_handler, st = sym stack_top);

extern "C" {
    fn handle_syscall();
}

extern "C" fn __syscall_handler(num: usize, arg0: usize, arg1: usize, arg2: usize, arg3: usize, arg4: usize, arg5: usize, regs: &mut SyscallRegs) -> isize {
    writeln!(DebugCon, "syscall[{num:#x}]({arg0:#x}, {arg1:#x}, {arg2:#x}, {arg3:#x}, {arg4:#x}, {arg5:#x})").unwrap();

    let res = syscall_handler(num, [arg0, arg1, arg2, arg3, arg4, arg5], regs);
    match res {
        ErrnoOr::Ok(val) => {
            writeln!(DebugCon, "syscall[{num:#x}] returned {val:#x}", num = num, val = val).unwrap();
            val as isize
        },
        ErrnoOr::Err(err) => {
            writeln!(DebugCon, "syscall[{num:#x}] returned error \"{err}\"").unwrap();
            -(err as isize)
        }
    }
}

pub const SYS_READ: usize = 0;
pub const SYS_WRITE: usize = 1;
pub const SYS_OPEN: usize = 2;
pub const SYS_MMAP: usize = 9;
pub const SYS_MUNMAP: usize = 11;
pub const SYS_NANOSLEEP: usize = 35;
pub const SYS_FORK: usize = 57;
pub const SYS_EXECVE: usize = 59;
pub const SYS_EXIT: usize = 60;
pub const SYS_WAIT4: usize = 61;
pub const SYS_GETDENTS: usize = 78;
pub const SYS_FSTATAT: usize = 262;
pub const SYS_RENDERBUF: usize = 1001;

fn syscall_handler(num: usize, args: [usize; 6], regs: &mut SyscallRegs) -> ErrnoOr<usize> {
    match num {
        SYS_MMAP => sys_mmap(args[0], args[1], args[2] as u8, args[3], args[4] as isize, args[5]),
        SYS_MUNMAP => sys_munmap(args[0], args[1]),
        SYS_READ => sys_read(args[0], args[1] as *mut u8, args[2]),
        SYS_WRITE => sys_write(args[0], args[1] as *const u8, args[2]),
        SYS_FORK => sys_fork(),
        SYS_EXIT => task::exit_current(args[0] as u8),
        SYS_WAIT4 => sys_wait4(args[0] as isize, args[1] as *mut isize, args[2] as isize, args[3] as *mut usize),
        SYS_EXECVE => sys_execve(args[0] as *const u8, args[1] as *const *const u8, args[2] as *const *const u8, regs),
        SYS_OPEN => sys_open(args[0] as *const u8, args[1], args[2]),
        SYS_GETDENTS => sys_getdents(args[0], args[1] as *mut u8, args[2]),
        SYS_FSTATAT => sys_fstatat(args[0] as isize, args[1] as *const u8, args[2] as *mut Stat, args[3] as usize),
        SYS_RENDERBUF => sys_renderbuf(args[0] as *mut u8),
        SYS_NANOSLEEP => {
            crate::time::sleep(unsafe{&*(args[0] as *const Timespec)});
            Ok(0)
        },
        _ => {
            writeln!(DebugCon, "syscall[{num:#x}] not implemented").unwrap();
            todo!();
//            Err(Errno::ENOSYS)
        }
    }
}

fn sys_mmap(addr: usize, len: usize, prot: u8, flags: usize, fd: isize, offset: usize) -> ErrnoOr<usize> {
    const MAP_SHARED: usize = 0x01;
    const MAP_PRIVATE: usize = 0x02;
    const MAP_FIXED: usize = 0x10;
    const MAP_ANONYMOUS: usize = 0x20;

    use crate::task::{PROT_READ, PROT_WRITE, PROT_EXEC};

    writeln!(DebugCon, "mmap({:#x}, {}, {:#x}, {:#x}, {}, {:#x})", addr, len, prot, flags, fd, offset).unwrap();

    if len == 0 {
        return Err(Errno::EINVAL);
    }

    if addr != 0 {
        writeln!(DebugCon, "mmap: addr != 0 // TODO").unwrap();
        return Err(Errno::ENOSYS);
    }

    if flags & MAP_ANONYMOUS == 0 {
        if fd == -1 {
            writeln!(DebugCon, "mmap: fd == -1, but anonymous not given").unwrap();
            return Err(Errno::EINVAL);
        } else {
            writeln!(DebugCon, "file-mmap: fd != -1 // TODO").unwrap();
            return Err(Errno::ENOSYS);
        }
    } else if fd != -1 {
        writeln!(DebugCon, "mmap: fd != -1, but anonymous given").unwrap();
        return Err(Errno::EINVAL);
    }

    if (flags & MAP_SHARED != 0) == (flags & MAP_PRIVATE != 0) {
        writeln!(DebugCon, "mmap: MAP_SHARED and MAP_PRIVATE, or none given").unwrap();
        return Err(Errno::EINVAL);
    }

    let is_shared = flags & MAP_SHARED != 0;
    if flags & MAP_FIXED != 0 {
        writeln!(DebugCon, "mmap: MAP_FIXED // TODO").unwrap();
        return Err(Errno::ENOTSUP);
    }
    
    if is_shared {
        writeln!(DebugCon, "mmap: shared // TODO").unwrap();
        return Err(Errno::ENOTSUP);
    }

    let _read = prot & PROT_READ != 0;
    let write = prot & PROT_WRITE != 0;
    let exec = prot & PROT_EXEC != 0;

    let pages = (len - 1) / 0x1000 + 1;
    let search_start = USERMODE_HEAP_START as usize;
    let mut pt = ActivePageTable::get();

    let mut search = search_start;
    let mut found_addr = search_start;
    let mut found = 0;
    loop {
        if (search >> 38) & 0b111111111 == 511 {
            writeln!(DebugCon, "mmap: no free space").unwrap();
            return Err(Errno::ENOMEM);
        }
        if pt.translate(Virtual(search)).is_none() {
            search += PAGE_SIZE;
            found += 1;
        } else {
            found = 0;
            search += PAGE_SIZE;
            found_addr = search;
            continue;
        }
        if found == pages {
            break;
        }
    }

    task::add_mmap_area(found_addr, found_addr+len, prot, true, None)?;

    let mut pflags = 0;
    if write {
        pflags |= PTEntry::WRITABLE;
    }
    if ! exec {
        pflags |= PTEntry::NO_EXECUTE;
    }

    for a in (found_addr..found_addr+len).step_by(PAGE_SIZE) {
        pt.map(Page::from_address(Virtual(a)), PTEntry::USER_ACCESSIBLE | pflags, false);
    }

    Ok(found_addr)
}

macro_rules! __check_ptr {
    ($p:expr) => {{
        use super::USERMODE_CODE_START;
        use crate::mem::alloc::HEAP_START;
        if ($p as usize) < (USERMODE_CODE_START as usize) || 
            ($p as usize) >= (HEAP_START as usize) {
                true
            } else {
                ActivePageTable::get().translate(Virtual($p as usize)).is_none()
            }
    }};
}

macro_rules! check_ptr {
    ($p:expr) => {{
        if __check_ptr!($p) {
            return Err(Errno::EFAULT);
        }
    }};
}

macro_rules! check_ptr_allow_null {
    ($p:expr) => {
        if __check_ptr!($p) && $p as usize != 0 {
            use crate::println;
            println!("ptr {:#x} is invalid", $p as usize);
            return Err(Errno::EFAULT);
        }
    };
}

fn sys_munmap(addr: usize, len: usize) -> ErrnoOr<usize> {
    check_ptr!(addr);
    check_ptr!(addr + len - 1);

    let areas = task::get_mmap_areas();
    let mut to_remove = vec![];
    for area in areas {
        for a in (addr..addr+len).step_by(PAGE_SIZE) {
            if ! area.user_alloc {
                continue;
            }

            if area.start <= a && a < area.end {
                if area.prot & task::PROT_WRITE == 0 {
                    return Err(Errno::EPERM);
                } else {
                    let mut pt = ActivePageTable::get();
                    pt.unmap(Page::from_address(Virtual(a)), true);
                    to_remove.push(area);
                    break;
                }
            }
        }
    }

    for a in to_remove {
        task::remove_mmap_area(a);
    }

    Ok(0)
}

fn sys_read(fd: usize, ptr: *mut u8, len: usize) -> ErrnoOr<usize> {
    // allow interrupts while reading
    check_ptr!(ptr);

    let buf = unsafe{core::slice::from_raw_parts_mut(ptr, len)}; //vec![0; len];
    let read = readfd(fd, buf)?;
    Ok(read)
}

fn sys_write(fd: usize, ptr: *const u8, len: usize) -> ErrnoOr<usize> {
    // allow interrupts while writing
//    unsafe{asm!("sti")};
    check_ptr!(ptr);

    let mut buf = vec![0; len];
    unsafe {
        core::ptr::copy(ptr, buf.as_mut_ptr(), len);
    }
    
    writefd(fd, &buf)
}

fn sys_fork() -> ErrnoOr<usize> {
    unsafe{asm!("cli")};
    let x = fork();
//    unsafe{asm!("sti")};
    Ok(x)
}

#[inline(never)]
fn sys_wait4(pid: isize, status: *mut isize, options: isize, rusage: *mut usize) -> ErrnoOr<usize> {
    unsafe{asm!("cli")};
    check_ptr_allow_null!(status);
    check_ptr_allow_null!(rusage);

    pub const WNOHANG: isize = 0x01;
    pub const WUNTRACED: isize = 0x02;

    if options & (WNOHANG | WUNTRACED) != 0 {
        return Err(Errno::ENOTSUP);
    }

    if rusage as usize != 0 {
        return Err(Errno::ENOTSUP);
    }

    if let Some((child_status, cpid)) = collect_child(pid)? {
        if status as usize != 0 {
            unsafe{status.write_unaligned(child_status as isize)};
        }
        Ok(cpid)
    } else {
        task::set_current_waiting(pid);

        loop {
            switch_task();
            if let Some((child_status, cpid)) = collect_child(pid)? {
                remove_child(cpid);
                if status as usize != 0 {
                    unsafe{status.write_unaligned(child_status as isize)};
                }
//                unsafe{asm!("sti")};
                break Ok(cpid);
            } else {
                continue;
            }
        }
    }
}

fn sys_execve(filename: *const u8, argv: *const *const u8, _envp: *const *const u8, regs: &mut SyscallRegs) -> ErrnoOr<usize> {
    check_ptr!(filename);

    let mut args = vec![];
    let mut i = 0;
    loop {
        let arg = unsafe{argv.add(i)};
        if unsafe{*arg} as usize == 0 {
            break;
        }
        check_ptr_allow_null!(arg);
        
        let mut arg_vec = vec![];
        let mut arg_len = 0;
        loop {
            let c = unsafe{*(*arg).add(arg_len)};
            if c as usize == 0 {
                break;
            }
            arg_len += 1;
            arg_vec.push(c);
        }

        args.push(arg_vec);

        i += 1;
    }

    /*let mut envs = vec![];
    let mut i = 0;
    loop {
        let env = unsafe{envp.add(i)};
        if unsafe{*env} as usize == 0 {
            break;
        }
        check_ptr_allow_null!(env);

        let mut env_len = 0;
        let mut env_vec = vec![];
        loop {
            let c = unsafe{*(*env).add(env_len)};
            if c as usize == 0 {
                break;
            }
            env_len += 1;
            env_vec.push(c);
        }

        envs.push(env_vec);

        i += 1;
    }*/

    let mut prog_name_slice = vec![];
    let mut prog_name_len = 0;
    loop {
        let c = unsafe{*filename.add(prog_name_len)};
        if c as usize == 0 {
            break;
        }
        prog_name_len += 1;
        prog_name_slice.push(c);
    }

    let name = unsafe{core::str::from_utf8_unchecked(&prog_name_slice)};
    println!("execve: {}", name);

    let args = args.iter().map(|x| unsafe{core::str::from_utf8_unchecked(x)}).collect::<Vec<_>>();
//    writeln!(DebugCon, "execve: {:?} {:?}", name, args).unwrap();
    let (rip, rsp) = elf_load(name, &args)?;

    regs.user_rip = rip as u64;
    regs.user_rsp = rsp as u64;
    Ok(0)
}

fn sys_open(filename: *const u8, flags: usize, mode: usize) -> ErrnoOr<usize> {
    check_ptr!(filename);

    if flags & 0b11 == 0b11 {
        return Err(Errno::EINVAL);
    }

    let mut filename_slice = vec![];
    let mut filename_len = 0;
    loop {
        let c = unsafe{*filename.add(filename_len)};
        if c as usize == 0 {
            break;
        }
        filename_len += 1;
        filename_slice.push(c);
    }
    let fname = unsafe{core::str::from_utf8_unchecked(&filename_slice)};
    let fd = openfd(fname, flags, mode)?;
    Ok(fd)
}

#[repr(C)]
struct Dirent {
    d_ino: u64,
    d_off: i64,
    d_reclen: u16,
    // d_name: [u8..],
    // d_type: u8,
}

const DIRENT_LEN: usize = 8 + 8 + 2;

fn sys_getdents(fd: usize, buf: *mut u8, len: usize) -> ErrnoOr<usize> {
    #[allow(unused)]
    const DT_UNKNOWN: u8 = 0;
    const DT_FIFO: u8 = 1;
    const DT_CHR: u8 = 2;
    const DT_DIR: u8 = 4;
    const DT_BLK: u8 = 6;
    const DT_REG: u8 = 8;
    #[allow(unused)]
    const DT_LNK: u8 = 10;
    const DT_SOCK: u8 = 12;
    #[allow(unused)]
    const DT_WHT: u8 = 14;

    check_ptr!(buf);

    let diroff = getfd(fd)?.diroff;

    let mut read = 0;
    for (i, a) in readdirfd(fd)?.iter().skip(diroff).enumerate() {
        let needed_len = DIRENT_LEN + a.0.len() + 1 /* null terminator */ + 1 /* d_type */;
        if read + needed_len > len {
            if read == 0 {
                return Err(Errno::EINVAL);
            } else {
                break;
            }
        }

        unsafe {
            let ptr = buf.add(read);
            let dirent = ptr as *mut Dirent;
            (*dirent).d_ino = i as u64;
            (*dirent).d_off = 0;
            (*dirent).d_reclen = needed_len as u16;
            
            core::ptr::copy_nonoverlapping(a.0.as_ptr(), ptr.add(DIRENT_LEN), a.0.len());
            *ptr.add(DIRENT_LEN + a.0.len()) = 0;
            *ptr.add(DIRENT_LEN + a.0.len() + 1) = match a.1 {
                FileType::File => DT_REG,
                FileType::Dir => DT_DIR,
                FileType::Fifo => DT_FIFO,
                FileType::Socket => DT_SOCK,
                FileType::CharDev(..) => DT_CHR,
                FileType::BlockDev(..) => DT_BLK,
            };
        }
        let mut ifd = getfd(fd)?;
        ifd.diroff += 1;
        setfd(fd, ifd)?;

        read += needed_len;
    }

    Ok(read)
}

fn sys_renderbuf(buf: *const u8) -> ErrnoOr<usize> {
    check_ptr!(buf);

    DISPLAY.write().console().render_from_buf(buf);
    Ok(0)
}

#[repr(C)]
struct Stat {
    st_dev: usize,
    st_ino: usize,
    st_nl: usize,

    st_mode: u32,
    st_uid: u32,
    st_gid: u32,
    st_pad0: u32,

    st_rdev: usize,
    st_size: isize,
    st_blksize: isize,
    st_blocks: isize,

    st_atime: usize,
    st_atime_nsec: usize,
    st_mtime: usize,
    st_mtime_nsec: usize,
    st_ctime: usize,
    st_ctime_nsec: usize,

    unused: [usize; 3],
}

fn sys_fstatat(fd: isize, _fname: *const u8, stat: *mut Stat, _flags: usize) -> ErrnoOr<usize> {
    check_ptr!(stat);

    if fd == -100 {
        return Err(Errno::ENOSYS);
    }

    let fd = getfd(fd as usize)?;
    unsafe {
        (*stat) = Stat {
            st_dev: 0,
            st_ino: 0,
            st_nl: 1,
            st_mode: 0,
            st_uid: 0,
            st_gid: 0,
            st_pad0: 0,
            st_rdev: 0,
            st_size: fd.len() as isize,
            st_blksize: 0,
            st_blocks: 0,
            st_atime: 0,
            st_atime_nsec: 0,
            st_mtime: 0,
            st_mtime_nsec: 0,
            st_ctime: 0,
            st_ctime_nsec: 0,
            unused: [0; 3],
        };
    }
    Ok(0)
}
