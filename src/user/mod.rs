pub mod syscall;

use core::arch::asm;

pub const USERMODE_CODE_START: u64 = 0x0000_5555_5555_5000;
pub const USERMODE_STACK_START: u64 = 0x0000_7fff_ffff_f000;
pub const USERMODE_STACK_SIZE: u64 = 0x100000 * 16;
pub const USERMODE_HEAP_START: u64 = 0x0000_6666_0000_0000;

#[inline(never)]
pub extern "C" fn user_entry() {
    unsafe{asm!("mov rax, 0x42",
                "mov rdi, 0xdeadbeef",
                "mov rsi, 0xcafebabe",
                "mov rdx, 0x42424242",
                "mov r10, 0x69696969",
                "mov r8, 0x420",
                "mov r9, 0x69",
                "syscall")};
}

#[inline(never)]
pub fn switch_user_mode(entry: usize, rsp: usize) {
//    let code_page = Page::from_address(mem::Virtual(USERMODE_CODE_START as usize));

//    let code_frame = alloc_frame();
//    let mut tbl = ActivePageTable::get();
/*    tbl.map_to_addr(code_page,
        code_frame, PTEntry::USER_ACCESSIBLE | PTEntry::WRITABLE, false, false);*/
/*
    let frame = alloc_frame();
    tbl.map_to_addr(Page::from_address(mem::Virtual(USERMODE_STACK_START as usize)),
        frame, PTEntry::USER_ACCESSIBLE | PTEntry::WRITABLE, false, false);

    unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};*/
/*
    let ptr = user_entry as usize;
    unsafe{ asm!("rep movsb", in("rsi") ptr, in("rdi") USERMODE_CODE_START, in("rcx") 0x1000); }

    tbl.unmap(code_page, false);
    tbl.map_to_addr(code_page, code_frame, PTEntry::USER_ACCESSIBLE, false, false);
    unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};
*/
    unsafe{
        asm!( "mov rcx, {code}",
              "mov r11, 0x202",
              "mov rsp, {stack}",
              "sysretq",
              stack = in(reg) rsp,
              code = in(reg) entry);
    }   
}

