pub type ErrnoOr<T> = Result<T, Errno>;

#[repr(u8)]
#[derive(Debug)]
pub enum Errno {
    SUCCESS = 0,
    EPERM = 1,
    ENOENT = 2,
    ESRCH = 3,
    EIO = 5,
    ENXIO = 6,
    ENOEXEC = 8,
    EBADF = 9,
    ECHILD = 10,
    ENOMEM = 12,
    EACCES = 13,
    EFAULT = 14,
    ENODEV = 19,
    ENOTDIR = 20,
    EISDIR = 21,
    EINVAL = 22,
    EROFS = 30,
    ENOSYS = 38,
    ENOTSUP = 95,
}


impl core::fmt::Display for Errno {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        use Errno::*;
        write!(f, "{}", match self {
            SUCCESS => "Success",
            EPERM => "Operation not permitted",
            ENOENT => "No such file or directory",
            ESRCH => "No such process",
            EIO => "Input/output error",
            ENXIO => "No such device or address",
            ENOMEM => "Cannot allocate memory",
            ENOEXEC => "Exec format error",
            EBADF => "Bad file descriptor",
            ECHILD => "No child process",
            EACCES => "Permission denied",
            EFAULT => "Bad address",
            ENODEV => "No such device",
            ENOTDIR => "Not a directory",
            EISDIR => "Is a directory",
            EINVAL => "Invalid argument",
            EROFS => "Read-only file system",
            ENOSYS => "Function not implemented",
            ENOTSUP => "Operation not supported",
        })
    }
}
