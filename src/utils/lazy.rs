use core::{ops::Deref, cell::Cell};

pub struct Lazy<T> {
    f: fn() -> T,
    v: Cell<Option<T>>
}

unsafe impl<T> Sync for Lazy<T> {}

impl<T> Lazy<T> {
    #[allow(unused)]
    pub const fn new(f: fn() -> T) -> Self {
        Self {
            f,
            v: Cell::new(None),
        }
    }
}

impl<T> Deref for Lazy<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        let v = self.v.as_ptr();
        unsafe {
            if (*v).is_none() {
                *v = Some((self.f)())
            }
            (*v).as_ref().unwrap()
        }
    }
}

#[macro_export]
macro_rules! lazy {
    ($e:expr) => {
        $crate::utils::lazy::Lazy::new(|| $e)
    };
}
