use core::{cell::UnsafeCell, ptr::{read_volatile, write_volatile},
           ops::{DerefMut, Deref}, arch::asm};

pub struct Lock<T> {
    val: UnsafeCell<T>,
    locked_read: UnsafeCell<bool>,
    locked_write: UnsafeCell<bool>,
}
impl<T> core::fmt::Debug for Lock<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "(lock)")
    }
}

unsafe impl<T> Sync for Lock<T> {}

impl<T> Lock<T> {
    pub const fn new(v: T) -> Self {
        Self {
            val: UnsafeCell::new(v),
            locked_read: UnsafeCell::new(false),
            locked_write: UnsafeCell::new(false),
        }
    }

    #[inline(always)]
    pub fn read(&self) -> _ReadGuard<T> {
        loop {
            if ! unsafe {read_volatile(self.locked_write.get())} {
                break;
            }
            unsafe{asm!("pause")};
        }

        unsafe{write_volatile(self.locked_read.get(), true)};
        _ReadGuard {
            inner: self,
        }
    }

    #[inline(never)]
    pub fn write(&self) -> _WriteGuard<T> {
        loop {
            if ! unsafe {read_volatile(self.locked_write.get())
                         || read_volatile(self.locked_read.get())} {
                break;
            }
            unsafe{asm!("pause")};
//            switch_task();
        }

        unsafe{write_volatile(self.locked_write.get(), true)};
        _WriteGuard {
            inner: self,
        }
    }

    /// Drops the read and write lock, useful in emergency situaltions, like panic!()
    /// # Safety
    /// This is unsafe, since it allows data races
    pub unsafe fn drop_lock(&self) {
        write_volatile(self.locked_read.get(), false);
        write_volatile(self.locked_write.get(), false);
    }

    pub fn write_avail(&self) -> bool {
        unsafe{! read_volatile(self.locked_write.get())}
    }
}

pub struct _ReadGuard<'a, T> {
    inner: &'a Lock<T>,
}

impl<T: core::fmt::Debug> core::fmt::Debug for _ReadGuard<'_, T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        (**self).fmt(f)
    }
}

impl<T> Drop for _ReadGuard<'_, T> {
    fn drop(&mut self) {
        unsafe{write_volatile(self.inner.locked_read.get(), false)};
    }
}

impl<'a, T> Deref for _ReadGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &'a Self::Target {
        unsafe{&*self.inner.val.get()}
    }
}



pub struct _WriteGuard<'a, T> {
    inner: &'a Lock<T>,
}

impl<T: core::fmt::Debug> core::fmt::Debug for _WriteGuard<'_, T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        (**self).fmt(f)
    }
}

impl<T> Drop for _WriteGuard<'_, T> {
    fn drop(&mut self) {
        unsafe{write_volatile(self.inner.locked_write.get(), false)};
    }
}

impl<'a, T> Deref for _WriteGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &'a Self::Target {
        unsafe{&*self.inner.val.get()}
    }
}

impl<'a, T> DerefMut for _WriteGuard<'a, T> {
    fn deref_mut(&mut self) -> &'a mut Self::Target {
        unsafe{&mut *self.inner.val.get()}
    }
}
