use crate::io::ports::IOPort;
use super::{DisplayInfo, DISPLAY, Display, write::{Console, Color}};

fn get_cursor() -> u16 {
    let mut pos = 0;
    unsafe {
        0x0fu8.pout(0x3d4);
        pos |= u8::pin(0x3d5) as u16;
        0x0eu8.pout(0x3d4);
        pos |= (u8::pin(0x3D5) as u16) << 8;
    }
    pos
}

pub fn setup_vga(info: &DisplayInfo) {
    let curs = get_cursor() as i32;
    let vga_state = VgaState {
        addr: info.address,
        cols: info.width as usize,
        rows: info.height as usize,
        cur: (curs as usize % 80, curs as usize / 80),
    };

    *DISPLAY.write() = Display::Vga(vga_state);
}

fn update_cursor(c: usize) {
    let pos = c as u16;
    unsafe {
        0x0Fu8.pout(0x3D4);
        ((pos & 0xFF) as u8).pout(0x3D5);
        0x0Eu8.pout(0x3D4);
        (((pos >> 8) & 0xFF) as u8).pout(0x3D5);
    }
}

#[derive(Debug)]
pub struct VgaState {
    pub addr: usize,
    pub cols: usize,
    pub rows: usize,
    pub cur: (usize, usize)
}

fn color_to_vga(c: Color) -> u8 {
    match c {
        Color::Black => 0x0,
        Color::Red => todo!(),
        Color::Green => todo!(),
        Color::Yellow => todo!(),
        Color::Blue => todo!(),
        Color::Magenta => todo!(),
        Color::Cyan => todo!(),
        Color::White => 0xf,
    }
}

impl Console for VgaState {
    fn get_cur(&mut self) -> (usize, usize) {
        self.cur
    }

    fn get_cols(&self) -> usize {
        self.cols
    }

    fn get_rows(&self) -> usize {
        self.rows
    }

    fn write_at(&mut self, cur: (usize, usize), byte: u8, fg: super::write::Color, bg: super::write::Color) {
        unsafe {
            let vga_ptr = (self.addr as *mut u16).add(cur.0 + self.cols * cur.1);
            *vga_ptr = (color_to_vga(bg) as u16) << 12 | (color_to_vga(fg) as u16) << 8 | byte as u16;
        }
    }

    fn advance_line_scroll(&mut self) {
        todo!()
    }

    fn flush_and_update_cursor(&mut self, cur: (usize, usize)) {
        update_cursor(cur.0 + self.cols * cur.1);
        self.cur = cur;
    }

    fn render_from_buf(&mut self, buf: *const u8) {
        unsafe {
            for i in 0..self.cols * self.rows {
                let vga_ptr = (self.addr as *mut u16).add(i);
                *vga_ptr = (color_to_vga(Color::White) as u16) << 12 | (color_to_vga(Color::Black) as u16) << 8 | *buf.add(i) as u16;
            }
        }
    }
}
