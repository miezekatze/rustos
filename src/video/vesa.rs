use core::fmt::Write;

use crate::{video::{Display, DISPLAY}, mem::{paging::{alloc_page, PAGE_SIZE}, Virtual}, drivers::debugcon::DebugCon};
use super::{DisplayInfo, write::{Console, Color}};

#[derive(Debug)]
#[repr(C)]
struct PSFHeader {
    magic: u32,
    version: u32,
    headersize: u32,
    flags: u32,
    numglyph: u32,
    bytesperglyph: u32,
    height: u32,
    width: u32,
}

pub fn setup_vesa(info: &DisplayInfo) {
    assert_eq!(info.bpp, 32);
    assert_eq!(info.width * 4, info.pitch);

    let font_header = unsafe {(&_binary_font_psfu_start as *const _ as *const PSFHeader).read_unaligned()};
    let _ = writeln!(DebugCon, "font: {font_header:#?}");
    assert_eq!(font_header.width, 8);
    assert_eq!(font_header.height, 16);

    let state = VesaState {
        addr: info.address,
        width: info.width as usize,
        height: info.height as usize,
        pitch: info.pitch as usize,
        bpp: info.bpp as usize,
        twidth: 8,
        theight: 16,
        tcur: (0, 0),
        bpg: 16,
        glyphs: font_header.numglyph as usize,
        font: unsafe {(&_binary_font_psfu_start as *const i8).add(font_header.headersize as usize) as *const _},
        buf: None,
    };

    *DISPLAY.write() = Display::Vesa(state);
}

#[derive(Debug)]
pub struct VesaState {
    pub addr: usize,
    pub width: usize,
    pub height: usize,
    pub pitch: usize,
    pub bpp: usize,
    pub twidth: usize,
    pub theight: usize,
    pub bpg: usize,
    pub tcur: (usize, usize),
    pub font: *const [u8; 16],
    pub glyphs: usize,
    pub buf: Option<&'static mut [u32]>,
}

impl VesaState {
    fn get_glyph<T: Into<usize> + Copy>(&self, c: T) -> &'static [u8; 16] {
        let c_idx = if c.into()> self.glyphs { 0 } else  { c.into() };
        unsafe {&*(self.font.add(c_idx) as *const [u8; 16])}
    }

    #[inline(always)]
    fn buf(&mut self) -> &mut [u32] {
        self.buf.as_deref_mut().unwrap()
    }
}

extern "C" {
    static _binary_font_psfu_start: i8;
}

fn color_to_vesa(c: Color) -> u32 {
    match c {
        Color::Black => 0x000000,
        Color::Red => 0xff0000,
        Color::Green => 0x00ff00,
        Color::Yellow => 0xffff00,
        Color::Blue => 0x0000ff,
        Color::Magenta => 0xff00ff,
        Color::Cyan => 0x00ffff,
        Color::White => 0xffffff,
    }
} 

impl Console for VesaState {
    fn get_cur(&mut self) -> (usize, usize) {
        self.tcur
    }

    fn get_cols(&self) -> usize {
        self.width / self.twidth
    }

    fn get_rows(&self) -> usize {
        self.height / self.theight
    }

    fn write_at(&mut self, cur: (usize, usize), byte: u8, fg: super::write::Color, bg: super::write::Color) { 
        if self.buf.is_none() {
            let virt_addr = 0xffff_8000_0000_0000usize;

            for a in (0 .. self.width * self.height * 32).step_by(PAGE_SIZE) {
                alloc_page(Virtual(virt_addr + a), true, true);
                // zero page
                unsafe {core::ptr::write_bytes((virt_addr + a) as *mut u8, 0, PAGE_SIZE)};
            }
            self.buf = Some(unsafe {core::slice::from_raw_parts_mut(virt_addr as *mut u32, self.width * self.height)});
        }

        let mem = unsafe {
            core::slice::from_raw_parts_mut(self.addr as *mut u32, self.width * self.height)
        };

        let (fg, bg) = (color_to_vesa(fg), color_to_vesa(bg));
        
        let (x, y) = cur;
        let glyph = self.get_glyph(byte);
        let off = self.pitch/4 * self.theight * y + x * self.twidth;
        let buf = self.buf.as_deref_mut().unwrap();
          for (rowi, row) in glyph.iter().enumerate() {
            for col in 0 .. self.twidth {
                let x = (*row & (1 << (7 - col))) != 0;
                let v = if x {fg} else {bg};
                buf[off + rowi * self.pitch/4 + col] = v;
                mem[off + rowi * self.pitch/4 + col] = v;
            }
        }
    }

    fn advance_line_scroll(&mut self) {
        let mem = unsafe {
            core::slice::from_raw_parts_mut(self.addr as *mut u32, self.width * self.height)
        };

        #[allow(clippy::needless_range_loop)]
        for a in 0 .. self.width * (self.height - self.theight) {
            let v = self.buf.as_deref_mut().unwrap()[a + self.width * self.theight];
            let oldv = self.buf()[a];

            if v != oldv {
                self.buf()[a] = v;
                mem[a] = v;
            }
        }

        #[allow(clippy::needless_range_loop)]
        for a in self.width * (self.height - self.theight) .. self.width * self.height {
            let oldv = self.buf()[a];

            if oldv != 0 {
                self.buf()[a] = 0;
                mem[a] = 0;
            }
        }
    }

    fn flush_and_update_cursor(&mut self, cur: (usize, usize)) {
        let mem = unsafe {
            core::slice::from_raw_parts_mut(self.addr as *mut u32, self.width * self.height)
        };

        // redraw character at old cursor position
        let (x, y) = self.tcur;
        let off = self.pitch/4 * self.theight * y + x * self.twidth;
        for row in 0 .. self.theight {
            for col in 0 .. self.twidth {
                let v = self.buf.as_deref_mut().unwrap()[off + row * self.pitch/4 + col];
                mem[off + row * self.pitch/4 + col] = v;
            }
        }

        self.tcur = cur;
        // render cursor
        let (x, y) = cur;
        let off = self.pitch/4 * self.theight * y + x * self.twidth;
        for row in self.theight - 3 .. self.theight - 1 {
            for col in 0 .. self.twidth {
                let v = self.buf.as_deref_mut().unwrap()[off + row * self.pitch/4 + col];
                mem[off + row * self.pitch/4 + col] = !v;
            }
        }
    }

    fn render_from_buf(&mut self, buf: *const u8) {
        let ptr = buf as *const u32;
        let mem = unsafe {
            core::slice::from_raw_parts_mut(self.addr as *mut u32, self.width * self.height)
        };
        // copy to double buffer
        let db = self.buf.as_deref_mut().unwrap();
        for i in 0 .. self.width * self.height {
            let v = unsafe {*ptr.offset(i as isize)};
            db[i] = v;
            mem[i] = v;
        }
    }
}
