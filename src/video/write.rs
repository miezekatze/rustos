#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Color {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White
}

pub trait Console {
    fn get_cur(&mut self) -> (usize, usize);
    fn get_cols(&self) -> usize;
    fn get_rows(&self) -> usize;
    fn write_at(&mut self, cur: (usize, usize), byte: u8, fg: Color, bg: Color);
    fn advance_line_scroll(&mut self);
    fn flush_and_update_cursor(&mut self, cur: (usize, usize));
    fn render_from_buf(&mut self, buf: *const u8);
}

pub fn write_bytes<T: Console>(t: &mut T, s: &[u8]) {
    // let vga_base_ptr = vga.addr as *mut u16;

    let mut cur = t.get_cur();
    for b in s {
        match b {
            b'\n' => {
                cur.0 = t.get_cols();
            },
            b'\r' => {
                cur.0 = 0;
                continue;
            },
            b'\x08' => {
                if cur.0 > 0 {
                    cur.0 -= 1;
                }
                t.write_at(cur, b' ', Color::White, Color::Black);
            },
            _ => {
                t.write_at(cur, *b, Color::White, Color::Black);
            }
        }

        if cur.0 >= t.get_cols() - 1 {
            if cur.1 >= t.get_rows() -1 {
                t.advance_line_scroll();
                cur.0 = 0;
                continue;
            }
            cur.0 = 0;
            cur.1 += 1;
        } else {
            cur.0 += 1;
        }
    }

    t.flush_and_update_cursor(cur);
}
