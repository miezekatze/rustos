pub mod vga;
pub mod vesa;
pub mod write;

use core::fmt::Write;

use crate::utils::lock::Lock;
use alloc::boxed::Box;
use vga::{setup_vga, VgaState};
use vesa::{setup_vesa, VesaState};

use self::write::{write_bytes, Console};

#[derive(Debug)]
pub enum Display {
    Vga(VgaState),
    Vesa(VesaState),
    Uninitialized
}

impl Display {
    pub fn get_address_area(&self) -> (usize, usize) {
        match self {
            Display::Vga(a) => (a.addr, a.addr + a.cols*a.rows*2),
            Display::Vesa(a) => (a.addr, a.addr + a.pitch * a.height),
            Display::Uninitialized => todo!(),
        }
    }

    pub fn write_bytes(&mut self, bytes: &[u8]) {
        match self {
            Display::Vga(a) => {
                write_bytes(a, bytes);
            },
            Display::Uninitialized => todo!(),
            Display::Vesa(a) => {
                write_bytes(a, bytes);
            },
        }
    }

    pub fn console(&mut self) -> Box<&mut dyn Console> {
        match self {
            Display::Vga(a) => Box::new(a),
            Display::Uninitialized => todo!(),
            Display::Vesa(a) => Box::new(a),
        }
    }
}

pub static DISPLAY: Lock<Display> = Lock::new(Display::Uninitialized);

pub fn setup_video(info: &DisplayInfo) {
    if info.pitch == info.width*2 {
        setup_vga(info);
    } else {
        setup_vesa(info);
    }
}

#[derive(Debug)]
pub struct DisplayInfo {
    pub address: usize,
    pub pitch: i32,
    pub width: i32,
    pub height: i32,
    pub bpp: u8,
    pub typ: u8,
}

impl DisplayInfo {
    pub fn from_ptr(s_ptr: *const ()) -> Self {
        Self {
            address: unsafe{*(s_ptr as *const usize)},
            pitch: unsafe{*(s_ptr as *const i32).add(2)},
            width: unsafe{*(s_ptr as *const i32).add(3)},
            height: unsafe{*(s_ptr as *const i32).add(4)},
            bpp: unsafe{*(s_ptr as *const u8).add(20)},
            typ: unsafe{*(s_ptr as *const u8).add(20)},
        }
    }
}

impl Write for Display {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.write_bytes(s.as_bytes());
        Ok(())
    }
}

