use core::fmt::Write;
use core::arch::global_asm;
use core::alloc::{GlobalAlloc, Layout};
use core::{marker::PhantomData, arch::asm};

use crate::drivers::debugcon::DebugCon;
use crate::drivers::pit::handle_timer_interrupt;
use crate::mem::Physical;
use crate::mem::frames::{bitset_get, alloc_frame, bitset_dec, FRAME_SIZE};
use crate::mem::paging::{PAGE_SIZE, ActivePageTable, PTEntry, PAGE_BUF, Page};
use crate::task::{STACK_PAGES, switch_task};
use crate::utils::lock::Lock;
use crate::video::DISPLAY;
use crate::{println, lazy, utils::lazy::Lazy, mem::{Virtual, alloc::ALLOCATOR}};

use super::DescriptorTablePointer;
use super::gdt::{TaskStateSegment, Gdt};
use super::pic::PIC;

pub trait HandlerFn {
    fn u64(self) -> u64;
}

macro_rules! handler_fn {
    ($($x:ty),*) => {
        $(impl HandlerFn for $x {
            fn u64(self) -> u64 {
                self as u64
            }
        })*
    }
}
handler_fn!(IntHandler, DivHandler, DivHandlerErr, extern "C" fn() -> !);

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Entry<T: HandlerFn> {
    pointer_low: u16,
    gdt_selector: u16,
    options: EntryOptions,
    pointer_middle: u16,
    pointer_high: u32,
    pub reserved: u32,
    __phantom_data: PhantomData<T>,
}

impl<T: HandlerFn> Entry<T> {
    #[inline]
    const fn empty() -> Self {
        Entry {
            gdt_selector: 0,
            pointer_low: 0,
            pointer_middle: 0,
            pointer_high: 0,
            options: EntryOptions::minimal(),
            reserved: 0,
            __phantom_data: PhantomData,
        }
    }

    #[inline]
    unsafe fn set_handler(&mut self, handler: T) -> &mut EntryOptions {
        let addr = handler.u64();

        self.pointer_low = addr as u16;
        self.pointer_middle = (addr >> 16) as u16;
        self.pointer_high = (addr >> 32) as u32;
        self.options = EntryOptions::new();

        asm!("mov {0:x}, cs", out(reg) self.gdt_selector);
        &mut self.options
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
struct EntryOptions(u16);

impl EntryOptions {
    #[inline]
    const fn minimal() -> Self {
        EntryOptions(0b1110_0000_0000)
    }

    fn new() -> Self {
        let mut opts = Self::minimal();
        opts.set_present(true);
        opts.disable_interrupts(true);
        opts
    }

    #[inline]
    pub fn set_present(&mut self, present: bool) {
        if present {
            self.0 |= 0b1000_0000_0000_0000;
        } else {
            self.0 &= 0b0111_1111_1111_1111;
        }
    }

    pub fn disable_interrupts(&mut self, disable: bool) {
        if disable {
            self.0 &= 0b1111_1110_1111_1111;
        } else {
            self.0 |= 0b0000_0001_0000_0000;
        }
    }

    pub fn set_stack_index(&mut self, index: u16) {
        self.0 |= (index+1) & 0b111;
    }
}

type IntHandler = unsafe extern "C" fn();
type DivHandler = extern "x86-interrupt" fn(frame: InterruptStackFrame) -> !;
type IntHandlerErr = unsafe extern "C" fn();
type DivHandlerErr = extern "x86-interrupt" fn(frame: InterruptStackFrame, err_code: u64) -> !;

#[repr(C, align(4096))]
#[derive(Debug)]
pub struct InterruptDescriptorTable {
    pub divide_error: Entry<IntHandler>,
    pub debug: Entry<IntHandler>,
    pub non_maskable_interrupt: Entry<IntHandler>,
    pub breakpoint: Entry<IntHandler>,
    pub overflow: Entry<IntHandler>,
    pub bound_range_exceeded: Entry<IntHandler>,
    pub invalid_opcode: Entry<IntHandler>,
    pub device_not_available: Entry<IntHandler>,
    pub double_fault: Entry<DivHandlerErr>,
    coprocessor_segment_overrun: Entry<IntHandler>,
    pub invalid_tss: Entry<IntHandlerErr>,
    pub segment_not_present: Entry<IntHandlerErr>,
    pub stack_segment_fault: Entry<IntHandlerErr>,
    pub general_protection_fault: Entry<IntHandlerErr>,
    pub page_fault: Entry<IntHandlerErr>,
    reserved_1: Entry<IntHandler>,
    pub x87_floating_point: Entry<IntHandler>,
    pub alignment_check: Entry<IntHandlerErr>,
    pub machine_check: Entry<DivHandler>,
    pub simd_floating_point: Entry<IntHandler>,
    pub virtualization: Entry<IntHandler>,
    reserved_2: [Entry<IntHandler>; 8],
    pub vmm_communication_exception: Entry<IntHandlerErr>,
    pub security_exception: Entry<IntHandlerErr>,
    reserved_3: Entry<IntHandler>,
    interrupts: [Entry<IntHandler>; 256 - 32],
}

impl InterruptDescriptorTable {
    const fn new() -> Self {
        Self {
            divide_error: Entry::empty(),
            debug: Entry::empty(),
            non_maskable_interrupt: Entry::empty(),
            breakpoint: Entry::empty(),
            overflow: Entry::empty(),
            bound_range_exceeded: Entry::empty(),
            invalid_opcode: Entry::empty(),
            device_not_available: Entry::empty(),
            double_fault: Entry::empty(),
            coprocessor_segment_overrun: Entry::empty(),
            invalid_tss: Entry::empty(),
            segment_not_present: Entry::empty(),
            stack_segment_fault: Entry::empty(),
            general_protection_fault: Entry::empty(),
            page_fault: Entry::empty(),
            reserved_1: Entry::empty(),
            x87_floating_point: Entry::empty(),
            alignment_check: Entry::empty(),
            machine_check: Entry::empty(),
            simd_floating_point: Entry::empty(),
            virtualization: Entry::empty(),
            reserved_2: [Entry::empty(); 8],
            vmm_communication_exception: Entry::empty(),
            security_exception: Entry::empty(),
            reserved_3: Entry::empty(),
            interrupts: [Entry::empty(); 256 - 32],
        }
    }
}

impl InterruptDescriptorTable {
    fn load(&'static self) {
        unsafe {asm!("lidt [{}]", in(reg) &self.pointer())};
        unsafe {asm!("sti")};
    }

    fn pointer(&'static self) -> DescriptorTablePointer {
        use core::mem::size_of;
        DescriptorTablePointer {
            limit: (size_of::<Self>() - 1) as u16,
            base: self as *const _ as u64,
        }
    }
}

#[repr(C)]
#[derive(Debug)]
pub struct InterruptStackFrame {
    pub instruction_pointer: Virtual,
    pub code_segment: u64,
    pub cpu_flags: u64,
    pub stack_pointer: Virtual,
    pub stack_segment: u64,
}

pub const PIC_OFFSET: u8 = 32;
pub static PIC: Lock<PIC> = Lock::new(PIC::new(PIC_OFFSET));

#[repr(u8)]
pub enum PICInterruptIndex {
    Timer = 0,
    Keyboard = 1,
    Dsp = 5,
    Ata = 0xe,
}

//extern "x86-interrupt" fn breakpoint(frame: InterruptStackFrame) {
//    println!("--- BREAKPOINT/TRAP ---: {frame:#x?}");
//}

#[inline(never)]
fn handle_page_fault(regs: &mut Registers, err_code: u64) {
//    println!("\n--- PAGE FAULT ({err_code}) ---\n{frame:#x?}");
    let mut pt = ActivePageTable::get();
    let addr: usize;
    unsafe{asm!("mov {r}, cr2", r = out(reg) addr)};

    let entry = pt.translate_entry(Virtual(addr));

    writeln!(DebugCon, "PAGE FAULT: {:#x} {:#x} {:?}", addr, err_code, entry).unwrap();
    if entry.map(|x| x.get_flags() & PTEntry::COPY_ON_WRITE) == Some(0) {
        let flags = entry.unwrap().get_flags();
        let mut dc = DebugCon;
        let _ = dc.write_str("PF-X\n");
        if flags & PTEntry::NO_EXECUTE != 0 {
            let _ = dc.write_str("PF-XD\n");
        }
        if flags & PTEntry::WRITABLE == 0 {
            let _ = dc.write_str("PF-RO\n");
        }
        if flags & PTEntry::USER_ACCESSIBLE == 0 {
            let _ = dc.write_str("PF-PL0\n");
        }
        unsafe{DISPLAY.drop_lock()};
        let _ = writeln!(dc, "FATAL: page fault at {:#x} with err_code {:#x}, registers: {:#x?}", addr, err_code, regs);
        panic!("FATAL: page fault at {:#x} with err_code {:#x}, registers: {:#x?}", addr, err_code, regs);
    } else if let Some(entry) = entry {
        let rc = bitset_get(entry.get_frame().unwrap().0);
        let mut flags = entry.get_flags();
        writeln!(DebugCon, "PF-RC: {:#x}", rc).unwrap();
        match rc {
            0 => {
                writeln!(DebugCon, "FATAL: frame {frame} not allocated", frame = entry.get_frame().unwrap().0).unwrap();
                unsafe{asm!("cli;hlt")};
            },
            1 => {
                flags &= !PTEntry::COPY_ON_WRITE;
                flags |= PTEntry::WRITABLE;

                let (ptr, val) = pt.map_to_addr(Page::from_address(Virtual(addr)),
                    Physical(entry.get_frame().unwrap().0 * FRAME_SIZE),
                    flags, true, false);
                unsafe{*(ptr as *mut usize) = val};
                unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};
                writeln!(DebugCon, "PF-A").unwrap();
            },
            _ => {
                flags &= !PTEntry::COPY_ON_WRITE;
                flags |= PTEntry::WRITABLE;

                let new_frame = alloc_frame();
                pt.map_to_addr(Page::from_address(Virtual(PAGE_BUF)),
                    new_frame, flags, false, false);
                unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};
                unsafe{asm!("cld;rep movsb", in("rsi") addr / PAGE_SIZE * PAGE_SIZE, in("rdi") PAGE_BUF, in("rcx") PAGE_SIZE)};
                pt.unmap(Page::from_address(Virtual(PAGE_BUF)), false);

                // map to new frame
                let (ptr, val) = pt.map_to_addr(Page::from_address(Virtual(addr)),
                    new_frame, flags, true, false);
                unsafe{*(ptr as *mut usize) = val};
                unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};

                bitset_dec(entry.get_frame().unwrap().0);
                writeln!(DebugCon, "PF-B").unwrap();
            }
        }
    } else {
        writeln!(DebugCon, "page fault: no entry at {:#x}", addr).unwrap();
        unsafe{asm!("cli;hlt")};
    }
}

extern "x86-interrupt" fn double_fault(frame: InterruptStackFrame, err_code: u64) -> ! {
    unsafe{asm!("cli;hlt")};
    println!("\n--- DOUBLE FAULT ({err_code}) ---\n{frame:#x?}");
    println!("aborting...");
    panic!("DOUBLE FAULT");
}

// extern "x86-interrupt" fn invalid_opcode(frame: InterruptStackFrame) {
//     println!("\n--- INVALID OPCODE ---\n{frame:#?}");
//     // FIXME replace this with SIGILL
//     panic!("invalid opcode");
// }

fn handle_ata_interrupt(_regs: &mut Registers) {
    PIC.write().end_of_interrupt(PICInterruptIndex::Ata as usize + 32);
}

#[derive(Debug)]
#[repr(C)]
pub struct Registers {
    ss: u64,
    rflags: u64,
    r15: u64,
    r14: u64,
    r13: u64,
    r12: u64,
    r11: u64,
    r10: u64,
    r9: u64,
    r8: u64,
    rbp: u64,
    rdi: u64,
    rsi: u64,
    rdx: u64,
    rcx: u64,
    rbx: u64,
    rax: u64,
    int_idx: u64,
    err_code: u64,

    user_rip: u64,
    user_cs: u64,
    user_rflags: u64,
    user_rsp: u64,
}

extern "C" {
    fn interrupt_stub();
    fn ata_interrupt();
    fn dsp_interrupt();
    fn keyboard_interrupt();
    fn timer_interrupt();
    fn page_fault();
}

global_asm!("interrupt_stub:
                cli
                push rax
                push rbx
                push rcx
                push rdx
                push rsi
                push rdi
                push rbp
                push r8
                push r9
                push r10
                push r11
                push r12
                push r13
                push r14
                push r15
                pushfq

                mov ax, ss
                push rax
                mov ax, 0x10
                mov ss, ax

                // move pointer to regs structure to rdi
                mov rdi, rsp

//                sub rsp, 8 // align stack
                // save mmx, sse, avx, etc. registers
                sub rsp, 512
                fxsave [rsp]

                // call interrupt handler
                call interrupt_handler

                // restore mmx, sse, avx, etc. registers
                fxrstor [rsp]
                add rsp, 512

                // restore stack
//                add rsp, 8

                pop rax
                mov ss, ax

                popfq
                pop r15
                pop r14
                pop r13
                pop r12
                pop r11
                pop r10
                pop r9
                pop r8
                pop rbp
                pop rdi
                pop rsi
                pop rdx
                pop rcx
                pop rbx
                pop rax
                
                add rsp, 16
                iretq

                ata_interrupt:
                // push error
                push 0
                // push interrupt index
                push 0x2e
                jmp interrupt_stub

                dsp_interrupt:
                // push error
                push 0
                // push interrupt index
                push 0x25
                jmp interrupt_stub

                timer_interrupt:
                // push error
                push 0
                // push interrupt index
                push 0x20
                jmp interrupt_stub

                page_fault:
                // push interrupt index
                push 0xe
                jmp interrupt_stub

                keyboard_interrupt:
                // push error
                push 0
                // push interrupt index
                push 0x21
                jmp interrupt_stub           ");

#[no_mangle]
fn interrupt_handler(regs: &mut Registers) {
    match regs.int_idx {
        0x2e => handle_ata_interrupt(regs),
        0x20 => handle_timer_interrupt(regs),
        0xe => handle_page_fault(regs, regs.err_code),
        0x21 => crate::drivers::keyboard::keyboard_interrupt(regs),
        _ => todo!("interrupt: {:#x}", regs.int_idx),
    }
}

const DOUBLE_FAULT_IST_INDEX: usize = 0;
const INT_IST_INDEX: usize = 0;
pub static IDT: Lazy<InterruptDescriptorTable> = lazy!({
    let mut idt = InterruptDescriptorTable::new();
    unsafe{
        idt.breakpoint.set_handler(interrupt_stub);
        idt.double_fault.set_handler(double_fault)
            .set_stack_index(DOUBLE_FAULT_IST_INDEX as u16);

        // idt.invalid_opcode.set_handler(invalid_opcode, EntryOptions::minimal());
        idt.page_fault.set_handler(page_fault)
            .set_stack_index(INT_IST_INDEX as u16);

        idt.interrupts[PICInterruptIndex::Timer as usize]
            .set_handler(timer_interrupt)
            .set_stack_index(INT_IST_INDEX as u16);

        idt.interrupts[PICInterruptIndex::Keyboard as usize]
            .set_handler(keyboard_interrupt)
            .set_stack_index(INT_IST_INDEX as u16);

        idt.interrupts[PICInterruptIndex::Ata as usize]
            .set_handler(ata_interrupt)
            .set_stack_index(INT_IST_INDEX as u16);
 
        idt.interrupts[PICInterruptIndex::Dsp as usize]
            .set_handler(dsp_interrupt)
            .set_stack_index(INT_IST_INDEX as u16);   }
    idt
});

pub const INT_STACK_TOP: usize = 0xffff_b000_0000_0000;

static TSS: Lazy<(TaskStateSegment, [u64; STACK_PAGES])> = lazy!({
    let mut tss = TaskStateSegment::new();
    let stack_bottom = unsafe {ALLOCATOR.alloc(Layout::from_size_align(STACK_SIZE, 8).unwrap())} as usize;
    let stack_top = stack_bottom + STACK_SIZE - 8;
    tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX] = Virtual(stack_top);
    tss.interrupt_stack_table[INT_IST_INDEX] = Virtual(INT_STACK_TOP);

    // init int_stack
    let mut tbl = ActivePageTable::get();
    let mut pages = [0; STACK_PAGES];
    for (i, a) in ((INT_STACK_TOP - crate::task::STACK_SIZE) .. INT_STACK_TOP).step_by(PAGE_SIZE).enumerate() {
        let frame = alloc_frame();
        tbl.map_to_addr(Page::from_address(Virtual(a)),
            frame, PTEntry::PRESENT | PTEntry::WRITABLE | PTEntry::NO_EXECUTE, false, false);
        pages[i] = frame.0 as u64;
    }

    unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};

    (tss, pages)
});

static GDT: Lazy<(super::gdt::Gdt, super::gdt::SegmentSelector, super::gdt::SegmentSelector, super::gdt::SegmentSelector, &'static [u64; STACK_PAGES])> = lazy!({
    let mut gdt = Gdt::new();
    let code_selector = gdt.add_entry(super::gdt::Descriptor::kernel_code_segment());
    let stack_selector = gdt.add_entry(super::gdt::Descriptor::kernel_stack_segment());

    let (ref tss, ref frames) = *TSS;
    let tss_selector = gdt.add_entry(super::gdt::Descriptor::tss_segment(tss));

    gdt.add_entry(super::gdt::Descriptor::user_stack_segment());
    gdt.add_entry(super::gdt::Descriptor::user_code_segment());
   (gdt, code_selector, stack_selector, tss_selector, frames)
});

const STACK_SIZE: usize = PAGE_SIZE * 2;
pub fn init_idt() -> &'static [u64; STACK_PAGES] {
    let (ref gdt, ref code, ref ss, ref tss, frames) = *GDT;
    gdt.load();
    unsafe {
        // println!();
        asm!("mov ss, {ss}",
             "push {sel}",
             "lea {tmp}, [1f + rip]",
             "push {tmp}",
             "retfq",
             "1:", sel = in(reg) code.0 as u64,
                   ss = in(reg) ss.0 as u64,
                   tmp = lateout(reg) _);
        asm!("ltr {0:x}", in(reg) tss.0);
    }

    println!("before");
    IDT.load();
    frames
}
