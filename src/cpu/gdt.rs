use core::arch::asm;

use crate::{mem::Virtual, cpu::DescriptorTablePointer};

pub struct Gdt {
    table: [u64; 8],
    next_free: usize,
}

pub struct SegmentSelector(pub u16);

impl Gdt {
    pub const fn new() -> Gdt {
        Gdt {
            table: [0; 8],
            next_free: 1,
        }
    }

    pub fn add_entry(&mut self, entry: Descriptor) -> SegmentSelector {
        let index = match entry {
            Descriptor::UserSegment(value) => self.push(value),
            Descriptor::SystemSegment(value_low, value_high) => {
                let index = self.push(value_low);
                self.push(value_high);
                index
            }
        };
        SegmentSelector((index as u16) << 3)
    }

    fn push(&mut self, value: u64) -> usize {
        if self.next_free < self.table.len() {
            let index = self.next_free;
            self.table[index] = value;
            self.next_free += 1;
            index
        } else {
            panic!("GDT full");
        }
    }

    pub fn load(&'static self) {
        use core::mem::size_of;

        let ptr = DescriptorTablePointer {
            limit: (self.table.len() * size_of::<u64>() - 1) as u16,
            base: self.table.as_ptr() as u64,
        };

        unsafe { asm!("lgdt [{}]", in(reg) &ptr); };
    }
}

pub enum Descriptor {
    UserSegment(u64),
    SystemSegment(u64, u64),
}

impl Descriptor {
    fn limit_base(limit: u64, base: u64) -> u64 {
        limit & 0xffff
            | ((base & 0xffffff) << 16)
            | ((limit & 0xf0000) << 32)
            | ((base & 0xff000000) << 32)
    }

    pub fn kernel_code_segment() -> Descriptor {
        let flags = DescriptorFlags::USER_SEGMENT
            | DescriptorFlags::READ_WRITE
            | DescriptorFlags::EXECUTABLE
            | DescriptorFlags::PRESENT
            | DescriptorFlags::LONG_MODE
            | DescriptorFlags::GRANULARITY
            | Self::limit_base(0xffff, 0);
        Descriptor::UserSegment(flags)
    }

    pub fn kernel_stack_segment() -> Descriptor {
        let flags = DescriptorFlags::USER_SEGMENT
            | DescriptorFlags::PRESENT
            | DescriptorFlags::CONFORMING
            | DescriptorFlags::READ_WRITE
            | DescriptorFlags::LONG_MODE
            | DescriptorFlags::GRANULARITY
            | Self::limit_base(0xfffff, 0);
        Descriptor::UserSegment(flags)
    }

    pub fn user_code_segment() -> Descriptor {
        let flags = DescriptorFlags::USER_SEGMENT
            | DescriptorFlags::READ_WRITE
            | DescriptorFlags::PRESENT
            | DescriptorFlags::DPL3
            | DescriptorFlags::EXECUTABLE
            | DescriptorFlags::LONG_MODE
            | DescriptorFlags::GRANULARITY
            | Self::limit_base(0xfffff, 0);
        Descriptor::UserSegment(flags)
    }

    pub fn user_stack_segment() -> Descriptor {
        let flags = DescriptorFlags::USER_SEGMENT
            | DescriptorFlags::PRESENT
            | DescriptorFlags::CONFORMING
            | DescriptorFlags::DPL3
            | DescriptorFlags::READ_WRITE
            | DescriptorFlags::LONG_MODE
            | DescriptorFlags::GRANULARITY
            | Self::limit_base(0xfffff, 0);
        Descriptor::UserSegment(flags)
    }

    pub fn tss_segment(tss: &'static TaskStateSegment) -> Descriptor {
        use core::mem::size_of;
        let ptr = tss as *const _ as u64;

        // base
        let low = DescriptorFlags::PRESENT
            | (ptr & 0xFFFFFF) << 16
            | (ptr >> 24) & 0xFF
            | 0b1001_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000
            | (size_of::<TaskStateSegment>() - 1) as u64;

        let high = ptr >> 32;

        Descriptor::SystemSegment(low, high)
    }
}


#[derive(Debug, Clone, Copy)]
#[repr(C, packed(4))]
pub struct TaskStateSegment {
    reserved_1: u32,
    pub privilege_stack_table: [Virtual; 3],
    reserved_2: u64,
    pub interrupt_stack_table: [Virtual; 7],
    reserved_3: u64,
    reserved_4: u16,
    pub iomap_base: u16,
}

impl TaskStateSegment {
    pub const fn new() -> Self {
        TaskStateSegment {
            privilege_stack_table: [Virtual(0); 3],
            interrupt_stack_table: [Virtual(0); 7],
            iomap_base: core::mem::size_of::<TaskStateSegment>() as u16,
            reserved_1: 0,
            reserved_2: 0,
            reserved_3: 0,
            reserved_4: 0,
        }
    }
}


pub struct DescriptorFlags;
impl DescriptorFlags {
    pub const READ_WRITE: u64        = 1 << 41;
    pub const CONFORMING: u64        = 1 << 42;
    pub const EXECUTABLE: u64        = 1 << 43;
    pub const USER_SEGMENT: u64      = 1 << 44;
    pub const DPL3: u64              = 3 << 45;
    pub const PRESENT: u64           = 1 << 47;
    pub const LONG_MODE: u64         = 1 << 53;
    pub const GRANULARITY: u64       = 1 << 55;
}
