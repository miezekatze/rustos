use crate::io::ports::IOPort;

pub struct PIC {
    int_offset: u8,
    primary_cmd_port: u16,
    primary_data_port: u16,
    secondary_cmd_port: u16,
    secondary_data_port: u16,
}

impl PIC {
    pub const fn new(int_offset: u8) -> Self {
        Self {
            int_offset,
            primary_cmd_port: 0x20,
            primary_data_port: 0x21,
            secondary_cmd_port: 0xA0,
            secondary_data_port: 0xA1,
        }
    }

    unsafe fn read_masks(&mut self) -> (u8, u8) {
        (u8::pin(self.primary_data_port),
         u8::pin(self.secondary_data_port))
    }

    fn update_masks(masks: &mut (u8, u8)) {
        masks.0 &= !1;
    }

    unsafe fn write_masks(&mut self, masks: (u8, u8)) {
        masks.0.pout(self.primary_data_port);
        masks.1.pout(self.secondary_data_port);
    }

    /// initialze PIC
    /// # Safety
    /// Can lead to exceptions + undefined behaviour, if wrongly configured
    pub unsafe fn init(&mut self) {
        const CMD_INIT: u8 = 0x11;
        const MODE_8086: u8 = 0x01;

        let mut saved_int_masks = self.read_masks();

        CMD_INIT.pout(self.primary_cmd_port);
        CMD_INIT.pout(self.secondary_cmd_port);

        self.int_offset.pout(self.primary_data_port);
        (self.int_offset + 8).pout(self.secondary_data_port);

        4u8.pout(self.primary_data_port);
        2u8.pout(self.secondary_data_port);

        MODE_8086.pout(self.primary_data_port);
        MODE_8086.pout(self.secondary_data_port);

        Self::update_masks(&mut saved_int_masks);
        self.write_masks(saved_int_masks);
    }

    pub fn end_of_interrupt(&mut self, idx: usize) {
        // println!("EOI {idx}");
        const END_OF_INT: u8 = 0x20;
        if idx < self.int_offset as usize || idx > self.int_offset as usize + 16 {
            panic!("Invalid interrupt index {idx} for EOI");
        }

        unsafe {
            if idx >= self.int_offset as usize + 8 {
                END_OF_INT.pout(self.secondary_cmd_port);
            }
            END_OF_INT.pout(self.primary_cmd_port);
        }
    }
}
