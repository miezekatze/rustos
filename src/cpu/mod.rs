pub mod interrupts;
pub mod gdt;
pub mod pic;

#[repr(C, packed)]
#[derive(Debug)]
pub struct DescriptorTablePointer {
    limit: u16,
    base: u64,
}
