use alloc::{vec, vec::Vec};
use crate::{io::ports::IOPort, println};

const CONFIG_ADDRESS: u16 = 0xCF8;
const CONFIG_DATA: u16 = 0xCFC;

fn pci_config_read_word(bus: u8, slot: u8, func: u8, offset: u8) -> u16 {
    let mut address: u32 = 0x80000000;
    address |= (bus as u32) << 16;
    address |= (slot as u32) << 11;
    address |= (func as u32) << 8;
    address |= (offset as u32) & 0xFC;

    unsafe {
        address.pout(CONFIG_ADDRESS);
        let result = u32::pin(CONFIG_DATA);
        return ((result >> ((offset & 2) * 8)) & 0xFFFF) as u16;
    }
}

pub fn pci_read_bar(bus: u8, slot: u8, func: u8, bar: u8) -> u32 {
    let offset = 0x10 + (bar as u32) * 4;
    let lo = pci_config_read_word(bus, slot, func, offset as u8) as u32;
    let hi = pci_config_read_word(bus, slot, func, (offset + 2) as u8) as u32;
    return (hi << 16) | lo;
}

#[derive(Debug, Clone, Copy)]
pub struct PCIHeader {
    pub vendor_id: u16,
    pub device_id: u16,
    pub command: u16,
    pub status: u16,
    pub revision_id: u8,
    pub prog_if: u8,
    pub sub_class: u8,
    pub class: u8,
    pub cache_line_size: u8,
    pub latency_timer: u8,
    pub header_type: u8,
    pub bist: u8,

    pub bus: u8,
    pub slot: u8,
    pub func: u8,
}

include!("./pci_devs.rinc");

impl core::fmt::Display for PCIHeader {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        assert_ne!(self.vendor_id, 0xFFFF);
        match self.class {
            0x1 => match self.sub_class {
                0x1 => match self.prog_if {
                    0x80 => write!(f, "ISA Compatibility mode-only controller, supports bus mastering")?,
                    a => write!(f, "IDE Controller {a:#x}")?,
                },
                a => write!(f, "Mass Storage Controller {a:#x}")?,
            },
            0x2 => match self.sub_class {
                0x0 => write!(f, "Ethernet Controller")?,
                a => write!(f, "Network Controller {a:#x}")?,
            },
            0x3 => match self.sub_class {
                0x0 => match self.prog_if {
                    0x0 => write!(f, "VGA Controller")?,
                    a => write!(f, "VGA Compatible Controller {a:#x}")?,
                }
                a => write!(f, "Display Controller {a:#x}")?,
            },
            0x4 => match self.sub_class {
                0x0 => write!(f, "Multimedia Video Controller")?,
                0x1 => write!(f, "Multimedia Audio Controller")?,
                0x3 => write!(f, "Audio Device")?,
                a => write!(f, "Multimedia Controller {a:#x}")?,
            },
            0x6 => match self.sub_class {
                0x0 => write!(f, "Host bridge")?,
                0x1 => write!(f, "ISA bridge")?,
                0x80 => write!(f, "Other bridge")?,
                a => write!(f, "Bridge {a:#x}")?,
            },
            a => write!(f, "(class {a:#x})")?,
        }

        write!(f, ": ")?;

        let devs = &PCI_DEVICES;
        for (a, b, s) in devs {
            if *a == self.vendor_id && *b == self.device_id {
                return write!(f, "{}", s);
            }
        }
        write!(f, "Unknown PCI device")
    }
}

fn pci_read_header(bus: u8, slot: u8, function: u8) -> PCIHeader {
    let vendor_id = pci_config_read_word(bus, slot, function, 0);
    let device_id = pci_config_read_word(bus, slot, function, 2);
    let command = pci_config_read_word(bus, slot, function, 4);
    let status = pci_config_read_word(bus, slot, function, 6);
    let rev_prog = pci_config_read_word(bus, slot, function, 8);
    let class_sub = pci_config_read_word(bus, slot, function, 0xA);
    let cache_latency = pci_config_read_word(bus, slot, function, 0xC);
    let type_bist = pci_config_read_word(bus, slot, function, 0xE);

    PCIHeader {
        vendor_id,
        device_id,
        command,
        status,
        revision_id: rev_prog as u8,
        prog_if: (rev_prog >> 8) as u8,
        sub_class: class_sub as u8,
        class: (class_sub >> 8) as u8,
        cache_line_size: cache_latency as u8,
        latency_timer: (cache_latency >> 8) as u8,
        header_type: type_bist as u8,
        bist: (type_bist >> 8) as u8,

        bus,
        slot,
        func: function,
    }
}

fn check_device(bus: u8, dev: u8) -> Vec<PCIHeader> {
    let mut devs = vec![];

    let header = pci_read_header(bus, dev, 0);
    if header.vendor_id == 0xFFFF {
        return vec![];
    }

    let header_type = header.header_type;
    devs.push(header);

    check_function(bus, dev, 0, &header);
    if header_type & 0x80 != 0 {
        // multi-function device
        for function in 1..8 {
            let header = pci_read_header(bus, dev, function);
            if header.vendor_id != 0xFFFF {
                devs.push(header);
                devs.append(&mut check_function(bus, dev, function, &header));
            }
        }
    }
    devs
}

fn check_function(bus: u8, device: u8, function: u8, header: &PCIHeader) -> Vec<PCIHeader> {
    let mut devs = vec![];

    let base_class = header.class;
    let sub_class = header.sub_class;
    if base_class == 0x6 && sub_class == 0x4 {
        // PCI-to-PCI bridge
        let secondary_bus = pci_config_read_word(bus, device, function, 0x18);
        let secondary_bus = (secondary_bus >> 8) as u8;
        devs.append(&mut check_bus(secondary_bus));
    }
    devs
}

fn check_bus(bus: u8) -> Vec<PCIHeader> {
    let mut devs = vec![];
    for dev in 0..32 {
        devs.append(&mut check_device(bus, dev));
    }
    devs
}

pub fn enumerate_pci() -> Vec<PCIHeader> {
    let bus = 0;

    let mut devs = vec![];

    let header = pci_read_header(0, 0, 0);
    let header_type = header.header_type;
    if header_type & 0x80 == 0 {
        // single PCI host controller
        println!("Single PCI host controller");
        devs.append(&mut check_bus(bus));
    } else {
        // multiple PCI host controllers
        for function in 0..8 {
            let header = pci_read_header(bus, 0, function);
            if header.vendor_id == 0xFFFF {
                break;
            }
            devs.append(&mut check_bus(function));
        }
    }
    
    devs
}
