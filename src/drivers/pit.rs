use crate::{io::ports::IOPort, cpu::interrupts::{Registers, PICInterruptIndex, PIC}, task::switch_task};

pub fn init_pit() {
    let divisor = 1193180 / FREQUENCY;
    unsafe{
        0x36u8.pout(0x43);
        (divisor as u8).pout(0x40);
        ((divisor >> 8) as u8).pout(0x40);
    }
}

static mut TIMER_TICKS: u64 = 0;
const FREQUENCY: u32 = 200;

pub fn get_nanos() -> u128 {
    let ticks = unsafe{TIMER_TICKS};
    let nanos = ticks as u128 * 1_000_000_000 / FREQUENCY as u128;
    nanos
}


#[inline(never)]
pub fn handle_timer_interrupt(_regs: &mut Registers) {
    PIC.write().end_of_interrupt(PICInterruptIndex::Timer as usize + 32);
    unsafe{TIMER_TICKS += 1};
    if unsafe{TIMER_TICKS % 4 == 0} {
        switch_task();
    }
}


