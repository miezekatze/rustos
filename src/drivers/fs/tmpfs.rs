use alloc::{boxed::Box, collections::BTreeMap, string::{String, ToString}, sync::Arc, vec::Vec, vec};
use crate::{utils::{lock::Lock, errno::{Errno, ErrnoOr}}, drivers::fs::Traverse};

use super::{CPath, DirFd, FileType, EmptyDir, DirEntries, MountDriverT, TraverseResult, CPathS, DirEntry, Path, Fd};

type Children = BTreeMap<String, TmpDirEntry>;

#[derive(Debug, Clone)]
pub enum TmpFile {
    File(Arc<Lock<Vec<u8>>>),
    Char(usize, usize),
    Block(usize, usize),
}

#[derive(Debug, Clone)]
pub enum TmpDirEntry {
    File(TmpFile),
    Dir(Children),
    ParentDir,
    CurrentDir,
}

impl TmpDirEntry {
    fn filetype(&self) -> FileType {
        match self {
            TmpDirEntry::File(TmpFile::Char(maj, min)) => FileType::CharDev(*maj, *min),
            TmpDirEntry::File(TmpFile::Block(maj, min)) => FileType::BlockDev(*maj, *min),
            TmpDirEntry::File(TmpFile::File(_)) => FileType::File,
            TmpDirEntry::Dir(_)
            | TmpDirEntry::ParentDir
            | TmpDirEntry::CurrentDir
            => FileType::Dir,
        }
    }
}

#[derive(Debug, Clone)]
pub struct TmpFs {
    pub root: Children,
}

impl TmpFs {
    pub fn empty() -> Self {
        Self {
            root: Children::from([("..".to_string(),
                                           TmpDirEntry::ParentDir),
                                          (".".to_string(),
                                           TmpDirEntry::CurrentDir)])
        }
    }

    fn traverse_dir_path(&self, p: &CPathS) -> Option<Children> {
        assert!(p.absolute);
        let mut dir = &self.root;
        for a in p.real_iter() {
            let entry = dir.get(a)?;
            dir = match entry {
                TmpDirEntry::Dir(c) => c,
                _ => return None,
            };
        }
        Some(dir.clone())
    }
}

impl MountDriverT for Arc<Lock<TmpFs>> {
    fn traverse_path_open(&self, mount_rel_path: super::CPathS, mount_path: Path, file: &str, _flags: usize, _mode: usize) -> TraverseResult {
        let mut path = mount_rel_path;
        path.data.push(CPath::singleton(file.to_string()));
        let mut rpath = path.clone();

        assert!(path.absolute);
        let v = &self.read().root;
        for (i, a) in path.real_iter().enumerate() {
            let entr = v.get(a).ok_or(Errno::ENOENT)?;
            match entr {
                TmpDirEntry::File(f) => {
                    if i < path.real_iter().count() - 1 {
                        return Err(Errno::ENOTDIR);
                    } else {
                        return Ok(Traverse::File(match f {
                            TmpFile::File(f) => Box::new(TmpRAMFd {
                                buf: f.clone(),
                                pos: 0,
                            }),
                            TmpFile::Char(maj, min) |
                            TmpFile::Block(maj, min) => crate::drivers::dev::dev_open(*maj, *min)?,
                        }))
                    }
                },
                TmpDirEntry::Dir(_) => {
                    if i < path.real_iter().count() - 1 {
                        continue;
                    } else {
                        return Ok(Traverse::Dir(rpath.clone(), TmpDirFd::from_path(rpath, mount_path, self.clone())));
                    }
                },
                TmpDirEntry::ParentDir => {
                    assert_eq!(i, path.real_iter().count() - 1);
                    rpath.data.pop();
                    rpath.data.pop();
                    return Ok(Traverse::MPParent(rpath));
                },
                TmpDirEntry::CurrentDir => {
                    assert_eq!(i, path.real_iter().count() - 1);
                    rpath.data.pop();
                    return Ok(Traverse::Dir(rpath.clone(), TmpDirFd::from_path(rpath, mount_path, self.clone())));
                },
            }
        }

        unreachable!()
    }

    fn umount(&mut self) -> ErrnoOr<()> { Ok(()) }
}

#[derive(Debug, Clone)]
pub struct TmpRAMFd {
    pub buf: Arc<Lock<Vec<u8>>>,
    pub pos: usize,
}

impl Fd for TmpRAMFd {
    fn read_at(&mut self, buf: &mut [u8], pos: usize) -> ErrnoOr<usize> {
        if pos >= self.buf.read().len() {
            return Ok(0);
        }
        let vals = &self.buf.read()[pos..(self.pos + buf.len())];
        let len = vals.len();
        assert!(len <= buf.len());
        buf[..len].copy_from_slice(&vals[..len]);
        Ok(len)
    }

    fn read(&mut self, buf: &mut [u8]) -> ErrnoOr<usize> {
        let len = self.read_at(buf, self.pos)?;
        self.pos += len;
        Ok(len)
    }

    unsafe fn close(&mut self) { }

    fn len(&self) -> usize {
        self.buf.read().len()
    }

    fn get_type(&self) -> FileType {
        FileType::File
    }

    fn write(&mut self, _buf: &[u8]) -> ErrnoOr<usize> {
        todo!()
    }

    fn clonefd(&self) -> Box<dyn Fd> {
        Box::new(self.clone())
    }
}

#[derive(Debug, Clone)]
pub struct TmpDirFd {
    pub path: CPathS,
    pub mount_path: Path,
    pub fs: Arc<Lock<TmpFs>>,
}

impl TmpDirFd {
    pub fn from_path(p: CPathS, mp: Path, fs: Arc<Lock<TmpFs>>) -> Box<Self> {
        Box::new(Self {
            path: p,
            fs,
            mount_path: mp,
        })
    }
}

#[derive(Debug, Clone)]
pub struct TmpEntries(Children);

impl TmpEntries {
    pub const fn new() -> Self {
        Self(BTreeMap::new())
    }
}

struct TmpEntryIt(TmpEntries, usize);
impl TmpEntryIt {
    fn new(x: TmpEntries) -> Self {
        Self(x, 0)
    }
}

impl Iterator for TmpEntryIt {
    type Item = (String, FileType);

    fn next(&mut self) -> Option<Self::Item> {
        let (n, val) = self.0.0.clone().into_iter().nth(self.1)?;
        self.1 += 1;
        Some((n, val.filetype()))
    }
}

impl DirEntries for TmpEntries {
    fn add(&mut self, item: DirEntry) {
        self.0.insert(item.0, match item.1 {
            FileType::Dir => TmpDirEntry::Dir(BTreeMap::new()),
            FileType::File => todo!(),
            FileType::CharDev(_, _) => todo!(),
            FileType::BlockDev(_, _) => todo!(),
            FileType::Fifo => todo!(),
            FileType::Socket => todo!(),
        });
    }

    fn iter(&self) -> Box<dyn Iterator<Item = DirEntry>> {
        Box::new(TmpEntryIt::new(self.clone()))
    }
}

impl DirFd for TmpDirFd {
    fn read_dir_contents(&self) -> ErrnoOr<Box<dyn super::DirEntries>> {
        let dir = self.fs.read().traverse_dir_path(&self.path);
        Ok(if let Some(dir) = dir {
            Box::new(TmpEntries(dir))
        } else {
            Box::new(EmptyDir(vec![]))
        })
    }

    fn get_absolute_path(&self) -> CPathS {
        let mut mnt_path = CPathS::from_path(self.mount_path.clone(), true);
        mnt_path.data.append(&mut self.path.data.clone());
        mnt_path
    }

    fn clonefd(&self) -> Box<dyn DirFd> {
        Box::new(self.clone())
    }

    fn close(&mut self) {
        // nothing to do, because of RAII
    }
}
