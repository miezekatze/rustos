use crate::{alloc::string::ToString, drivers::fs::tmpfs::TmpFile, utils::errno::ErrnoOr};
use alloc::collections::BTreeMap;
use alloc::sync::Arc;

use crate::{utils::{errno::Errno, lock::Lock, lazy::Lazy}, lazy};
use super::{tmpfs::TmpFs, Path, MountDriverT, parse_abs_path};

pub static DEVTMPFS: Lazy<Arc<Lock<TmpFs>>> = lazy!(Arc::new(Lock::new(TmpFs::empty())));

#[derive(Debug)]
pub struct DevTmpFs {
    pub fs: Arc<Lock<TmpFs>>,
    pub path: Path
}

pub fn create_devtmpfs(dev: Path, path: Path) -> ErrnoOr<DevTmpFs> {
    if dev.len() != 1 && dev[0] != "devtmpfs" {
        return Err(Errno::EINVAL);
    }
    Ok(DevTmpFs {
        fs: DEVTMPFS.clone(),
        path,
    })
}

impl MountDriverT for DevTmpFs {
    fn traverse_path_open(&self, mount_rel_path: super::CPathS, mount_path: Path, file: &str, flags: usize, mode: usize) -> super::TraverseResult {
        self.fs.traverse_path_open(mount_rel_path, mount_path, file, flags, mode)
    }

    fn umount(&mut self)  -> ErrnoOr<()> {
        self.fs.umount()
    }
}

pub fn add_synthetic_file(p: &str, file: TmpFile) -> ErrnoOr<()> {
    let path = parse_abs_path(p);
    let mut l = DEVTMPFS.write();
    let mut insdir = &mut l.root as *mut BTreeMap<_, _>;
    use crate::drivers::fs::tmpfs::TmpDirEntry;

    for p in path.iter().take(path.len() - 1) {
        unsafe {
            let x = (*insdir).get_mut(p);
            match x {
                Some(a) => match a {
                    TmpDirEntry::File(_) => return Err(Errno::EISDIR),
                    TmpDirEntry::Dir(c) => insdir = c,
                    _ => return Err(Errno::EINVAL),
                },
                None => {
                    (*insdir).insert(p.to_string(), TmpDirEntry::Dir(BTreeMap::new()));
                    if let Some(TmpDirEntry::Dir(c)) = (*insdir).get_mut(p) {
                        c.insert(".".to_string(), TmpDirEntry::CurrentDir);
                        c.insert("..".to_string(), TmpDirEntry::ParentDir);
                        insdir = c;
                    } else {
                        unreachable!()
                    }
                },
            }
        }
    }

    let name = path.last().ok_or(Errno::EINVAL)?;
    
    unsafe {
        (*insdir).insert(name.to_string(),
                         TmpDirEntry::File(file));
    }
    
    Ok(())
//    DEVTMPFS
}
