use crate::{alloc::borrow::ToOwned, utils::errno::ErrnoOr};
use crate::drivers::fs::devfs::create_devtmpfs;
use alloc::{collections::BTreeMap, boxed::Box, string::{String, ToString}, vec, vec::Vec};
use empty::EmptyFs;

pub mod empty;
pub mod devfs;
pub mod tmpfs;

use crate::{println, utils::{lock::Lock, errno::Errno}};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct CPathS {
    pub data: Vec<CPath>,
    pub absolute: bool,
}

impl CPathS {
    const ROOT: Self = Self {
        absolute: true,
        data: vec![],
    };

    pub fn from_path(p: Path, abs: bool) -> Self {
        Self {
            data: p.iter().map(|x |CPath::singleton(x.to_string())).collect(),
            absolute: abs,
        }
    }

    fn real_iter(&self) -> impl Iterator<Item = &String> {
        self.data.iter().flat_map(|x| x.real.iter())
    }

    pub fn real(&self) -> Path {
        self.real_iter().cloned().collect()
    }

    pub fn virt(&self) -> Path {
        self.data.iter().map(|x| x.virt.clone()).collect()
    }
}

#[inline]
pub fn format_path(x: &[String]) -> String {
    "/".to_string() + &x.join("/")
}

impl core::fmt::Display for CPathS {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "real: {} | ", format_path(&self.real()))?;
        write!(f, "virtual: {}", format_path(&self.virt()))
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct CPath {
    virt: String,
    real: Path
}

impl CPath {
    pub fn singleton(s: String) -> CPath {
        Self {
            virt: s.clone(),
            real: vec![s],
        }
    }
}
pub type Path = Vec<String>;

// impl Path {
//     const ROOT: Self = Self {
//         data: vec![],
//         absolute: true,
//     };

fn parse_path(s: &str) -> (Path, bool) {
    let absolute = s.starts_with('/');
    let x = s.split('/').map(str::to_owned).filter(|s| !s.is_empty()).collect();
    (x, absolute)
}

fn parse_abs_path(s: &str) -> Path {
    let (p, absolute) = parse_path(s);
    assert!(absolute);
    p
}
// }

#[derive(Debug)]
pub enum Traverse {
    File(Box<dyn Fd>),
    Dir(CPathS, Box<dyn DirFd>),
    MPParent(CPathS),
}

pub type TraverseResult = ErrnoOr<Traverse>;

#[allow(clippy::len_without_is_empty)]
pub trait Fd: core::fmt::Debug {
    fn read(&mut self, buf: &mut [u8]) -> ErrnoOr<usize>;
    fn read_at(&mut self, buf: &mut [u8], pos: usize) -> ErrnoOr<usize>;
    fn write(&mut self, buf: &[u8]) -> ErrnoOr<usize>;
    fn len(&self) -> usize;
    fn get_type(&self) -> FileType;

    fn clonefd(&self) -> Box<dyn Fd>;

    /// Close / deinit code
    /// # Safety
    /// self should *not* be used after calling close, use VFS.close instead
    unsafe fn close(&mut self);
}

pub trait MountDriverT: core::fmt::Debug {
    fn traverse_path_open(&self, mount_rel_path: CPathS, mount_path: Path, file: &str, flags: usize, mode: usize) -> TraverseResult;
    fn umount(&mut self) -> ErrnoOr<()>;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum FileType {
    Dir,
    File,
    CharDev(usize, usize),
    BlockDev(usize, usize),
    Fifo,
    Socket
}

pub type DirEntry = (String, FileType);

pub trait DirEntries: core::fmt::Debug {
    fn add(&mut self, item: DirEntry);
    fn iter(&self) -> Box<dyn Iterator<Item = DirEntry>>;
}

// impl Iterator for &mut Box<dyn DirEntries> {
//     type Item = (String, FileType);

//     fn next(&mut self) -> Option<Self::Item> {
//         let x: &mut dyn DirEntries = &mut ***self;
//         x.next()
//     }
// }

// #[derive(Debug)]
// pub struct DirEntry {
//     name: String,
//     typ: FileType,
// }

pub trait DirFd: core::fmt::Debug {
    fn read_dir_contents(&self) -> ErrnoOr<Box<dyn DirEntries>>;
    fn get_absolute_path(&self) -> CPathS;
    fn clonefd(&self) -> Box<dyn DirFd>;
    fn close(&mut self);
}

#[derive(Debug)]
pub struct EmptyDir(Vec<(String, FileType)>);
impl Iterator for EmptyDir {
    type Item = (String, FileType);

    fn next(&mut self) -> Option<Self::Item> {
        None
    }
}

impl DirEntries for EmptyDir {
    fn add(&mut self, item: DirEntry) {
        self.0.push(item);
    }

    fn iter(&self) -> Box<dyn Iterator<Item = DirEntry>> {
        Box::new(self.0.clone().into_iter())
    }
    
}

#[derive(Debug)]
pub struct MountDriver {
    pub create: fn(dev: Path, path: Path) -> ErrnoOr<Box<dyn MountDriverT>>,
}


#[derive(Debug)]
pub struct MountPoint {
    pub typ: String,
    pub dev: Path,
    pub path: Path,
    pub drv: Box<dyn MountDriverT>,
}

pub struct Vfs {
    mount_types: Lock<BTreeMap<&'static str, MountDriver>>,
    mount_points: Lock<BTreeMap<Path, MountPoint>>,
}

pub static VFS: Vfs = Vfs {
    mount_types: Lock::new(BTreeMap::new()),
    mount_points: Lock::new(BTreeMap::new()),
};

impl Vfs {
    pub fn register_mnt_driver(&self, typ: &'static str, drv: MountDriver) {
        self.mount_types.write().insert(typ, drv);
    }

    pub fn mount(&self, path: &str, typ: &str, device: &str) -> ErrnoOr<()> {
        let dev = parse_path(device).0;
        let path = parse_abs_path(path);
        let drv = (self.mount_types.read().get(typ).ok_or(Errno::ENODEV)?.create)(dev.clone(), path.clone())?;
        self.mount_points.write().insert(path.clone(), MountPoint {
            typ: typ.to_string(),
            dev,
            drv,
            path,
        });
        Ok(())
    }

    pub fn umount(&self, path: &str) -> ErrnoOr<()> {
        let path = parse_abs_path(path);
        match self.mount_points.write().remove(&path) {
            Some(ref mut x) => x.drv.umount(),
            None => Err(Errno::EINVAL),
        }
    }
}

pub fn close(mut fd: FileDescriptor) {
    unsafe {
        fd.close();
    }
    drop(fd);
}


pub const O_CREAT: usize = 0o100;
pub const O_RDONLY: usize = 0o0;
pub const O_WRONLY: usize = 0o1;
pub const O_RDWR: usize = 0o2;
pub const O_DIRECTORY: usize = 0o200000;

#[derive(Debug)]
pub struct FileDescriptor {
    pub typ: FDType,
    pub flags: usize,
    pub diroff: usize,
}

#[derive(Debug)]
pub enum FDType {
    File(Box<dyn Fd>),
    Dir(Box<dyn DirFd>),
}

impl Clone for FileDescriptor {
    fn clone(&self) -> Self {
        FileDescriptor {
        typ: match &self.typ {
                FDType::File(f) => FDType::File(f.clonefd()),
                FDType::Dir(d) => FDType::Dir(d.clonefd()),
            },
            flags: self.flags,
            diroff: self.diroff,
        }
    }
}

impl Fd for FileDescriptor {
    fn read(&mut self, buf: &mut [u8]) -> ErrnoOr<usize> {
        if self.flags & O_WRONLY != 0 {
            return Err(Errno::EBADF);
        }

        match &mut self.typ {
            FDType::File(f) => f.read(buf),
            FDType::Dir(_) => Err(Errno::EISDIR),
        }
    }

    fn read_at(&mut self, buf: &mut [u8], pos: usize) -> ErrnoOr<usize> {
        if self.flags & O_WRONLY != 0 {
            return Err(Errno::EBADF);
        }

        match &mut self.typ {
            FDType::File(f) => f.read_at(buf, pos),
            FDType::Dir(_) => Err(Errno::EISDIR),
        }
    }

    fn write(&mut self, buf: &[u8]) -> ErrnoOr<usize> {
        if self.flags & (O_RDWR | O_WRONLY) == 0 {
            return Err(Errno::EBADF);
        }

        match &mut self.typ {
            FDType::File(f) => f.write(buf),
            FDType::Dir(_) => Err(Errno::EISDIR),
        }
    }

    fn len(&self) -> usize {
        match &self.typ {
            FDType::File(f) => f.len(),
            FDType::Dir(_) => 0,
        }
    }

    fn get_type(&self) -> FileType {
        match &self.typ {
            FDType::File(f) => f.get_type(),
            FDType::Dir(_) => FileType::Dir,
        }
    }

    fn clonefd(&self) -> Box<dyn Fd> {
        match &self.typ {
            FDType::File(f) => Box::new(FileDescriptor {
                typ: FDType::File(f.clonefd()),
                flags: self.flags,
                diroff: self.diroff,
            }),
            FDType::Dir(d) => Box::new(FileDescriptor {
                typ: FDType::Dir(d.clonefd()),
                flags: self.flags,
                diroff: self.diroff,
            }),
        }
    }

    unsafe fn close(&mut self) {
        match &mut self.typ {
            FDType::File(f) => f.close(),
            FDType::Dir(d) => d.close(),
        }
    }
}

pub fn open(s: &str, flags: usize, mode: usize) -> ErrnoOr<FileDescriptor> {
    let t = open_(s, flags, mode)?;
    match t {
        Traverse::File(f) => if flags & O_DIRECTORY != 0 {
            Err(Errno::ENOTDIR)
        } else {
            Ok(FileDescriptor {
                typ: FDType::File(f),
                flags,
                diroff: 0,
            })
        },
        Traverse::Dir(_, d) => if flags & O_DIRECTORY == 0 {
            Err(Errno::EISDIR)
        } else {
            Ok(FileDescriptor {
                typ: FDType::Dir(d),
                flags,
                diroff: 0,
            })
        },
        Traverse::MPParent(_) => Err(Errno::EINVAL),
    }
}

pub fn readdir(fd: &FileDescriptor) -> ErrnoOr<Box<dyn DirEntries>> {
    if let FDType::Dir(ref fd) = fd.typ {
        let path = fd.get_absolute_path();
        let mut entries = fd.read_dir_contents()?;
        for mp in VFS.mount_points.read().keys() {
            let mut prefix = mp.clone();
            let nm = prefix.pop();
            if prefix == path.real() {
                if let Some(nm) = nm {
                    entries.add((nm, FileType::Dir));
                }
            }
        }
        Ok(entries)
    } else {
        Err(Errno::ENOTDIR)
    }
}

fn open_(s: &str, flags: usize, mode: usize) -> ErrnoOr<Traverse> {
    let (target, absolute) = parse_path(s);
    
    // TODO: read cwd
    if ! absolute {
        // relative
        println!("[warn] cwd only present after tasking");
        return Err(Errno::EINVAL);
    }

    let lock = VFS.mount_points.read();

    let mut path_prefs = vec![];
    let mut path = CPathS::ROOT;

    let mut old_mps = vec![];
    let mut current_mnt = lock.get(&parse_abs_path("/")).unwrap_or_else(|| {
        panic!("/ not mounted");
    });
    let mut mount_ress = vec![];

    let mut last_res = None;
    let mut paths = target;
    let mut is_file = false;
    //    paths.insert(0, ".".to_string());
    if paths.is_empty() {
        paths.push(".".to_string())
    }
    for mut p in paths {
        if is_file {
            return Err(Errno::ENOTDIR);
        }
        let mut rpath_next: Vec<_> = path_prefs.iter().flat_map(|x: &CPathS| x.real_iter()).cloned().collect();
        rpath_next.append(&mut path.real());
        rpath_next.push(p.clone());
        if let Some(mp) = lock.get(&rpath_next) {
            old_mps.push(current_mnt);
            current_mnt = mp;

            path.data.push(CPath::singleton(p));
            path_prefs.push(path);
            if let Some(a) = last_res {
                mount_ress.push(a);
            }
            path = CPathS::ROOT;
            p = ".".to_string();
        }
        
        let res = current_mnt.drv.traverse_path_open(path.clone(), current_mnt.path.clone(), &p, flags, mode)?;
        match res {
            Traverse::File(_) => {
                is_file = true;
                last_res = Some(res);
            },
            Traverse::Dir(ref d, _) => {
                path = d.clone();
                last_res = Some(res);
            },
            Traverse::MPParent(_) => {
                current_mnt = old_mps.pop().unwrap_or(current_mnt);
                path = path_prefs.pop().unwrap_or(CPathS::ROOT);
                path.data.pop();

                // if let Some(p) = path.data.last_mut() {
                //     p.real.clear();
                // }
                // p.data.iter_mut().for_each(|CPath {real, virt: _}| {
                //     real.clear()
                // });
                // path.data.append(&mut p.data);
                last_res = mount_ress.pop();
            },
        }
    }

    last_res.ok_or(Errno::ENOENT)
}

pub fn init_vfs() {
    VFS.register_mnt_driver("empty", MountDriver {
        create: |s, p| Ok(Box::new(EmptyFs::new(s, p))),
    });
    VFS.register_mnt_driver("devtmpfs", MountDriver {
        create: |s, p| Ok(Box::new(create_devtmpfs(s, p)?)),
    });

    VFS.mount("/", "empty", "empty").unwrap_or_else(|e| {
        panic!("could not mount '/': {e}");
    });

    VFS.mount("/dev", "devtmpfs", "dev").unwrap_or_else(|e| {
        panic!("could not mount '/dev': {e}");
    });
}
