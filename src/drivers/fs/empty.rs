use crate::{alloc::string::ToString, utils::errno::ErrnoOr};
use alloc::{string::String, boxed::Box, vec};
use crate::utils::errno::Errno;

use super::{Path, MountDriverT, TraverseResult, Traverse, DirFd, EmptyDir, CPathS, CPath, FileType, O_RDWR, O_WRONLY};

#[derive(Debug, Clone)]
pub struct EmptyFs {
    pub dev: Path,
    path: vec::Vec<String>
}

// #[derive(Debug)]
// pub struct EmptyFsFd {
//     pub fs: EmptyFs,
//     pub path: String,
// }

// impl Fd for EmptyFsFd {}

#[derive(Debug, Clone)]
struct EmptyFsDirFd(CPathS, Path);
impl DirFd for EmptyFsDirFd {
    fn read_dir_contents(&self) -> ErrnoOr<Box<dyn super::DirEntries>> {
        Ok(Box::new(EmptyDir(vec![(".".to_string(), FileType::Dir), ("..".to_string(), FileType::Dir)])))
    }

    fn get_absolute_path(&self) -> super::CPathS {
        let mut pref = CPathS::from_path(self.1.clone(), true);
        pref.data.append(&mut self.0.data.clone());
        pref
    }

    fn clonefd(&self) -> Box<dyn DirFd> {
        Box::new(self.clone())
    }

    fn close(&mut self) {
        // Nothing to do
    }
}

impl MountDriverT for EmptyFs {
    fn traverse_path_open(&self, mount_rel_path: super::CPathS, _mount_path: Path, file: &str, flags: usize, _mode: usize) -> TraverseResult {
        if flags & O_RDWR != 0 || flags & O_WRONLY != 0{
            return Err(Errno::EACCES);
        }

        if mount_rel_path.real().is_empty() && file == "." {
            Ok(Traverse::Dir(mount_rel_path.clone(), Box::new(EmptyFsDirFd(mount_rel_path, self.path.clone()))))
        } else if mount_rel_path.real().is_empty() && file == ".." {
            let mut par = mount_rel_path;
            par.data.push(CPath::singleton(file.to_string()));
            Ok(Traverse::MPParent(par))
        } else {
            Err(Errno::ENOENT)
        }
    }

    fn umount(&mut self) -> ErrnoOr<()> { Ok(()) }
}

impl EmptyFs {
    pub fn new(dev: Path, path: Path) -> Self {
        Self {
            dev,
            path
        }
    }
}
