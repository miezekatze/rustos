pub mod layout;

use alloc::{vec, vec::Vec};
use core::{arch::asm, cell::UnsafeCell};

use alloc::{boxed::Box, collections::VecDeque};
use crate::{video::DISPLAY, drivers::tty::layout::KEYBOARD_LAYOUT, utils::lock::Lock};
use super::{dev::{self, DeviceDriver}, fs::{Fd, devfs, tmpfs::TmpFile}, keyboard::{set_keyboard_handler, Scancode}};

const TTY_MAJOR: usize = 4;

static TTYS: [TtyDriver; 12] = [
    TtyDriver::new(0),
    TtyDriver::new(1),
    TtyDriver::new(2),
    TtyDriver::new(3),
    TtyDriver::new(4),
    TtyDriver::new(5),
    TtyDriver::new(6),
    TtyDriver::new(7),
    TtyDriver::new(8),
    TtyDriver::new(9),
    TtyDriver::new(10),
    TtyDriver::new(11),
];

static CURRENT_TTY: Lock<usize> = Lock::new(0);

// c_lflags
#[allow(unused)]
const ISIG: u32 = 0x0000001;
#[allow(unused)]
const ICANON: u32 = 0x0000002;
#[allow(unused)]
const ECHO: u32 = 0x0000010;

#[derive(Debug)]
struct Mutable<T> {
    inner: UnsafeCell<T>,
}

unsafe impl<T> Sync for Mutable<T> {}

impl <T> Mutable<T> {
    const fn new(inner: T) -> Self {
        Self {
            inner: UnsafeCell::new(inner),
        }
    }

    fn _read(&self) -> &T {
        unsafe { &*self.inner.get() }
    }

    fn write(&self) -> &mut T {
        unsafe { &mut *self.inner.get() }
    }
}

#[derive(Debug)]
#[allow(unused)]
struct TtyDriver {
    id: usize,
    line_queue: Mutable<VecDeque<u8>>,
    raw_queue: Mutable<VecDeque<u8>>,
    c_iflags: u32,
    c_oflags: u32,
    c_cflags: u32,
    c_lflags: u32,
    c_cc: [u8; 32],

    escape: bool,
    csi: bool,
    csi_param: Vec<u8>,
}

// ttyfd
impl TtyDriver {
    fn read(&self, buf: &mut [u8]) -> crate::utils::errno::ErrnoOr<usize> {
        let mut i = 0;
        while i < buf.len() {
            if let Some(c) = self.raw_queue.write().pop_front() {
                buf[i] = c;
                i += 1;
            } else {
                // block here
                unsafe{asm!("pause")};
            }
        }
        Ok(buf.len())
    }

    fn read_at(&self, _buf: &mut [u8], _pos: usize) -> crate::utils::errno::ErrnoOr<usize> {
        todo!()
    }

    fn write(&self, buf: &[u8]) -> crate::utils::errno::ErrnoOr<usize> {
        // stub
        assert_eq!(self.id, 0);
        


        // TODO: this should remove itself, after buffers moved to tty
        DISPLAY.write().write_bytes(buf);
        Ok(buf.len())
    }

    fn len(&self) -> usize {
        todo!()
    }

    fn get_type(&self) -> super::fs::FileType {
        todo!()
    }

    unsafe fn close(&self) {} // do nothing
}

impl TtyDriver {
    pub const fn new(id: usize) -> Self {
        Self {
            id,
            line_queue: Mutable::new(VecDeque::new()), 
            raw_queue: Mutable::new(VecDeque::new()),
            c_iflags: 0,
            c_oflags: 0,
            c_cflags: 0,
            c_lflags: 0,
            c_cc: [0; 32],

            escape: false,
            csi: false,
            csi_param: vec![],
        }
    }
}

static mut SHIFT: bool = false;
static mut CTRL: bool = false;
static mut ALT: bool = false;
static mut SUPER_: bool = false;

fn keyboard_in(sc: Scancode) {
    let key = (KEYBOARD_LAYOUT.read().convert)(sc.code);

    match key {
        layout::Key::Ctrl => {
            unsafe {
                CTRL = !sc.released;
            }
        },
        layout::Key::Alt => {
            unsafe {
                ALT = !sc.released;
            }
        },
        layout::Key::Shift => {
            unsafe {
                SHIFT = !sc.released;
            }
        },
        layout::Key::Super => {
            unsafe {
                SUPER_ = !sc.released;
            }
        },
        layout::Key::Invalid => {},
        layout::Key::Key(normal, shifted) => {
            if sc.released {
                return;
            }

            let tty = &TTYS[*CURRENT_TTY.read()];
            if tty.c_lflags & ECHO != 0 {
                todo!();
            }
            if tty.c_lflags & ICANON != 0 {
                todo!();
            }

            for a in if unsafe { SHIFT } { shifted } else { normal } {
                tty.raw_queue.write().push_back(*a);
            }
        },
    }
}

#[derive(Debug)]
struct TtyFd {
    tty: usize,
}

impl Fd for TtyFd {
    fn read(&mut self, buf: &mut [u8]) -> crate::utils::errno::ErrnoOr<usize> {
        TTYS[self.tty].read(buf)
    }

    fn read_at(&mut self, buf: &mut [u8], pos: usize) -> crate::utils::errno::ErrnoOr<usize> {
        TTYS[self.tty].read_at(buf, pos)
    }

    fn write(&mut self, buf: &[u8]) -> crate::utils::errno::ErrnoOr<usize> {
        TTYS[self.tty].write(buf)
    }

    fn len(&self) -> usize {
        TTYS[self.tty].len()
    }

    fn get_type(&self) -> super::fs::FileType {
        TTYS[self.tty].get_type()
    }

    fn clonefd(&self) -> Box<dyn Fd> {
        Box::new(Self { tty: self.tty })
    }

    unsafe fn close(&mut self) {
        TTYS[self.tty].close()
    }
}

pub fn tty_init() {
    let drv = DeviceDriver {
        open_device: |id| Ok(Box::new(TtyFd { tty: id })),
    };
    dev::register_device_driver(TTY_MAJOR, drv);

    devfs::add_synthetic_file("/tty0", TmpFile::Char(TTY_MAJOR, 0)).unwrap();
    set_keyboard_handler(keyboard_in);
}
