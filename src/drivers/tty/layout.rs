use crate::utils::lock::Lock;

#[derive(Debug)]
pub enum Key {
    Key(&'static [u8], &'static [u8]),
    Ctrl,
    Alt,
    Shift,
    Super,
    Invalid,
}

pub struct KeyboardLayout {
    pub name: &'static str,
    pub convert: fn(u8) -> Key,
}

pub static KEYBOARD_LAYOUT: Lock<KeyboardLayout> = Lock::new(KeyboardLayout {
    name: "US",
    convert: us_convert,
});

fn us_convert(scancode: u8) -> Key {
    match scancode {
        0x01 => Key::Key(b"\x1b", b"\x1b"),
        0x02 => Key::Key(b"1", b"!"),
        0x03 => Key::Key(b"2", b"@"),
        0x04 => Key::Key(b"3", b"#"),
        0x05 => Key::Key(b"4", b"$"),
        0x06 => Key::Key(b"5", b"%"),
        0x07 => Key::Key(b"6", b"^"),
        0x08 => Key::Key(b"7", b"&"),
        0x09 => Key::Key(b"8", b"*"),
        0x0a => Key::Key(b"9", b"("),
        0x0b => Key::Key(b"0", b")"),
        0x0c => Key::Key(b"-", b"_"),
        0x0d => Key::Key(b"=", b"+"),
        0x0e => Key::Key(b"\x08", b"\x08"),
        0x0f => Key::Key(b"\t", b"\t"),
        0x10 => Key::Key(b"q", b"Q"),
        0x11 => Key::Key(b"w", b"W"),
        0x12 => Key::Key(b"e", b"E"),
        0x13 => Key::Key(b"r", b"R"),
        0x14 => Key::Key(b"t", b"T"),
        0x15 => Key::Key(b"y", b"Y"),
        0x16 => Key::Key(b"u", b"U"),
        0x17 => Key::Key(b"i", b"I"),
        0x18 => Key::Key(b"o", b"O"),
        0x19 => Key::Key(b"p", b"P"),
        0x1a => Key::Key(b"[", b"{"),
        0x1b => Key::Key(b"]", b"}"),
        0x1c => Key::Key(b"\n", b"\n"),
        0x1d => Key::Ctrl,
        0x1e => Key::Key(b"a", b"A"),
        0x1f => Key::Key(b"s", b"S"),
        0x20 => Key::Key(b"d", b"D"),
        0x21 => Key::Key(b"f", b"F"),
        0x22 => Key::Key(b"g", b"G"),
        0x23 => Key::Key(b"h", b"H"),
        0x24 => Key::Key(b"j", b"J"),
        0x25 => Key::Key(b"k", b"K"),
        0x26 => Key::Key(b"l", b"L"),
        0x27 => Key::Key(b";", b":"),
        0x28 => Key::Key(b"'", b"\""),
        0x29 => Key::Key(b"`", b"~"),
        0x2a => Key::Shift,
        0x2b => Key::Key(b"\\", b"|"),
        0x2c => Key::Key(b"z", b"Z"),
        0x2d => Key::Key(b"x", b"X"),
        0x2e => Key::Key(b"c", b"C"),
        0x2f => Key::Key(b"v", b"V"),
        0x30 => Key::Key(b"b", b"B"),
        0x31 => Key::Key(b"n", b"N"),
        0x32 => Key::Key(b"m", b"M"),
        0x33 => Key::Key(b",", b"<"),
        0x34 => Key::Key(b".", b">"),
        0x35 => Key::Key(b"/", b"?"),
        0x36 => Key::Shift,
        0x37 => Key::Key(b"*", b"*"),
        0x38 => Key::Alt,
        0x39 => Key::Key(b" ", b" "),
        _ => Key::Invalid
    }
}
