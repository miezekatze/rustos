use alloc::{vec::Vec, vec};
use crate::{utils::errno::{Errno, ErrnoOr}, rust_utils::SizedStr};
use super::dev::{dev_open, BLOCK_SIZE};

#[derive(Debug, Clone, Copy)]
#[repr(packed)]
#[allow(unused)]
struct Gpt {
    magic: SizedStr<8>,
    revision: u32,
    header_size: u32,
    crc32_checksum: u32,
    reserved: [u8; 4],
    header_lba: u64,
    alternate_header_lba: u64,
    first_usable_block: u64,
    last_usable_block: u64,
    disk_guid: u128,
    gpe_start_lba: u64,
    parts: u32,
    part_entry_size: u32,
    crc32_pe_array: u32,
}

// #[repr(packed)]
#[derive(Debug, Clone, Copy)]
pub struct GptEntry {
    pub type_guid: u128,
    pub part_guid: u128,
    pub start_lba: u64,
    pub end_lba: u64,
    pub attrs: u64,
    pub name: [u8; 72],
}

pub fn gpt_detect(maj: usize, min: usize) -> ErrnoOr<Vec<GptEntry>> {
    let mut fd = dev_open(maj, min)?;

    let mut lba1 = [0u8; BLOCK_SIZE];
    fd.read_at(&mut lba1, BLOCK_SIZE)?;

    if &lba1[..8] != b"EFI PART" {
        return Err(Errno::EINVAL);
    }
    let gpt = unsafe {*(lba1.as_ptr() as *const Gpt)};

    let entries_size = gpt.parts * gpt.part_entry_size;
    let mut entries = vec![0u8; entries_size as usize];
    fd.read_at(&mut entries, gpt.gpe_start_lba as usize * BLOCK_SIZE)?;

    let mut v = vec![];
    
    for a in 0 .. gpt.parts {
        let data = &entries[(a * gpt.part_entry_size) as usize ..
                            ((a+1) * gpt.part_entry_size) as usize];
        let entry = unsafe {*(data.as_ptr() as *const GptEntry)};

        if entry.type_guid != 0 {
            v.push(entry);
        }
    }

    Ok(v)
}
