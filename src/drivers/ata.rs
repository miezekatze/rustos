use core::arch::asm;

use alloc::{vec, vec::Vec, boxed::Box, format, string::ToString};

use crate::{io::ports::IOPort, println, utils::{lock::Lock, errno::{Errno, ErrnoOr}}, drivers::fs::{devfs::add_synthetic_file, tmpfs::TmpFile}};
use super::{dev::{register_device_driver, DeviceDriver, BlockFd, blockdev}, fs::Fd, gpt::gpt_detect, pci::{PCIHeader, pci_read_bar}};

#[derive(Clone, Copy)]
struct ATAPort(u16);
impl ATAPort {
    pub fn from_drive(drive: ATADrive) -> ErrnoOr<Self> {
        match drive.0 {
            0 => Ok(Self(0x1F0)),
            1 => Ok(Self(0x170)),
            2 => Ok(Self(0x1E8)),
            3 => Ok(Self(0x168)),
            _ => Err(Errno::EIO)
        }
    }

    pub fn io_reg(&self, reg: ATARegs) -> u16 {
        self.0 + reg as u16
    }
}

#[repr(u16)]
pub enum ATARegs {
    Data = 0,
    Features = 1,
    SectorCount = 2,
    LbaLow = 3,
    LbaMid = 4,
    LbaHigh = 5,
    DriveSelect = 6,
    StatusCmd = 7,
}

#[repr(u8)]
#[derive(Clone, Copy, Debug)]
pub enum ATACommand {
    Identify = 0xec,
    ReadSectorsExt = 0x24,
}

impl ATACommand {
    pub fn send(&self, port: u16) {
        unsafe {(*self as u8).pout(port)}
    }
}

const ATA_BSY: u8 = 1<<7;
const ATA_DRQ: u8 = 1<<3;   // <3 :D
const ATA_ERR: u8 = 1<<0;
//const ATA_RDY: u8 = 1<<6;

#[derive(Clone, Copy, Debug)]
pub struct ATADrive(u8, bool);
impl core::fmt::Display for ATADrive {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Drive({} {})", self.0, if self.1 { "master" } else { "slave" })
    }
}

//const MAX_DRIVES: u8 = 4;

#[derive(Debug, Clone)]
pub enum AtaDev {
    Drive {
        drive: ATADrive,
        info: DriveInfo,
        pci_dev: PCIHeader,
    },
    Partition(Box<AtaDev>, usize, usize)
}

#[derive(Debug, Clone, Copy)]
#[repr(align(4))]
struct Prd {
    addr: u32,
    len: u16,
    flags: u16,
}

#[derive(Debug, Clone, Copy)]
#[repr(align(65536))]
struct Prdt([Prd; 256]);

static mut PRDT: Prdt = Prdt([Prd { addr: 0, len: 0, flags: 0 }; 256]);

impl AtaDev {
    fn len(&self) -> usize {
        match &self {
            AtaDev::Drive { info, .. } => {
                match info {
                    DriveInfo::Ata { lba48_sectors, .. } => *lba48_sectors as usize * 512,
                    _ => 0
                }
            },
            AtaDev::Partition(_, min, max) => (max - min) * 512
        }
    }

    fn read_sector(&self, start_sector: usize, mut count: u16, buf: &mut [u8]) -> ErrnoOr<()> {
        let len = buf.len().min(count as usize * 512);

        match &self {
            AtaDev::Partition(dev, min, max) => {
                let drive_start_sect = start_sector + min;
                if drive_start_sect >= *max {
                    return Ok(());
                }
                let drive_end_sect = drive_start_sect + count as usize;
                if drive_end_sect >= *max {
                    count = (*max - drive_start_sect) as u16;
                }

                dev.read_sector(drive_start_sect, count, buf)
            },
            AtaDev::Drive { drive, info, pci_dev } => {
                let bar4 = pci_read_bar(pci_dev.bus, pci_dev.slot, pci_dev.func, 4);
//                println!("bar4: {:#x}", bar4);
                assert!(bar4 & 1 == 1);
                let port_base = bar4 & !0x3;
                assert!(port_base < 0x10000);
                let port_base = port_base as u16;

                const BUS_MASTER_COMMAND: u16 = 0x0;
                const BUS_MASTER_STATUS: u16 = 0x2;
                const BUS_MASTER_PRDT: u16 = 0x4;

                let max_sects = match info {
                    DriveInfo::Ata { lba48_supported, lba48_sectors } => {
                        if ! lba48_supported {
                            return Err(Errno::EINVAL)
                        }
                        *lba48_sectors as usize
                    },
                    _ => return Err(Errno::EINVAL),
                };

                if start_sector >= max_sects {
                    return Ok(())
                } else if start_sector + count as usize > max_sects {
                    count = (max_sects - start_sector) as u16;
                }

                let _dma_read = |start_frame: crate::mem::Physical, end_frame: crate::mem::Physical| {
                    let prdt = unsafe { &mut PRDT.0 };
                    for a in (start_frame.0 .. end_frame.0).step_by(65536) {
                        let mut len = (end_frame.0 - a).min(65536);
                        if len == 65536 {
                            len = 0;
                        }
                        let prd = &mut prdt[(a - start_frame.0) /65536];
                        assert!(a < 0x100000000);
                        prd.addr = a as u32;
                        prd.len = len as u16;
                        prd.flags = 0;
                    }
                    let last_idx = (end_frame.0 - start_frame.0 - 1) / 65536;
                    prdt[last_idx as usize].flags = 0x8000;

                    let port = ATAPort::from_drive(*drive)?;
                    let port_base = if drive.1 { port_base + 0x8 } else { port_base };
                    unsafe {
                        asm!("cli");
                        // reset command register
                        0u8.pout(port_base as u16 + BUS_MASTER_COMMAND);

                        // enable bus master
                        let val = u8::pin(port_base as u16 + BUS_MASTER_COMMAND);
                        (val | 0x04).pout(port_base as u16 + BUS_MASTER_COMMAND);

                        // set PRDT
                        let prdt_addr = &PRDT as *const _ as u64;
                        assert!(prdt_addr < 0x100000000);
                        (prdt_addr as u32).pout(port_base as u16 + BUS_MASTER_PRDT);

                        // set DMA mode
                        let val = u8::pin(port_base as u16 + BUS_MASTER_COMMAND);
                        (val | 0x8).pout(port_base as u16 + BUS_MASTER_COMMAND);

                        // clear interrupt and error
                        let val = u8::pin(port_base as u16 + BUS_MASTER_STATUS);
                        (val & !(0x04 | 0x02)).pout(port_base as u16 + BUS_MASTER_STATUS);

                        // wait for ready
                        while (u8::pin(port.io_reg(ATARegs::StatusCmd)) & 0x80) != 0 {}

                        // select drive
                        if drive.1 {
                            0xE0u8 | ((start_sector >> 24) as u8 & 0xf)
                        } else {
                            0xF0u8 | ((start_sector >> 24) as u8 & 0xf)
                        }.pout(port.io_reg(ATARegs::DriveSelect));

                        // wait for ready
//                        ata_poll(port);
//                        println!("ready / {}", last_idx);

                        // set features
                        0x00u8.pout(port.io_reg(ATARegs::Features));

                        // send LBA and sector count
//                        ((count >> 8) as u8).pout(port.io_reg(ATARegs::SectorCount));
//                        ((start_sector >> (8*3)) as u8).pout(port.io_reg(ATARegs::LbaLow));
//                        ((start_sector >> (8*4)) as u8).pout(port.io_reg(ATARegs::LbaMid));
//                        ((start_sector >> (8*5)) as u8).pout(port.io_reg(ATARegs::LbaHigh));

                        (count as u8).pout(port.io_reg(ATARegs::SectorCount));
                        ((start_sector >> (8*0)) as u8).pout(port.io_reg(ATARegs::LbaLow));
                        ((start_sector >> (8*1)) as u8).pout(port.io_reg(ATARegs::LbaMid));
                        ((start_sector >> (8*2)) as u8).pout(port.io_reg(ATARegs::LbaHigh));

                        ata_poll_bsy(port);

                        // send command
                        0xc8u8.pout(port.io_reg(ATARegs::StatusCmd));

                        ata_poll_bsy(port);

                        u8::pin(port_base as u16 + BUS_MASTER_COMMAND);
                        u8::pin(port_base as u16 + BUS_MASTER_STATUS);

                        // set start/stop bit
                        let val = u8::pin(port_base as u16 + BUS_MASTER_COMMAND);
                        (val | 1).pout(port_base as u16 + BUS_MASTER_COMMAND);

                        // wait for interrupts
                        for _ in 0 ..= last_idx {
                            while (u8::pin(port_base as u16 + BUS_MASTER_STATUS) & 0x04) == 0 {
                                println!("waiting for dma interrupt: {:#x}", u8::pin(port_base as u16 + BUS_MASTER_STATUS));
                            }
                            println!("dma interrupt");
                            // clear interrupt
                            let val = u8::pin(port_base as u16 + BUS_MASTER_STATUS);
                            (val | 0x04).pout(port_base as u16 + BUS_MASTER_STATUS);
                        }

                        // clear interrupt and error
                        let val = u8::pin(port_base as u16 + BUS_MASTER_STATUS);
                        (val | 0x04 | 0x02).pout(port_base as u16 + BUS_MASTER_STATUS);

                        // clear start/stop bit
                        let val = u8::pin(port_base as u16 + BUS_MASTER_COMMAND);
                        (val & !0x01).pout(port_base as u16 + BUS_MASTER_COMMAND);

                        // read status
                        let status = u8::pin(port.io_reg(ATARegs::StatusCmd));
                        if status & 0x01 != 0 {
                            return Err(Errno::EIO)
                        }

                        // clear PRDT
                        (0u32).pout(port_base as u16 + BUS_MASTER_PRDT);

                        // clear DMA mode
                        0x00u8.pout(port_base as u16 + BUS_MASTER_COMMAND);

                        asm!("sti");
                    }
                    Ok(())
                };

                // get all physical frames
/*                let tbl = ActivePageTable::get();
                let mut start_frame = tbl.translate(Virtual(buf.as_ptr() as usize)).unwrap();
                let mut last_frame = start_frame;

                for a in (PAGE_SIZE .. buf.len()).step_by(PAGE_SIZE) {
                    let addr = buf.as_ptr() as usize + a;
                    let frame = tbl.translate(Virtual(addr)).unwrap();

                    if frame.0 != last_frame.0 + PAGE_SIZE {
                        dma_read(start_frame, frame)?;
                        start_frame = frame;
                    }
                    last_frame = frame;
                }
                dma_read(start_frame, Physical(last_frame.0 + PAGE_SIZE))?;*/
//                return Ok(());

//                Ok(())
                
                let port = ATAPort::from_drive(*drive)?;
                
                unsafe {
                    if drive.1 {
                        0x40u8
                    } else {
                        0x50
                    }.pout(port.io_reg(ATARegs::DriveSelect));

                    ((count >> 8) as u8).pout(port.io_reg(ATARegs::SectorCount));
                    ((start_sector >> (8*3)) as u8).pout(port.io_reg(ATARegs::LbaLow));
                    ((start_sector >> (8*4)) as u8).pout(port.io_reg(ATARegs::LbaMid));
                    ((start_sector >> (8*5)) as u8).pout(port.io_reg(ATARegs::LbaHigh));

                    (count as u8).pout(port.io_reg(ATARegs::SectorCount));
                    (start_sector as u8).pout(port.io_reg(ATARegs::LbaLow));
                    ((start_sector >> 8) as u8).pout(port.io_reg(ATARegs::LbaMid));
                    ((start_sector >> (8*2)) as u8).pout(port.io_reg(ATARegs::LbaHigh));

                    (ATACommand::ReadSectorsExt as u8).pout(port.io_reg(ATARegs::StatusCmd));
                    for i in 0 .. count {
                        ata_poll_drq(port);
//                        ata_poll_bsy(port);
                        let buf_off = i as usize * 512;

                        for a in 0 .. 256 {
                            let word = u16::pin(port.io_reg(ATARegs::Data));
                            if buf_off + a*2+1 >= len {
                                continue;
                            }
                            buf[buf_off + a*2] = word as u8;
                            buf[buf_off + a*2 + 1] = (word >> 8) as u8;
                        }
                    }
//                    println!("buf: {:?}", buf);
                    Ok(())
                }

                // [0; SECTOR_SIZE]
            }
        }
    }
}

const ATA_DRV: usize = 8;

#[derive(Debug, Clone)]
pub struct AtaDevFd {
    pub dev: AtaDev,
    pub minor: usize
}

impl BlockFd for AtaDevFd {
    fn read_sector_uncached(&mut self, start_sector: usize, count: u16, buf: &mut [u8]) -> ErrnoOr<()> {
        self.dev.read_sector(start_sector, count, buf)
    }

    fn len(&self) -> usize {
        self.dev.len()
    }

    fn get_type(&self) -> super::fs::FileType {
        super::fs::FileType::BlockDev(ATA_DRV, self.minor)
    }

    fn clonefd(&self) -> Box<dyn BlockFd> {
        Box::new(self.clone()) 
    }
}

static ATA_DEVS: Lock<Vec<AtaDev>> = Lock::new(vec![]);

fn open_ata_dev(min: usize) -> ErrnoOr<Box<dyn Fd>> {
    let guard = ATA_DEVS.read();
    let dev = guard.get(min).ok_or(Errno::ENXIO)?;
    Ok(blockdev(Box::new(AtaDevFd {
        dev: dev.clone(),
        minor: min,
    })))
}

#[inline(never)]
pub fn ata_init(devices: &[PCIHeader]) -> ErrnoOr<()> {
    let mut pci_devs = vec![];
    for dev in devices {
        if dev.class == 0x1 && dev.sub_class == 0x1 {
           pci_devs.push(dev.clone());
        }
    }

    register_device_driver(ATA_DRV, DeviceDriver {
        open_device: open_ata_dev,
    });
    
    let mut ata_letter = 'a';
    let mut cdrom_num = 0;
    for drive in 0 .. pci_devs.len() {
        for a in [true, false] {
            let drv = ATADrive(drive as u8, a);
            let r = ata_detect(drv)?;
            let min = ATA_DEVS.read().len();
            let (dev, prefix) = match r {
                DriveInfo::Ata { .. } => {
                    let pref = format!("/hd{ata_letter}");
                    match add_synthetic_file(&pref, TmpFile::Block(ATA_DRV, min)) {
                        Ok(()) => (),
                        Err(e) => panic!("Could not add synthetic file: {e}"),
                    }
                     ata_letter = (ata_letter as u8 + 1) as char;

                    (AtaDev::Drive {
                        drive: drv,
                        info: r,
                        pci_dev: pci_devs[drive].clone(),
                    }, pref)
                 },
                DriveInfo::AtaPi { .. } => {
                    let mut pref = format!("/cdrom{cdrom_num}");
                    match add_synthetic_file(&pref, TmpFile::Block(ATA_DRV, min)) {
                        Ok(()) => (),
                        Err(e) => panic!("Could not add synthetic file: {e}"),
                    }

                    cdrom_num += 1;
                    pref += "p";
                    (AtaDev::Drive {
                        drive: drv,
                        info: r,
                        pci_dev: pci_devs[drive].clone(),
                    }, pref)
                },
                DriveInfo::NonExistend => {
                    continue;
                },
            };

            ATA_DEVS.write().push(dev.clone());
            let parts = match gpt_detect(ATA_DRV, min) {
                Ok(p) => p,
                Err(Errno::EINVAL) => continue,
                Err(e) => return Err(e),
            };

            let min = ATA_DEVS.read().len();
            for (i, part) in parts.iter().enumerate() {
                ATA_DEVS.write().push(AtaDev::Partition(Box::new(dev.clone()), part.start_lba as usize, part.end_lba as usize));
                match add_synthetic_file(&(prefix.clone() + &(i+1).to_string()), TmpFile::Block(ATA_DRV, min + i)) {
                    Ok(()) => (),
                    Err(e) => panic!("Could not add synthetic file: {e}"),
                }
            }
        }
    }
    Ok(())
}

#[derive(Debug, Clone, Copy)]
pub enum DriveInfo {
    Ata {
        lba48_supported: bool,
        lba48_sectors: u64,
    },
    AtaPi {},
    NonExistend
}

fn ata_poll_bsy(port: ATAPort) {
    unsafe {
        let mut v = u8::pin(port.io_reg(ATARegs::StatusCmd));
        loop {
            if v & ATA_BSY == 0 {
                break;
            }
            if v & ATA_ERR != 0 {
                let err = u8::pin(port.io_reg(ATARegs::Features));
                println!("error: {:#x}", err);
                return;
            }
            v = u8::pin(port.io_reg(ATARegs::StatusCmd));
        }
    }
}

fn ata_poll_drq(port: ATAPort) {
    unsafe {
        let mut v = u8::pin(port.io_reg(ATARegs::StatusCmd));
        loop {
            if v & ATA_DRQ != 0 {
                break;
            }
            if v & ATA_ERR != 0 {
                let err = u8::pin(port.io_reg(ATARegs::Features));
                println!("error: {:#x}", err);
                return;
            }
            v = u8::pin(port.io_reg(ATARegs::StatusCmd));
        }
    }
}

fn ata_detect(drive: ATADrive) -> ErrnoOr<DriveInfo> {
    let port = ATAPort::from_drive(drive)?;
    let v = unsafe {u8::pin(port.io_reg(ATARegs::StatusCmd))};

    if v == 0xff {
        return Ok(DriveInfo::NonExistend);
    }

    unsafe {
        if drive.1 {
            0xA0u8
        } else {
            0xB0
        }.pout(port.io_reg(ATARegs::DriveSelect));
        0u8.pout(port.io_reg(ATARegs::LbaHigh));
        0u8.pout(port.io_reg(ATARegs::LbaMid));
        0u8.pout(port.io_reg(ATARegs::LbaLow));

        ATACommand::Identify.send(port.io_reg(ATARegs::StatusCmd));

        let mut v = u8::pin(port.io_reg(ATARegs::StatusCmd));
        if v == 0 {
            return Ok(DriveInfo::NonExistend);
        }
        ata_poll_drq(port);

        while v & ATA_DRQ == 0 && v & ATA_ERR != 0 {
            let lbah = u8::pin(port.io_reg(ATARegs::LbaHigh));
            let lbam = u8::pin(port.io_reg(ATARegs::LbaMid));
            if lbah != 0 || lbam != 0 {
                break;
            }
            v = u8::pin(port.io_reg(ATARegs::StatusCmd));
        }

        let mut atapi = false;
        // if v & ATA_ERR != 0 {
        //     // ignore
        // }

        let high = u8::pin(port.io_reg(ATARegs::LbaHigh));
        let mid = u8::pin(port.io_reg(ATARegs::LbaMid));
        if mid == 0x14 && high == 0xeb {
            atapi = true;
        } else if mid == 0x3c && high == 0xc3 {
            println!("sata");
        }

        if ! atapi {
            let mut vals = [0u16; 256];
            for a in &mut vals {
                let word = u16::pin(port.io_reg(ATARegs::Data));
                *a = word;
            }
            // asm!("rep insw", in("ecx") 256, in("dx") port.io_reg(ATARegs::Data), in("rdi") vals.as_ptr());
            let lba48_supported = vals[83] & 1 << 10 != 0;
            let lba48_sectors = u64::from_le_bytes(vals[100..104].align_to::<u8>().1.try_into().unwrap());
            Ok(DriveInfo::Ata {
                lba48_supported,
                lba48_sectors,
            })
        } else {
            Ok(DriveInfo::AtaPi {  })
        }
    }
}
