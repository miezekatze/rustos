use core::arch::asm;

use alloc::boxed::Box;

use crate::{io::ports::IOPort, utils::{errno::{ErrnoOr, Errno}, lock::Lock}, println};
use super::{dev::DeviceDriver, fs::{Fd, tmpfs::TmpFile}};
use alloc::{vec, vec::Vec};

const DSP_RESET: u16 = 0x226;
const DSP_READ: u16 = 0x22A;
const DSP_WRITE: u16 = 0x22C;
const DSP_MIXER: u16 = 0x224;
const DSP_MIXER_DATA: u16 = 0x225;

const DSP_DRV: usize = 14;

pub fn init_sb16() -> ErrnoOr<()> {
    // reset dsp
    unsafe {
        1u8.pout(DSP_RESET);
        // wait 3 microseconds?
        for _ in 0..0xffff {
            asm!("nop");
        }
        0u8.pout(DSP_RESET);

        let val = u8::pin(DSP_READ);
        if val != 0xAA {
            return Err(Errno::ENODEV);
        }

        // check version
        0xE1u8.pout(DSP_WRITE);
        let val = u8::pin(DSP_READ);
        u8::pin(DSP_READ);
        if val < 4 {
            return Err(Errno::ENODEV);
        }

        // set irq
        0x80u8.pout(DSP_MIXER);
        0x02u8.pout(DSP_MIXER); // IRQ 5
    }

    // init dev device
    crate::drivers::dev::register_device_driver(DSP_DRV, DeviceDriver { open_device: |min| match min {
        0 => Ok(Box::new(Dsp)),
        _ => Err(Errno::EINVAL),
    }});
    
    crate::drivers::fs::devfs::add_synthetic_file("/dsp", TmpFile::Char(DSP_DRV, 0))?;

    Ok(())
}

#[derive(Debug, Clone)]
struct DspDriver {
    data: [(Vec<u8>, usize); 1],
}

static DSP_DRIVER: Lock<DspDriver> = Lock::new(DspDriver {
    data: [(Vec::new(), 0); 1],
});

static DSP_LOADED_BUF: Lock<[u8; 1024*2]> = Lock::new([0; 1024*2]);

#[derive(Debug)]
#[repr(C)]
struct DspHeader {
    magic: [u8; 4],
    data_size: u32,
    num_samples: u32,
    sample_rate: u32,
    channels: u8,
    bits_per_sample: u8,
    data: *const u8,
}

#[derive(Debug)]
struct Dsp;

impl Fd for Dsp {
    fn read(&mut self, _buf: &mut [u8]) -> ErrnoOr<usize> {
        todo!()
    }

    fn read_at(&mut self, _buf: &mut [u8], _pos: usize) -> ErrnoOr<usize> {
        todo!()
    }

    fn write(&mut self, buf: &[u8]) -> ErrnoOr<usize> {
        if buf.len() < core::mem::size_of::<DspHeader>() {
            println!("HERE: {} < {}", buf.len(), core::mem::size_of::<DspHeader>());
            return Err(Errno::EINVAL);
        }

        let header = unsafe { &*(buf.as_ptr() as *const DspHeader) };

        unsafe {
            1u8.pout(DSP_RESET);
            // wait 3 microseconds?
            for _ in 0..0xffff {
                asm!("nop");
            }
            0u8.pout(DSP_RESET);

            if header.bits_per_sample == 16 {
                // disable channel
                0x05u8.pout(0xd4);
                // flip-flop
                0xFFu8.pout(0xd8);
                // send page number
                let ptr = DSP_LOADED_BUF.read().as_ptr();
                (ptr as usize as u8).pout(0xc4);
                ((ptr as usize >> 8) as u8).pout(0xc4);
                // send page
                ((ptr as usize >> 16) as u8).pout(0x8b);

                // flip-flop back
                0xFFu8.pout(0xd8);

                // send count
                0xffu8.pout(0xc6);
                0x00u8.pout(0xc6);

                // transfer mode
                0x58u8.pout(0xd6);

                // enable channel
                0x01u8.pout(0xd4);
            } else {
                todo!();
            }

            // acknoledge?
            u8::pin(0x22e);
            u8::pin(0x22f);

            // load first data to memory
            let data = header.data;
//            let count = header.data_size;
            DSP_DRIVER.write().data[0].0 = alloc::vec::Vec::from_raw_parts(data as *mut _, header.data_size as usize, header.data_size as usize);
            DSP_DRIVER.write().data[0].1 = header.data_size as usize;

            let mut buf = DSP_LOADED_BUF.write();
            buf.copy_from_slice(core::slice::from_raw_parts(data, 1024*2));

            // set volume
            0x22u8.pout(DSP_MIXER);
            0xccu8.pout(DSP_MIXER_DATA);

            // turn on speaker
            0xd1u8.pout(DSP_WRITE);

            // set sample rate
            0x40u8.pout(DSP_WRITE);
            let time_constant = (256 - (1000000 / (header.channels as u32 * header.sample_rate))) as u8;
            time_constant.pout(DSP_WRITE);

            // write mode
            0xb0u8.pout(DSP_WRITE);

            // write type
            (0x0 | ((header.channels - 1) << 5) | 0 << 4).pout(DSP_WRITE);

            // write count
            (header.data_size as u16).pout(DSP_WRITE);
            ((header.data_size >> 8) as u16).pout(DSP_WRITE);
        }

        Ok(buf.len())
    }

    fn len(&self) -> usize {
        todo!()
    }

    fn get_type(&self) -> super::fs::FileType {
        todo!()
    }

    fn clonefd(&self) -> Box<dyn Fd> {
        Box::new(Dsp)
    }

    unsafe fn close(&mut self) {}
}
