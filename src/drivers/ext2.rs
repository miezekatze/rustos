use core::fmt::Write;
use alloc::{boxed::Box, vec, vec::Vec, format, string::{String, ToString}, sync::Arc};

use crate::{utils::{errno::{Errno, ErrnoOr}, lock::Lock}, drivers::fs::Traverse, rust_utils::{SizedCStr, Unused}, println};
use super::{fs::{VFS, MountDriverT, MountDriver, Path, open, format_path, Fd, CPathS, CPath, DirFd, DirEntries, DirEntry, O_RDONLY, FileDescriptor}, debugcon::DebugCon};

#[macro_export]
macro_rules! div_ceil {
    ($a:expr, $b:expr) => {{
        let rem = $a % $b;
        let div = $a / $b;
        if rem == 0 {
            div
        } else {
            div + 1
        }
    }};
}

#[derive(Debug, Clone, Copy)]
#[allow(unused)]
#[repr(C)]
struct Superblock {
    inodes: u32,
    blocks: u32,
    su_blocks: u32,
    unalloc_blocks: u32,
    unalloc_inodes: u32,
    superblock: u32,
    shift1024left_block_size: u32,
    shift1024left_fragment_size: u32,
    blocks_per_group: u32,
    fragments_per_group: u32,
    inodes_per_group: u32,
    last_mount_time: u32,
    last_written_time: u32,
    mounts_since_last_check: u16,
    mounts_until_next_check: u16,
    ext2_magic: u16,
    fs_state: u16,
    err_handle_method: u16,
    minor_version: u16,
    last_check: u32,
    checks_interval: u32,
    osid: u32,
    major_version: u32,
    reserved_uid: u16,
    reserved_gid: u16,
    // EXTENDED
    first_non_reserved: u32,
    inode_size: u16,
    superblock_group: u16,
    optional_featuers: u32,
    required_features: u32,
    reaedonly_features: u32,
    fsid: u128,
    volname: SizedCStr<16>,
    last_mnt: SizedCStr<64>,
    compressions_used: u32,
    prealloc_files: u8,
    prealloc_dirs: u8,
    unused: u16,
    journalid: u128,
    journal_inode: u32,
    journal_dev: u32,
    orphan_list_head: u32,
}

impl Superblock {
    #[inline(always)]
    const fn block_size(&self) -> usize {
        1024 << self.shift1024left_block_size
    }
}

#[derive(Debug, Clone, Copy)]
#[allow(unused)]
#[repr(C)]
struct BlockGroupDescriptor {
    block_usage_bitmap: u32,
    inode_usage_bitmap: u32,
    inode_table: u32,
    unalloc_blocks: u16,
    unalloc_inodes: u16,
    dirs: u16,
    __unused: Unused<14>
}

#[derive(Debug)]
#[allow(unused)]
struct Ext2Driver {
    dev: FileDescriptor,
    mnt: Path,
    superblock: Superblock,
    block_groups: Vec<BlockGroupDescriptor>,
}

#[allow(unused)]
#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Inode {
    type_perm: TypePerms,
    uid: u16,
    size_low: u32,
    last_access: u32,
    creation: u32,
    last_mod: u32,
    del_time: u32,
    group_id: u16,
    hard_links: u16,
    used_disk_sector: u32,
    flags: u32,
    _oss_spec1: Unused<4>,
    direct_ptrs: [u32; 12],
    single_indirect: u32,
    double_indirect: u32,
    triple_indirect: u32,
    generation_number: u32,
    file_acl: u32,
    size_high_or_dir_acl: u32,
    fragment_block_addr: u32,
    _oss_spec2: Unused<12>
}

impl Inode {
    fn size(&self, block: &Superblock) -> usize {
        if block.reaedonly_features & 0x2 != 0 {
            ((self.size_high_or_dir_acl as usize) << 4) | self.size_low as usize
        } else {
            self.size_low as usize
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
#[repr(u8)]
enum Type {
    Fifo = 0x1,
    CharDev = 0x2,
    Dir = 0x4,
    BlockDev = 0x6,
    File = 0x8,
    Symlink = 0xa,
    Socket = 0xc,
}

impl Type {
    fn to_file_type(self) -> super::fs::FileType {
        use super::fs::FileType::*;
        match self {
            Type::Fifo => Fifo,
            Type::CharDev => File,
            Type::Dir => Dir,
            Type::BlockDev => BlockDev(0, 0),
            Type::File => File,
            Type::Symlink => unreachable!("should be dereferenced"),
            Type::Socket => Socket,
        }
    }
}

#[derive(Clone, Copy)]
struct TypePerms(u16);

impl TypePerms {
    fn typ(&self) -> Type {
        match self.0 >> 12 {
            x if x == Type::Fifo as u16 => Type::Fifo,
            x if x == Type::CharDev as u16 => Type::CharDev,
            x if x == Type::Dir as u16 => Type::Dir,
            x if x == Type::BlockDev as u16 => Type::BlockDev,
            x if x == Type::File as u16 => Type::File,
            x if x == Type::Symlink as u16 => Type::Symlink,
            x if x == Type::Socket as u16 => Type::Socket,
            _ => Type::File
        }
    }

    fn perms(&self) -> (u8, u8, u8, u8) {
        let other = self.0 & 7;
        let group = (self.0 >> 3) & 7;
        let user = (self.0 >> 6) & 7;
        let special = (self.0 >> 9) & 7;
        (special as u8, user as u8, group as u8, other as u8)
    }

    fn format_perms(&self) -> String {
        let (special, user, group, other) = self.perms();

        fn f(p: u8, x1: char, x2: char) -> String {
            format!("{}{}{}", if p & 0b100 != 0 {'r'} else {'-'},
                    if p & 0b010 != 0 {'w'} else {'-'},
                    if p & 0b001 != 0 {x1} else {x2})
        }

        let user = f(user, if special & 0b100 != 0 {'s'} else {'x'},
                     if special & 0b100 != 0 {'S'} else {'-'});
        let group = f(group, if special & 0b010 != 0 {'g'} else {'x'},
                      if special & 0b010 != 0 {'G'} else {'-'});
        let other = f(other, if special & 0b010 != 0 {'t'} else {'x'},
                      if special & 0b010 != 0 {'T'} else {'-'});
        format!("{user}{group}{other}")
    }
}

impl core::fmt::Debug for TypePerms {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let (special, user, group, other) = self.perms();
        let perms = format!("{special}{user}{group}{other}");

        f.debug_struct("TypePerms")
            .field("type", &self.typ())
            .field("perms", &perms)
            .finish()
    }
}

impl core::fmt::Display for TypePerms {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let tpchar = match self.typ() {
            Type::Fifo => "p",
            Type::CharDev => "c",
            Type::Dir => "d",
            Type::BlockDev => "b",
            Type::File => ".",
            Type::Symlink => "l",
            Type::Socket => "s",
        };
        let perms = self.format_perms();
        write!(f, "{tpchar}{perms}")
    }
}

#[allow(unused)]
#[derive(Debug, Clone)]
struct DirEnt {
    inode: usize,
    typ: Type,
    name: String,
}

macro_rules! read_direct_block {
    ($block:expr, $dev:expr, $read:expr, $buf:expr, $self:expr, $block_off:expr, $len:expr) => {{
        let start_addr = $block * $self.superblock.block_size() + $block_off;
        let len = (($len - *$read) as usize).min($self.superblock.block_size());

        let data = &mut $buf[*$read .. *$read + len];
        $dev.read_at(data, start_addr)?;
        *$read += len;
    }};
}

macro_rules! read_indirect_block {
    ($read_blocks:expr, $self:expr, $block:expr, $off_block:expr, $block_off:expr, $read:expr, $buf:expr, $len:expr) => {{
        let mut ptrs = [0u32; 256];
        let ptrs_bytes = unsafe{core::mem::transmute::<&mut [u32; 256], &mut [u8; 1024]>(&mut ptrs)};
        $self.dev.read_at(ptrs_bytes, $block as usize * $self.superblock.block_size())?;

        for a in 0 .. 256 {
            let ptr = ptrs[a];
            if ptr == 0 {
                continue;
            }

            $read_blocks += 1;
            if $off_block <= $read_blocks {
                read_direct_block!(ptr as usize, &mut $self.dev, $read, $buf, $self, $block_off, $len);
                if *$read >= $len {
                    return Ok(*$read);
                }
            }
        }
    }};
}

macro_rules! read_double_indirect_block {
    ($read_blocks:expr, $self:expr, $block:expr, $off_block:expr, $block_off:expr, $read:expr, $buf:expr, $len:expr) => {{
        let mut ptrs = [0u32; 256];
        let ptrs_bytes = unsafe{core::mem::transmute::<&mut [u32; 256], &mut [u8; 1024]>(&mut ptrs)};
        $self.dev.read_at(ptrs_bytes, $block as usize * $self.superblock.block_size())?;

        for a in 0 .. 256 {
            let ptr = ptrs[a];
            if ptr == 0 {
                continue;
            }

            read_indirect_block!($read_blocks, $self, ptr, $off_block, $block_off, $read, $buf, $len);
            if *$read >= $len {
                return Ok(*$read);
            }
        }       
    }};
}

impl Ext2Driver {
    fn readdir_inode(&mut self, inode: usize) -> ErrnoOr<Vec<DirEnt>> {
        let inode = self.get_inode(inode)?;
        if inode.type_perm.typ() != Type::Dir {
            println!("inode {:?} is not a directory", inode);
            return Err(Errno::ENOTDIR);
        }

        assert!(self.superblock.required_features & 0x0002 != 0); // dirs have type
        let mut inode_buf = vec![0u8; inode.size(&self.superblock)];
        self.read_inode(&mut inode_buf, 0, &inode)?;

        let mut ents = vec![];
        let mut idx = 0;
        while idx < inode_buf.len() {
            let mut cidx = idx;
            let inode = u32::from_le_bytes(inode_buf[cidx .. cidx+4].try_into().unwrap());
            cidx += 4;
            let size = u16::from_le_bytes(inode_buf[cidx .. cidx+2].try_into().unwrap());
            cidx += 2;
            let name_len = inode_buf[cidx];
            cidx += 1;
            let typ = match inode_buf[cidx] {
                1 => Some(Type::File),
                2 => Some(Type::Dir),
                3 => Some(Type::CharDev),
                4 => Some(Type::BlockDev),
                5 => Some(Type::Fifo),
                6 => Some(Type::Socket),
                7 => Some(Type::Symlink),
                _ => None,
            };
            cidx += 1;
            let name = core::str::from_utf8(&inode_buf[cidx .. cidx + name_len as usize]).unwrap();
            idx += size as usize;

            if size == 0 {
                break;
            } else if inode == 0 {
                continue;
            }

            if let Some(typ) = typ {
                ents.push(DirEnt {
                    inode: inode as usize,
                    typ,
                    name: name.to_string(),
                });
            }
        }

        // println!("INODE: {:?}", inode_buf);
        // println!("mut perms: {}", inode.type_perm);

        Ok(ents)
    }

    fn read_inode(&mut self, buf: &mut [u8], offset: usize, inode: &Inode) -> ErrnoOr<usize> {
        let len = (inode.size(&self.superblock) - offset).min(buf.len());

//        let end_addr = (offset + buf.len()).min(len);
        let mut read = 0;

        let block_off = offset % self.superblock.block_size();
        let off_block = offset / self.superblock.block_size();

        let mut read_blocks = 0;
        for a in 0 .. 12 {
            if inode.direct_ptrs[a] == 0 {
                continue;
            }
            read_blocks += 1;
            if off_block <= read_blocks {
                read_direct_block!(inode.direct_ptrs[a] as usize, &mut self.dev, &mut read, buf, self, block_off, len);
                if read >= len {
                    return Ok(read);
                }
            }
        }

        if inode.single_indirect != 0 {
            read_indirect_block!(read_blocks, self, inode.single_indirect, off_block, block_off, &mut read, buf, len);
            if read >= len {
                return Ok(read);
            }
        }

        if inode.double_indirect != 0 {
            read_double_indirect_block!(read_blocks, self, inode.double_indirect, off_block, block_off, &mut read, buf, len);
            if read >= len {
                return Ok(read);
            }
       }

        if inode.triple_indirect != 0 {
            let mut ptrs = [0u32; 256];
            let ptrs_bytes = unsafe{core::mem::transmute::<&mut [u32; 256], &mut [u8; 1024]>(&mut ptrs)};
            self.dev.read_at(ptrs_bytes, inode.triple_indirect as usize * self.superblock.block_size())?;

            for a in 0 .. 256 {
                let ptr = ptrs[a];
                if ptr == 0 {
                    continue;
                }

                read_double_indirect_block!(read_blocks, self, ptr, off_block, block_off, &mut read, buf, len);
                if read >= len {
                    return Ok(read);
                }
            }       
        }

        assert!(read >= len, "TODO: 4x indirect!?");

//
//        for block_addr in (offset .. end_addr).step_by(self.superblock.block_size()) {
//            let block = block_addr / self.superblock.block_size();
//            let end_addr = end_addr.min(len);
//
//            let mut read_indirect = |off, idx, dev: &mut FileDescriptor, read: &mut usize| {
//                let mut ptr = 0u32;
//                unsafe{dev.read_at(core::slice::from_raw_parts_mut(&mut ptr as *mut _ as *mut u8, 4), off + idx * 4)}?;
//
//                let off = ptr as usize * self.superblock.block_size() + block_off;
//                read_direct(off, dev, read)?;
//                Ok(())
//            };
//
//            match block {
//                x@(0 ..= 11) => {
//                    let off = inode.direct_ptrs[x] as usize * self.superblock.block_size() + block_off;
//                    read_direct(off, &mut self.dev, &mut read)?;
//                },
//                x@(12 ..= 267) => {
//                    let off = inode.single_indirect as usize * self.superblock.block_size() + block_off;
//                    let idx = x - 12;
//                    read_indirect(off, idx, &mut self.dev, &mut read)?;
//                },
//                x@(268 ..= 65803) => {
//                    let off = inode.double_indirect as usize * self.superblock.block_size() + block_off;
//                    let idx = (x - 268) / 256;
//                    let idx2 = (x - 268) % 256;
//                    let mut ptr = 0u32;
//                    unsafe{self.dev.read_at(core::slice::from_raw_parts_mut(&mut ptr as *mut _ as *mut u8, 4), off + idx * 4)}?;
//
//                    let off = ptr as usize * self.superblock.block_size() + block_off;
//                    read_indirect(off, idx2, &mut self.dev, &mut read)?;
//                },
//                a => todo!("block: {a}, read: {read:#x}"),
//            }
//            block_off = 0;
//        }

        Ok(read)
    }

    fn get_inode(&mut self, inode: usize) -> ErrnoOr<Inode> {
        let block_group = (inode - 1) / self.superblock.inodes_per_group as usize;
        let index = (inode - 1) % self.superblock.inodes_per_group as usize;
        let inode_table = self.block_groups[block_group].inode_table as usize;

        let inode_addr = inode_table * self.superblock.block_size() + index * self.superblock.inode_size as usize;

        let mut inode = vec![0; self.superblock.inode_size as usize];
        self.dev.read_at(&mut inode, inode_addr)?;

        let inode = unsafe {*(inode.as_ptr() as *const Inode)};
        Ok(inode)
    }
}

#[derive(Debug, Clone)]
#[allow(unused)]
struct Ext2Fd {
    inode: usize,
    fs: Arc<Lock<Ext2Driver>>,
    seek: usize,
}

impl Fd for Ext2Fd {
    fn read(&mut self, buf: &mut [u8]) -> ErrnoOr<usize> {
        let v = self.read_at(buf, self.seek)?;
        self.seek += v;
        Ok(v)
    }

    fn read_at(&mut self, buf: &mut [u8], offset: usize) -> ErrnoOr<usize> {
        let inode = self.fs.write().get_inode(self.inode)?;
        let res = self.fs.write().read_inode(buf, offset, &inode);
        writeln!(DebugCon, "requested: {}, got: {:?} // at offset {}", buf.len(), res, offset).unwrap();
        res
    }

    unsafe fn close(&mut self) {
        // self.fs.write().close(self.inode);
    }

    fn len(&self) -> usize {
        let inode = self.fs.write().get_inode(self.inode).unwrap();
        inode.size(&self.fs.read().superblock)
    }

    fn get_type(&self) -> super::fs::FileType {
        self.fs.write().get_inode(self.inode).unwrap().type_perm.typ().to_file_type()
    }

    fn write(&mut self, _buf: &[u8]) -> ErrnoOr<usize> {
        todo!()
    }

    fn clonefd(&self) -> Box<dyn Fd> {
        Box::new(self.clone())
    }
}

#[derive(Debug)]
struct Ext2DirEnts(Vec<DirEnt>);

impl DirEntries for Ext2DirEnts {
    fn add(&mut self, _item: DirEntry) {
        todo!()
    }

    fn iter(&self) -> Box<dyn Iterator<Item = DirEntry>> {
        Box::new(self.0
                 .clone()
                 .into_iter()
                .map(|x| (x.name, x.typ.to_file_type())))
    }
}

#[derive(Debug, Clone)]
struct Ext2DirFd {
    abs_path: CPathS,
    inode: usize,
    fs: Arc<Lock<Ext2Driver>>
}

impl DirFd for Ext2DirFd {
    fn read_dir_contents(&self) -> ErrnoOr<Box<dyn super::fs::DirEntries>> {
        let ents = self.fs.write().readdir_inode(self.inode)?;
        Ok(Box::new(Ext2DirEnts(ents)))
    }

    fn get_absolute_path(&self) -> CPathS {
        self.abs_path.clone()
    }

    fn clonefd(&self) -> Box<dyn DirFd> {
        Box::new(self.clone())
    }

    fn close(&mut self) {
        // TODO: close file?
    }
}

impl MountDriverT for Arc<Lock<Ext2Driver>> {
    fn traverse_path_open(&self, mut mount_rel_path: super::fs::CPathS, _mount_path: Path, file: &str, _flags: usize, _mode: usize) -> super::fs::TraverseResult {
        let mut inode = 2; // root inode
        for p in mount_rel_path.real() {
            let files = self.write().readdir_inode(inode)?;
            let ent = files.iter().find(|x| x.name == p)
                .ok_or(Errno::ENOENT)?;
            inode = ent.inode
        }

        let dirents = self.write().readdir_inode(inode)?;
        let ent = dirents.iter().find(|x| x.name == file);
        if let Some(ent) = ent {
            match ent.typ {
                Type::Fifo
                    | Type::CharDev
                    | Type::BlockDev
                    | Type::Socket
                    | Type::File => {
                        Ok(Traverse::File(Box::new(Ext2Fd {inode: ent.inode, fs: self.clone(), seek: 0})))
                    },
                Type::Symlink => todo!(),
                Type::Dir => {
                    mount_rel_path.data.push(CPath::singleton(file.to_string()));
                    Ok(Traverse::Dir(mount_rel_path.clone(), Box::new(Ext2DirFd {
                        abs_path: mount_rel_path,
                        fs: self.clone(),
                        inode: ent.inode,
                    })))
                },
            }
        } else {
            Err(Errno::ENOENT)
        }
    }

    fn umount(&mut self)  -> ErrnoOr<()> {
        unsafe {self.write().dev.close()};
        Ok(())
    }
}

fn create_ext2_driver(dev: Path, mntpath: Path) -> ErrnoOr<Ext2Driver> {
    let mut file = open(&format_path(&dev), O_RDONLY, 0)?;
    let mut buf = [0; 1024];
    file.read_at(&mut buf, 1024)?;

    let superblock = unsafe{*(buf.as_ptr() as *const Superblock)};
    if superblock.major_version < 1 {
        writeln!(DebugCon, "ext2 major number <= 0").unwrap();
        return Err(Errno::EINVAL);
    }

    let bgroups1 = div_ceil!(superblock.blocks, superblock.blocks_per_group);
    let bgroups2 = div_ceil!(superblock.blocks, superblock.blocks_per_group);
    if bgroups1 != bgroups2 {
        writeln!(DebugCon, "bgroups1 != bgroups2").unwrap();
        return Err(Errno::EINVAL);
    }
    let bs = superblock.block_size();
    let bgdt_offset = (1024 + bs) / bs * bs;
    let mut bgdt = vec![0; bs];
    file.read_at(&mut bgdt, bgdt_offset)?;

    let bgds = unsafe {core::slice::from_raw_parts(bgdt.as_mut_ptr() as *mut BlockGroupDescriptor, bgroups1 as usize)}.to_vec();

    let drv = Ext2Driver {
        dev: file,
        mnt: mntpath,
        superblock,
        block_groups: bgds,
    };

    // drv.readdir_inode(2)?;

    Ok(drv)
}

pub fn ext2_init() {
    VFS.register_mnt_driver("ext2", MountDriver {
        create: |s, p| Ok(Box::new(Arc::new(Lock::new(create_ext2_driver(s, p)?)))),
    });
}
