use crate::io::ports::IOPort;
use crate::utils::lock::Lock;
use crate::cpu::interrupts::{PIC, PICInterruptIndex, Registers};

type KeyboardHandler = fn(scancode: Scancode);

static KBD_HANDLER: Lock<Option<KeyboardHandler>> = Lock::new(None);

pub fn set_keyboard_handler(handler: KeyboardHandler) {
    *KBD_HANDLER.write() = Some(handler);
}

#[derive(Debug)]
pub struct Scancode {
    pub code: u8,
    pub released: bool
}

pub fn keyboard_interrupt(_regs: &mut Registers) {
    let scancode = unsafe {u8::pin(0x60)};
    let sc = if scancode & 128 != 0 {
        Scancode {
            code: scancode & 0b01111111,
            released: true
        }
    } else {
        Scancode {
            code: scancode,
            released: false
        }
    };

    if let Some(h) = *KBD_HANDLER.read() {
        h(sc);
    }
    
    PIC.write().end_of_interrupt(PICInterruptIndex::Keyboard as usize + 32);
}
