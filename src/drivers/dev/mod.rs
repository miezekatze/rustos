use alloc::{boxed::Box, collections::BTreeMap};
use crate::utils::{lock::Lock, errno::{Errno, ErrnoOr}};

use super::fs::Fd;

pub struct DeviceDriver {
    pub open_device: fn(min: usize) -> ErrnoOr<Box<dyn Fd>>,
}
pub const BLOCK_SIZE: usize = 512;

static DRIVERS: Lock<BTreeMap<usize, DeviceDriver>> = Lock::new(BTreeMap::new());

pub fn dev_open(maj: usize, min: usize) -> ErrnoOr<Box<dyn Fd>> {
    let guard = DRIVERS.read();
    let dev = guard.get(&maj).ok_or(Errno::ENXIO)?;
    (dev.open_device)(min)
}

pub fn register_device_driver(maj: usize, drv: DeviceDriver) {
    DRIVERS.write().insert(maj, drv);
}

pub fn blockdev(drv: Box<dyn BlockFd>) -> Box<dyn Fd> {
    Box::new(BlockDevFd {
        seek: 0,
        cache: BTreeMap::new(),
        drv,
    })
}

#[derive(Debug)]
pub struct BlockDevFd {
    pub seek: usize,
    pub cache: BTreeMap<usize, [u8; BLOCK_SIZE]>,
    pub drv: Box<dyn BlockFd>
}

#[inline(always)]
fn start_end(index: usize, len: usize) -> (usize, usize) {
    let start = index*BLOCK_SIZE;

    let nstart = (index+1) * BLOCK_SIZE;
    let end = nstart.min(len);

    let shift_end = end - start;
    let overflow = if shift_end > BLOCK_SIZE {
        shift_end - BLOCK_SIZE
    } else {
        0
    };
    
    (start, end - overflow)
}

impl Fd for BlockDevFd {
    fn read_at(&mut self, buf: &mut [u8], pos: usize) -> ErrnoOr<usize> {
        let off = pos;
        let mut sector = off / BLOCK_SIZE;
        let start_sect = sector;
        let mut sect_off = off % BLOCK_SIZE;

        let mut read = 0;

        let v = loop {
            if read >= buf.len() {
                break Ok(read);
            }
            if let Some(b) = self.cache.get(&sector) {
                if sect_off != 0 || buf.len() - read < BLOCK_SIZE {
                    let new_read = (if sect_off != 0 {BLOCK_SIZE - sect_off} else {buf.len() - read}).min(buf.len() - read);
                    buf[read .. read + new_read]
                        .copy_from_slice(&b[sect_off .. sect_off + new_read]);
                    read += new_read;
                    sect_off = 0;
                } else {
                    let new_read = BLOCK_SIZE;
                    buf[read .. read + new_read]
                        .copy_from_slice(b);
                    read += new_read;
                }
                sector += 1;
            } else {
                let mut next_found = sector + 1;
                while (next_found - start_sect) * BLOCK_SIZE < buf.len() && self.cache.get(&next_found).is_none() {
                    next_found += 1;
                }

/*                if next_found * BLOCK_SIZE > buf.len() {
                    next_found = buf.len() / BLOCK_SIZE + 1;
                }*/

                let mut count = next_found - sector;
//                println!("read: {}, count: {}, len: {}", read, count, buf.len());
                if next_found * BLOCK_SIZE > buf.len() && count > 1 {
                    next_found -= 1;
                    count -= 1;
                }

                if sect_off != 0 || buf.len() - read < BLOCK_SIZE {
//                    assert_eq!(read, 0);
                    // create buffer
                    let mut b = [0u8; BLOCK_SIZE];
//                    assert_eq!(count, 1);

                    let len = if sect_off != 0 {sect_off} else {BLOCK_SIZE - (buf.len() - read)};

                    self.drv.read_sector_uncached(sector, 1, &mut b)?;
                    let new_read = (BLOCK_SIZE - len).min(buf.len() - read);

                    buf[read .. read + new_read]
                        .copy_from_slice(&b[sect_off .. new_read + sect_off]);
                    // add to cache
                    self.cache.insert(sector, b);

                    read += new_read;
                    sector += 1;
                    count -= 1;

                    if count == 0 {
                        sector = next_found;
                        sect_off = 0;
                        continue;
                    }
                }

                self.drv.read_sector_uncached(sector, count as u16, &mut buf[read .. ])?;
//                let cnt_diff = count - data.len();
//                next_found -= cnt_diff;
//
                for _ in 0..count {
                    let index = read / BLOCK_SIZE + start_sect;
                    let mut b = [0u8; BLOCK_SIZE];

                    // copy from buf to b
//                    println!("read: {}, count: {}", read, count);
                    b.copy_from_slice(&buf[read .. read + BLOCK_SIZE]);
                    self.cache.insert(index, b);

                    let (start, end) = start_end(index - start_sect, buf.len());
                    read += end - start;
                }
//                
                sector = next_found;
                sect_off = 0;
            }
       };
        assert_eq!(read, buf.len());
        v
    }

    fn read(&mut self, buf: &mut [u8]) -> ErrnoOr<usize> {
        let len = self.read_at(buf, self.seek)?;
        self.seek += len;
        Ok(len)
    }

    unsafe fn close(&mut self) { }

    fn len(&self) -> usize {
        self.drv.len()
    }

    fn get_type(&self) -> super::fs::FileType {
        self.drv.get_type()
    }

    fn write(&mut self, _buf: &[u8]) -> ErrnoOr<usize> {
        todo!()
    }

    fn clonefd(&self) -> Box<dyn Fd> {
        Box::new(Self {
            seek: self.seek,
            cache: self.cache.clone(),
            drv: self.drv.clonefd()
        })
    }
}

#[allow(clippy::len_without_is_empty)]
pub trait BlockFd: core::fmt::Debug {
    fn read_sector_uncached(&mut self, start_sector: usize, count: u16, buf: &mut [u8]) -> ErrnoOr<()>;
    fn len(&self) -> usize;
    fn get_type(&self) -> super::fs::FileType;
    fn clonefd(&self) -> Box<dyn BlockFd>;
}
