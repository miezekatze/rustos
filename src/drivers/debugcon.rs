use crate::io::ports::IOPort;

pub struct DebugCon;
impl core::fmt::Write for DebugCon {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for b in s.bytes() {
            unsafe {b.pout(0xe9)};
        }
        Ok(())
    }
}

