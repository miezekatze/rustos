use core::arch::asm;

use alloc::vec;

use crate::{println, drivers::fs::{open, FileType}, utils::errno::{ErrnoOr, Errno},
    user::{USERMODE_CODE_START, USERMODE_STACK_START, USERMODE_STACK_SIZE},
    mem::{paging::{PAGE_SIZE, Page}, self, Virtual}, task::{self, openfd}};

use super::fs::{O_RDONLY, O_RDWR, Fd};

#[allow(unused)]
#[derive(Debug)]
#[repr(C)]
struct Elf64Ehdr {
    e_ident: [u8; 16],
    e_type: u16,
    e_machine: u16,
    e_version: u32,
    e_entry: u64,
    e_phoff: u64,
    e_shoff: u64,
    e_flags: u32,
    e_ehsize: u16,
    e_phentsize: u16,
    e_phnum: u16,
    e_shentsize: u16,
    e_shnum: u16,
    e_shstrndx: u16,
}

#[derive(Debug)]
#[allow(unused)]
#[repr(C)]
struct Elf64Phdr {
    p_type: u32,
    p_flags: u32,
    p_offset: u64,
    p_vaddr: u64,
    p_paddr: u64,
    p_filesz: u64,
    p_memsz: u64,
    p_align: u64,
}

const EI_CLASS: usize = 4;
const EI_DATA: usize = 5;
const EI_VERSION: usize = 6;

const ELFCLASS64: u8 = 2;
const ELFDATA2LSB: u8 = 1;
const EV_CURRENT: u8 = 1;

const ET_EXEC: u16 = 2;
const EM_X86_64: u16 = 62;

const PT_LOAD: u32 = 1;
const PT_NOTE: u32 = 4;
const PT_GNU_PROPERTY: u32 = 0x6474e553;
const PT_GNU_STACK: u32 = 0x6474e551;

const PF_X: u32 = 1;
const PF_W: u32 = 2;
const PF_R: u32 = 4;

#[inline(never)]
pub fn elf_load(path: &str, args: &[&str]) -> ErrnoOr<(usize, usize)> {
    let mut fd = open(path, O_RDONLY, 0)?;
    let tp = fd.get_type();

    if tp != FileType::File {
        return Err(Errno::EACCES);
    }

    let len = fd.len();
    let mut buf = vec![0u8; len];
    fd.read(&mut buf)?;

    let ehdr = unsafe { &*(buf.as_ptr() as *const Elf64Ehdr) };
    if &ehdr.e_ident[0..4] != b"\x7fELF" {
        return Err(Errno::ENOEXEC);
    }

    if ehdr.e_ident[EI_CLASS] != ELFCLASS64 {
        return Err(Errno::ENOEXEC);
    }

    if ehdr.e_ident[EI_DATA] != ELFDATA2LSB {
        return Err(Errno::ENOEXEC);
    }

    if ehdr.e_ident[EI_VERSION] != EV_CURRENT {
        return Err(Errno::ENOEXEC);
    }

    if ehdr.e_type != ET_EXEC {
        return Err(Errno::ENOEXEC);
    }

    if ehdr.e_machine != EM_X86_64 {
        return Err(Errno::ENOEXEC);
    }

    if ehdr.e_version != EV_CURRENT as u32 {
        return Err(Errno::ENOEXEC);
    }

    // chcek ELF entry point
    if ehdr.e_entry == 0 {
        return Err(Errno::ENOEXEC);
    }

    // check ELF program header
    if ehdr.e_phoff == 0 || ehdr.e_phnum == 0 || ehdr.e_phentsize == 0 {
        return Err(Errno::ENOEXEC);
    }
    unsafe{asm!("cli")};

    // clear proc memory areas
    task::clear_mmap_areas()?;

    // load program segments
    let phdr: &[Elf64Phdr] = unsafe { core::slice::from_raw_parts(buf.as_ptr().add(ehdr.e_phoff as usize) as *const _,
        ehdr.e_phnum as usize) };
    for phdr in phdr {
        match phdr.p_type {
            PT_LOAD => {
                let addr = phdr.p_vaddr as usize;

                if addr < USERMODE_CODE_START as usize {
                    println!("address too low: {:#x}", addr);
                    return Err(Errno::ENOEXEC);
                }

                let size = phdr.p_memsz as usize;
                let offset = phdr.p_offset as usize;
                let fsize = phdr.p_filesz as usize;

                // allocate pages
                let mut pt = mem::paging::ActivePageTable::get();
                for a in (addr .. addr + size).step_by(PAGE_SIZE) {
                    if pt.translate(Virtual(a)).is_some() {
                        pt.unmap(Page::from_address(Virtual(a)), true);
                    }

                    mem::paging::alloc_page(Virtual(a), true, true);
                    // zero page
                    unsafe { core::ptr::write_bytes(a as *mut u8, 0, PAGE_SIZE) };
                }

                // copy data
                unsafe { core::ptr::copy_nonoverlapping(buf.as_ptr().add(offset), addr as *mut u8, fsize) };

                let mut flags = mem::paging::PTEntry::PRESENT | mem::paging::PTEntry::USER_ACCESSIBLE;
                if phdr.p_flags & PF_X == 0 {
                    flags |= mem::paging::PTEntry::NO_EXECUTE;
                }
                if phdr.p_flags & PF_W != 0 {
                    flags |= mem::paging::PTEntry::WRITABLE;
                }

                // remap pages
                for a in (addr .. addr + size).step_by(PAGE_SIZE) {
                    let adr = Virtual(a);
                    let pg = Page::from_address(adr);
                    let frame = pt.translate(adr).unwrap();
                    pt.unmap(pg, false);

                    pt.map_to_addr(pg, frame, flags, false, false);
                }

                let mut prot = 0;
                if phdr.p_flags & PF_X != 0 {
                    prot |= task::PROT_EXEC;
                }
                if phdr.p_flags & PF_W != 0 {
                    prot |= task::PROT_WRITE;
                }
                if phdr.p_flags & PF_R != 0 {
                    prot |= task::PROT_READ;
                }
                task::add_mmap_area(addr, addr+size, prot, false, None)?;
            },
            PT_NOTE 
            | PT_GNU_PROPERTY
            | PT_GNU_STACK => {},
            a => {
                println!("TODO: {a:#x}");
                return Err(Errno::ENOSYS);
            }
        }
    }

    let stack_addr = USERMODE_STACK_START as usize;
    let stack_size = USERMODE_STACK_SIZE as usize;

    let mut tbl = mem::paging::ActivePageTable::get();
    // allocate stack
    for (i, a) in (stack_addr - stack_size .. stack_addr).step_by(PAGE_SIZE).enumerate() {
//        println!("unmapping page {:#x}", a);
        tbl.unmap(Page::from_address(Virtual(a)), false);
        unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};
//        println!("allocating page {:#x}", a);
        mem::paging::alloc_user_page(Virtual(a), true, true);
        // zero page
        unsafe { core::ptr::write_bytes(a as *mut u8, 0, PAGE_SIZE) };
        let frame = tbl.translate(Virtual(a)).unwrap();

        task::set_ustack_frame(i, frame.0 as u64);
    }

    // allocated all pages, flush
    unsafe{asm!("mov {r}, cr3; mov cr3, {r}", r = out(reg) _)};

    let mut rsp = stack_addr - stack_size / 2;
    let mut ptrs = vec![0; args.len()];

    // push arguments
    for (i, arg) in args.iter().enumerate() {
        rsp -= arg.len();
        rsp &= !0xf;
        unsafe { core::ptr::copy_nonoverlapping(arg.as_ptr(), rsp as *mut u8, arg.len()) };
        ptrs[i] = rsp;
    }

    // push pointers
    rsp -= ptrs.len() * core::mem::size_of::<usize>();
    unsafe { core::ptr::copy_nonoverlapping(ptrs.as_ptr(), rsp as *mut usize, ptrs.len()) };

    // push argc
    rsp -= core::mem::size_of::<usize>();
    unsafe { core::ptr::write(rsp as *mut usize, args.len()) };

    // open fds
    openfd("/dev/tty0", O_RDWR, 0)?;
    openfd("/dev/tty0", O_RDWR, 0)?;
    openfd("/dev/tty0", O_RDWR, 0)?;

    Ok((ehdr.e_entry as usize, rsp))
}
