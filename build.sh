#!/bin/sh

cd "$(dirname $0)"

if [[ ! -f img ]]; then
    echo "Creating image..."
    set -xe
    dd if=/dev/zero of=img bs=1024 count=1048576
    parted img mklabel gpt
    # 1MB for the bootloader
    parted img mkpart primary 1MB 2MB
    parted img type 1 21686148-6449-6E6F-744E-656564454649
    # 1GB for the OS
    parted img mkpart primary 2MB 1026MB
    sudo losetup --partscan /dev/loop0 img || (echo "/dev/loop0 not free" && exit 1)
    sudo mkfs.ext2 /dev/loop0p2
    mkdir --parents mnt
    sudo mount /dev/loop0p2 mnt || exit 1
    sudo rmdir mnt/lost+found
    sudo chown -R $USER mnt

    cp -R root/* mnt || exit 1
    sudo grub-install --boot-directory=./mnt/boot/ --target=i386-pc /dev/loop0

    sudo umount mnt

    set +xe
fi

if [[ ! -f font.o ]]; then
    echo "Compiling font..."
    objcopy -O elf64-x86-64 -B i386 -I binary font.psfu font.o
fi

export RUST_TARGET_PATH=$(pwd)

OUTPUT=root/boot/kernel.elf

if [[ $# -lt 1 ]]; then
    echo "Usage: $0 <debug|release> [cargo args]"
    exit 1
fi

WAS_MOUNTED=0

if ! mount | grep rust/os2/mnt > /dev/null; then
    echo "Mounting..."
    sudo ./mount || exit 1
    WAS_MOUNTED=1
fi

MODE=$1
shift

nasm -felf64 loader.asm || exit 1

if [[ $MODE == "debug" ]]; then
    cargo build --target x86_64-os2 $@ || exit 1
    ld -n -T linker.ld loader.o target/x86_64-os2/debug/libos2.a font.o -o $OUTPUT || exit 1
    cp $OUTPUT kernel.elf || exit 1
    strip $OUTPUT
elif [[ $MODE == "release" ]]; then
    cargo build -Z build-std-features=compiler-builtins-mem --target x86_64-os2 --release $@ || exit 1
    ld -n -T linker.ld loader.o target/x86_64-os2/release/libos2.a font.o -o $OUTPUT || exit 1
    cp $OUTPUT kernel.elf || exit 1
    strip $OUTPUT
fi

(cd userland &&
    (make || exit 1) &&
    cd ..) || exit 1

mkdir --parents mnt
sudo cp -R root/* mnt || exit 1

if [[ $WAS_MOUNTED == 1 ]]; then
    echo "Unmounting..."
    sudo ./umount || exit 1
fi
