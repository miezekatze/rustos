#ifndef _STRING_H_
#define _STRING_H_

#include "./stddef.h"

size_t strlen(const char *s);

// copies from one memory are to another
void *memcpy(void *restrict dest, const void *restrict src, size_t n);

// sets n bytes at address s to c
void *memset(void *s, int c, size_t n);

// duplicates a string
char *strdup(const char *s);

// duplicates a string at most n bytes
char *strndup(const char *s, size_t n);

// copies a string
char *strcpy(char *restrict dets, const char *src);

// compare two strings
int strncmp(const char *s1, const char *s2, size_t n);

int strcmp(const char *s1, const char *s2);

// concateneate two strings
char *strncat(char *restrict dest, const char *restrict src, size_t n);

char *strcat(char *restrict dest, const char *restrict src);

char *strchr(const char *s, int c);

#ifdef  __GNUC__
#define alloca(size)   __builtin_alloca (size)
#endif

#endif //_STRING_H_
