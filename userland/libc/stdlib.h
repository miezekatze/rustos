#ifndef _STDLIB_H_
#define _STDLIB_H_

#include <stddef.h>

#define EXIT_FAILURE (-1)
#define EXIT_SUCCESS (0)
__attribute__((noreturn)) void exit(int status);

// allocates dynamic memory
void *malloc(size_t size);

// frees dynamic memory
void free(void *ptr);

// allocates and clears dynamic memory
void *calloc(size_t nmemb, size_t size);

// changes the size of a dynamic memory allocation
void *realloc(void *ptr, size_t size);

// convert a string to integer
int atoi(const char *nptr);

// abort execution
__attribute__((noreturn)) void abort(void);

char *getenv(const char *name);

int abs(int j);

#endif // _STDLIB_H_
