#ifndef _SYS_STAT_H
#define _SYS_STAT_H

#include <stddef.h>
#include <stdint.h>

struct stat {
    size_t st_dev;
    size_t st_ino;
    size_t st_nlink;

    uint32_t st_mode;
    uint32_t st_uid;
    uint32_t st_gid;
    uint32_t __pad0;

    size_t st_rdev;
    ssize_t st_size;
    ssize_t st_blksize;
    ssize_t st_blocks;

    size_t st_atime;
    size_t st_atime_nsec;
    size_t st_mtime;
    size_t st_mtime_nsec;
    size_t st_ctime;
    size_t st_ctime_nsec;

    size_t __unused[3];
};

int stat(const char *path, struct stat *buf);
int fstat(int fd, struct stat *buf);
int fstatat(int dirfd, const char *path, struct stat *buf, int flags);

#endif
