#ifndef __SYS_TYPES_H_
#define __SYS_TYPES_H_

#include <assert.h>

typedef unsigned long size_t;
static_assert(sizeof(size_t) == 8, "size_t is 64-bit");

typedef signed long ssize_t;
static_assert(sizeof(ssize_t) == 8, "ssize_t is 64-bit");

typedef ssize_t off_t;
typedef long pid_t;

#endif // __SYS_TYPES_H_
