#include <sys/types.h>

struct rusage;

pid_t wait4(pid_t pid, int *wstatus, int options,
                   struct rusage *rusage);
pid_t wait(int *wstatus);
