#include "./syscall.h"
#include "../errno.h"
#include "../stdarg.h"
#include <stdio.h>

long syscall(long number, ...) {
    int args;
    switch (number) {
        case SYS_fork:
            args = 0;
            break;
        case SYS_exit:
        case SYS_render_buf:
            args = 1;
            break;
        case SYS_getcwd:
        case SYS_munmap:
        case SYS_nanosleep:
            args = 2;
            break;
        case SYS_write:
        case SYS_read:
        case SYS_execve:
        case SYS_open:
        case SYS_getdents:
            args = 3;
            break;
        case SYS_wait4:
        case SYS_fstatat:
            args = 4;
            break;
        case SYS_mmap:
            args = 6;
            break;
        default:
            printf("syscall %d not implemented yet.\n", (int)number);
            errno = ENOSYS;
            return -1;
    }

    long sargv[6] = {0};

    // get args
    va_list ap;
    va_start(ap, number);
    for (int i = 0; i < args; i++) {
        long arg = va_arg(ap, long);
        sargv[i] = arg;
    }
    va_end(ap);

    // syscall
    long ret;
    register long r10 __asm__("r10") = sargv[3];
    register long r8 __asm__("r8") = sargv[4];
    register long r9 __asm__("r9") = sargv[5];
    asm volatile("syscall"
                 : "=a"(ret)
                 : "a"(number), "D"(sargv[0]), "S"(sargv[1]), "d"(sargv[2]),
                    "r"(r10), "r"(r8), "r"(r9));

    if (ret < 0) {
        errno = -ret;
        return -1;
    }

    return ret;
}
