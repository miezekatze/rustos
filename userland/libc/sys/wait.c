#include "wait.h"
#include "syscall.h"
#include <assert.h>
#include <errno.h>

pid_t wait4(pid_t pid, int *wstatus, int options,
                   struct rusage *rusage) {
    return syscall(SYS_wait4, pid, wstatus, options, rusage);
}

pid_t wait(int *wstatus) {
    assert(wstatus == NULL && "not implemented");
    return wait4(-1, NULL, 0, NULL);
}
