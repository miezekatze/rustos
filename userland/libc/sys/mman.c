#include <sys/mman.h>
#include <sys/syscall.h>
#include <errno.h>
#include <stdio.h>

void *mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset) {
    long ret = syscall(SYS_mmap, addr, len, (long)prot, (long)flags, (long)fd, (long)offset);
    if (ret < 0) {
        return MAP_FAILED;
    }
    return (void *)ret;
}

int munmap(void *addr, size_t len) {
    return syscall(SYS_munmap, addr, len);
}
