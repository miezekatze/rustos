#include <sys/stat.h>
#include <sys/syscall.h>
#include <fcntl.h>

int fstatat(int dirfd, const char *path, struct stat *buf, int flags) {
    return syscall(SYS_fstatat, dirfd, path, buf, flags);
}

int fstat(int fd, struct stat *buf) {
    return fstatat(fd, "", buf, AT_EMPTY_PATH);
}

int stat(const char *path, struct stat *buf) {
    return fstatat(AT_FDCWD, path, buf, 0);
}
