#ifndef _SYSCALL_USER_H_
#define _SYSCALL_USER_H_

#define __os_syscalls__

#include <stdint.h>
#include <sys/types.h>
#include <stdbool.h>

long syscall(long number, ...);

#define SYS_read 0
#define SYS_write 1
#define SYS_open 2
#define SYS_close 3
#define SYS_lseek 8
#define SYS_mmap 9
#define SYS_munmap 11
#define SYS_nanosleep 35
#define SYS_getpid 39
#define SYS_shutdown 48
#define SYS_fork 57
#define SYS_execve 59
#define SYS_exit 60
#define SYS_wait4 61
#define SYS_getdents 78
#define SYS_getcwd 79
#define SYS_chdir 80
#define SYS_geteuid 107
#define SYS_fstatat 262

#define SYS_pthread_create 1000
#define SYS_render_buf 1001

#endif //_SYSCALL_USER_H_
