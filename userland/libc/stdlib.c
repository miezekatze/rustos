#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

void exit(int status) {
    syscall(SYS_exit, status);
    __builtin_unreachable();
}

// memory allocation
typedef struct __mem_block {
    size_t size;
    int free;
    struct __mem_block *next;
    struct __mem_block *prev;
} __mem_block;

static __mem_block *__mem_block_head = NULL;

// malloc (using mmap)
void *malloc(size_t size) {
    if (size == 0) {
        return NULL;
    }

    __mem_block *block = __mem_block_head;
    while (block != NULL) {
        if (block->free && block->size >= size) {
            block->free = 0;
            return (void *)(block) + sizeof(__mem_block);
        }
        block = block->next;
    }

    // allocate new block
    block = (__mem_block *)mmap(NULL, size + sizeof(__mem_block),
                                PROT_READ | PROT_WRITE,
                                MAP_PRIVATE | MAP_ANONYMOUS,
                                -1, 0);
    if (block == MAP_FAILED) {
        return NULL;
    }

    block->size = size;
    block->free = 0;
    block->next = __mem_block_head;
    block->prev = NULL;
    if (__mem_block_head != NULL) {
        __mem_block_head->prev = block;
    }
    __mem_block_head = block;

    return (void *)(block) + sizeof(__mem_block);
}

// free
void free(void *ptr) {
    if (ptr == NULL) {
        return;
    }

    __mem_block *block = (__mem_block *)ptr - 1;
    block->free = 1;

    // merge adjacent free blocks
    if (block->next != NULL && block->next->free) {
        block->size += block->next->size + sizeof(__mem_block);
        block->next = block->next->next;
        if (block->next != NULL) {
            block->next->prev = block;
        }
    }

    void *free_ptr = (void *)block;
    size_t free_size = block->size + sizeof(__mem_block);

    // remove block from list
    if (block->prev != NULL) {
        block->prev->next = block->next;
    } else {
        __mem_block_head = block->next;
    }

    if (block->next != NULL) {
        block->next->prev = block->prev;
    }

    int ret = munmap(free_ptr, free_size);
    if (ret < 0) {
        printf("free: mnumap: %s\n", strerror(errno));
        return;
    }
}

void *realloc(void *ptr, size_t size) {
    if (ptr == NULL) {
        return malloc(size);
    }

    if (size == 0) {
        free(ptr);
        return NULL;
    }

    __mem_block *block = (__mem_block *)(ptr - sizeof(__mem_block));
    if (block->size >= size) {
        return ptr;
    }

    void *new_ptr = malloc(size);
    if (new_ptr == NULL) {
        return NULL;
    }

    memcpy(new_ptr, ptr, block->size);
    free(ptr);

    return new_ptr;
}

int atoi(const char *nptr) {
    int sign = 1;
    int result = 0;

    while (*nptr == ' ') {
        nptr++;
    }

    if (*nptr == '-') {
        sign = -1;
        nptr++;
    } else if (*nptr == '+') {
        nptr++;
    }

    while (*nptr >= '0' && *nptr <= '9') {
        result = result * 10 + (*nptr - '0');
        nptr++;
    }

    return sign * result;
}

__attribute__((noreturn)) void abort(void) {
    puts("abort(); called");
    puts("this function is stubbed\nTODO: raise(SIGABRT)");
    exit(-1);
}

char *getenv(const char *name) {
    return "__getenv()_not_implemented__";
    (void)name;
}

int abs(int j) {
    return (j >= 0) ? j : (-j);
}
