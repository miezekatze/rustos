#ifndef _STDDEF_H_
#define _STDDEF_H_

#include <sys/types.h>

#define NULL ((void *)0)

#define _WINT_T
typedef unsigned int wint_t;

#endif // _STDDEF_H_
