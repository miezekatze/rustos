#include "stdarg.h"
#include <fcntl.h>
#include <errno.h>
#include <sys/syscall.h>

int open(const char *pathname, int flags, ...) {
    if (flags & O_CREAT) {
        va_list list;
        va_start(list, flags);
        int mode = va_arg(list, int);
        va_end(list);
        return syscall(SYS_open, pathname, flags, mode);
    } else {
        return syscall(SYS_open, pathname, flags, 0);
    }
}

int close(int fd) {
    return syscall(SYS_close, fd);
}
