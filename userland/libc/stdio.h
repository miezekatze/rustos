#ifndef _STDIO_H_
#define _STDIO_H_

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include <sys/types.h>

struct _FILE_STRUCT {
    uint32_t fd;
    uint8_t buf[100];
    size_t buf_pos;
};
typedef struct _FILE_STRUCT FILE;

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

// prints a given string with a newline
int puts(const char *s);

// prints a given string with a newline
int fputs(const char *s, FILE *file);

// prints a format string with a val_list
int vfprintf(FILE *restrict stream, const char *restrict format, va_list args);

// prints a formatted format string to a given file
__attribute__((format(printf, 2, 3))) int
fprintf(FILE *restrict stream, const char *restrict format, ...);

// prints a formatted format string to a given string
__attribute__((format(printf, 2, 3))) int
    sprintf(char *restrict str, const char *restrict format, ...);

// prints a formatted format string to stdout
__attribute__((format(printf, 1, 2))) int printf(const char *restrict format,
                                                 ...);
// reads an entire line from stream
ssize_t getline(char **restrict lineptr, size_t *restrict n,
                FILE *restrict stream);

// opens a new file
FILE *fopen(const char *restrict pathname, const char *restrict mode);

// reads from a FILE *ptr
size_t fread(void *restrict ptr, size_t size, size_t nmemb,
             FILE *restrict stream);

// closes a file
int fclose(FILE *stream);

#define EOF (-1)

int fputc(int c, FILE *stream);
#define putc(c, stream) fputc(c, stream)

// flushes the given stream
int fflush(FILE *stream);

size_t fwrite(const void *restrict ptr, size_t size, size_t nmemb,
              FILE *restrict stream);

long ftell(FILE *stream);

int fseek(FILE *stream, long offset, int whence);

char *gets(char *s);
char *fgets(char *restrict s, int size, FILE *restrict stream);

#endif // _STDIO_H_
