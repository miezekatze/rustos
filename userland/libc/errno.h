#ifndef _ERRNO_H_
#define _ERRNO_H_

#define ESUCC    0
#define ENOENT   2
#define ENOEXEC  8
#define ECHILD  10
#define ENOMEM  12
#define EACCES  13
#define EEXIST  17
#define ENOTDIR 20
#define EINVAL  22
#define EISDIR  21
#define ENOSPC  28
#define ESPIPE  29
#define ENAMETOOLONG 36
#define EDOM    33
#define ERANGE  34
#define ENOSYS  38
#define ENOTSUP 95

#define errno (*__errno_impl())

int *__errno_impl(void);
void perror(const char *prefix);
char *strerror(int err);

#endif //_ERRNO_H_
