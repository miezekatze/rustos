#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/syscall.h>

static FILE stdin_str = {
    .fd = 0,
};

static FILE stdout_str = {
    .fd = 1,
    .buf_pos = 0,
};

static FILE stderr_str = {
    .fd = 2,
};

FILE *stdin  = &stdin_str;
FILE *stdout = &stdout_str;
FILE *stderr = &stderr_str;


int fflush(FILE *stream) {
    int ret = syscall(SYS_write, stream->fd, (const char*)stream->buf, stream->buf_pos);
    stream->buf_pos = 0;
    return ret;
}

static int fprint(FILE *restrict stream, const char *restrict str) {
    int ret = syscall(SYS_write, stream->fd, str, strlen(str));
    return (ret < 0) ? ret : 0;
}

#if 0
static void buf_write(FILE *restrict stream, const char *restrict ptr, const size_t len) {
    /*    if (stream->buf_pos + len > sizeof(stream->buf)) {
        fflush(stream);
    }
    
    memcpy(stream->buf + stream->buf_pos, ptr, len);
    (void)ptr;
    stream->buf_pos += len;*/

    assert(0 && "buf_write Currently not implemented, because of wrong linking (has to be an own ELF file...)");
    (void)stream;
    (void)ptr;
    (void)len;
}
#endif

int puts(const char *s) {
    return fputs(s, stdout);
}

int fputs(const char *s, FILE *file) {
    int ret = fprint(file, s);
    if (ret < 0) return ret;
    ret = fprint(file, "\n");
    return (ret < 0) ? ret : 0;
}

// int vsprintf(FILE *restrict stream, const char *restrict format, va_list
// args) {
#define GEN_PRINTF(__name, __a_tp, __append_str)                        \
    int __name(__a_tp __a, const char *restrict format, va_list args) {      \
        for (size_t pos = 0, last_pos = 0; pos < strlen(format); last_pos = pos) { \
            (void)last_pos;                                             \
            while (format[pos] != '%' && pos < strlen(format)) pos++;   \
            /*        buf_write(stream, format + last_pos, pos - last_pos); */ \
            __a_tp __dest = __a;                                        \
            const char *__ptr = format + last_pos;                      \
            size_t __len = pos - last_pos;                              \
            __append_str;                                               \
            if (pos < strlen(format)) {                                 \
                /* parse argument */                                    \
                char c = format[++pos];                                 \
                if (c == 's') {                                         \
                    const char *string = va_arg(args, const char*);     \
                    __a_tp __dest = __a;                                \
                    const char *__ptr = string;                         \
                    size_t __len = strlen(string);                      \
                    __append_str;                                       \
                    pos += 1;                                           \
                } else if (c == 'd') {                                  \
                    const int num = va_arg(args, const uint32_t);       \
                    uint32_t num2 = num;                                \
                    size_t len = 0;                                     \
                    while (num2 > 0) {                                  \
                        num2 /= 10;                                     \
                        len++;                                          \
                    }                                                   \
                    if (len == 0) len++;                                \
                    char buf[len];                                      \
                    uint32_t rnum = num;                                \
                    for (int buf_pos = len - 1;                         \
                         buf_pos >= 0; rnum /= 10, buf_pos--) {         \
                        buf[buf_pos] = '0' + (rnum % 10);               \
                    }                                                   \
                    __a_tp __dest = __a;                                \
                    char *__ptr = buf;                                  \
                    size_t __len = len;                                 \
                    __append_str;                                       \
                    pos += 1;                                           \
                }                                                       \
            } else {                                                    \
                break;                                                  \
            }                                                           \
        }                                                               \
        va_end(args);                                                   \
        return 0;                                                       \
    }

GEN_PRINTF(vfprintf, FILE *restrict,
           { syscall(SYS_write, __dest->fd, __ptr, __len);});

GEN_PRINTF(vsprintf, char *restrict, {
        //        syscall_write(__dest->fd, __ptr, __len);
        size_t _end = strlen(__dest);
        for (size_t i = 0; i < __len; i++) {
            __dest[_end + i] = __ptr[i];
            __dest[_end + i + 1] = '\0';
        }
    });


int sprintf(char *restrict str, const char *restrict format, ...) {
    va_list args;
    va_start(args, format);
    int res = vsprintf(str, format, args);
    va_end(args);
    return res;
}

int fprintf(FILE *restrict stream, const char *restrict format, ...) {
    va_list args;
    va_start(args, format);
    int res = vfprintf(stream, format, args);
    va_end(args);

    return res;
}

int printf(const char *restrict format, ...) {
    va_list args;
    va_start(args, format);
    int res = vfprintf(stdout, format, args);
    va_end(args);

    return res;
}

ssize_t getline(char **restrict lineptr, size_t *restrict n,
                FILE *restrict stream) {
    if (*lineptr == NULL) {
        assert(0 && "not implemented yet");
        (void)((uintptr_t)lineptr + (uintptr_t)n+stream);
        __builtin_unreachable();
    }

    size_t pos = 0;
    for (;;) {
        char c;
        ssize_t ret = syscall(SYS_read, stream->fd, &c, 1);
        if (ret < 0) return ret;
        if (ret == 0) return pos;

        if (c == '\n') {
            (*lineptr)[pos++] = '\n';
            (*lineptr)[pos++] = '\0';
            return pos - 1;
        }

        (*lineptr)[pos++] = c;

        if (pos >= *n) {
            *n *= 2;
            *lineptr = realloc(*lineptr, *n);
        }

        if (*lineptr == NULL) {
            return -ENOMEM;
        }
    }
    return pos;
}

FILE *fopen(const char *restrict pathname, const char *restrict mode) {
    int omode = 0;
    switch (mode[0]) {
    case 'r':
        omode = O_RDONLY;
        break;
    case 'w':
        omode = O_WRONLY | O_CREAT | O_TRUNC;
        break;
    case 'a':
        omode = O_WRONLY | O_CREAT | O_APPEND;
        break;
    default:
        errno = EINVAL;
        return NULL;
    }

    if (strlen(mode) > 1) {
        if (mode[1] == '+') {
            omode &= ~(O_RDONLY | O_WRONLY);
            omode |= O_RDWR;
        } else if (mode[1]  == 'b') {
            // check for mode[2] == '+' ?
            if (strlen(mode) > 2) {
                if (mode[2] == '+') {
                    omode &= ~(O_RDONLY | O_WRONLY);
                    omode |= O_RDWR;
                } else {
                    errno = EINVAL;
                    return NULL;
                }
            }
        } else {
            errno = EINVAL;
            return NULL;
        }
    }

    int fd = syscall(SYS_open, pathname, omode, 0);
    if (fd < 1) {
        return NULL;
    }

    FILE *file = malloc(sizeof(FILE));
    file->fd = fd;
    file->buf_pos = 0;
    return file;
}

size_t fread(void *restrict ptr, size_t size, size_t nmemb,
             FILE *restrict stream) {
    size_t read = 0;
    for (size_t i = 0; i < nmemb; i++) {
        int nread = syscall(SYS_read, stream->fd, ptr + i * size, size);
        if (nread == 0) {
            return read;
        }
        if (nread < 0) {
            return -1;
        }
        read += nread;
    }
    return read;
}

int fclose(FILE *stream) {
    int ret = close(stream->fd);
    free(stream);
    return ret;
}

int fputc(int c, FILE *stream) {
    return syscall(SYS_write, stream->fd, (char*)&c, 1);
}

long ftell(FILE *stream) {
    return lseek(stream->fd, 0, SEEK_CUR);
}

int fseek(FILE *stream, long offset, int whence) {
    return lseek(stream->fd, offset, whence);
}

size_t fwrite(const void *restrict ptr, size_t size, size_t nmemb,
              FILE *restrict stream) {
    size_t written = 0;
    for (size_t i = 0; i < nmemb; i++) {
        int nread = syscall(SYS_write, stream->fd, ptr + i * size, size);
        if (nread == 0) {
            return written;
        }
        if (nread < 0) {
            return -1;
        }
        written += nread;
    }
    return written;
}

char *gets(char *s) {
    size_t n = 0;
    for (;;) {
        char c;
        ssize_t ret = syscall(SYS_read, 0, &c, 1);
        if (ret < 0) return NULL;
        if (ret == 0) return s;

        if (c == '\n') {
            s[n++] = '\n';
            s[n++] = '\0';
            return s;
        }

        s[n++] = c;

        if (n >= 1024) {
            return NULL;
        }
    }
    return s;
}

char *fgets(char *restrict s, int size, FILE *restrict stream) {
    size_t n = 0;
    for (;;) {
        char c;
        ssize_t ret = syscall(SYS_read, stream->fd, &c, 1);
        if (ret < 0) return NULL;
        if (ret == 0) return s;

        if (c == '\n') {
            s[n++] = '\n';
            s[n++] = '\0';
            return s;
        }

        s[n++] = c;

        if (n >= (size_t)size) {
            return NULL;
        }
    }
    return s;
}
