#ifndef __UNISTD_H_
#define __UNISTD_H_

#include <stdint.h>
#include <sys/types.h>
#include <fcntl.h>

typedef uint32_t uid_t;

// creates a new process by duplicating the child process
pid_t fork(void);

// replaces the current process imange with a new process image
int execve(const char *path, char *const argv[], char *const envp[]);
int execv(const char *path, char *const argv[]);
int execvp(const char *name, char *const argv[]);

// write to a file descriptor
ssize_t write(int fd, const void *buf, size_t count);

// read from a file descriptor
ssize_t read(int fd, void *buf, size_t count);

// get current working directory
char *getcwd(char *buf, size_t size);

// get directory entries
ssize_t getdents(int fd, void *dirp, size_t count);

// change current working directory
int chdir(const char *path);

// gets the current pid
pid_t getpid(void);

void _exit(int code);

// get current uid
uid_t getuid(void);

// get current euid
uid_t geteuid(void);

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

// move the read/write file offset
off_t lseek(int fd, off_t offset, int whence);
#define SEEK_SET 0
#define SEEK_CUR 1

typedef uint32_t useconds_t;
int usleep(useconds_t usec);

typedef uint64_t time_t;

struct timespec {
    time_t tv_sec;
    uint64_t tv_nsec;
};

#endif // __UNISTD_H_
