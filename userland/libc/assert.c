#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

void __assert_fail(const char *restrict str, const char *restrict file,
                   const unsigned int line) {
    printf("Assertion failed: %s at %s:%d\n", str, file, line);
    exit(-1);
}
