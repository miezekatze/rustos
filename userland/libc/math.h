#ifndef _MATH_H_
#define _MATH_H_

double floor(double x);
double round(double x);
double scalbn(double x, int exp);

#endif //_MATH_H_
