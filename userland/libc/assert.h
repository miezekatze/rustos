#ifndef _ASSERT_H_
#define _ASSERT_H_

#define static_assert _Static_assert

void __assert_fail(const char *restrict str, const char *restrict file, const unsigned int line);

#define assert(expr)                                       \
    ((void) sizeof ((expr) ? 1 : 0), __extension__ ({      \
            if (expr)                                      \
                ; /* empty */                              \
            else                                           \
                __assert_fail (#expr, __FILE__, __LINE__); \
        }))

#endif // _ASSSERT_H_
