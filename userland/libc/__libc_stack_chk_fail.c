#include <stdio.h>
#include <stdlib.h>

void __stack_chk_fail() {
    puts("--- stack smashing detected ---");
    exit(-1);
}
