#include "errno.h"
#include "stdio.h"
#include "string.h"

static int __errno_val = 0;

int *__errno_impl(void) {
    return &__errno_val;
}

char *strerror(int err) {
    switch (err) {
    case ESUCC:
        return "Sucess. No error.";
    case ENOENT:
        return "No such file or directory";
    case EISDIR:
        return "Is a directory";
    case ENOTDIR:
        return "Not a directory";
    case ENOEXEC:
        return "Exec format error";
    case ENOSYS:
        return "Function not implemented";
    case EINVAL:
        return "Invalid argument";
    case ENOMEM:
        return "Not enought space / cannot allocate memory";
    case ENAMETOOLONG:
        return "Filname too long";
    case EACCES:
        return "Permission denied";
    case EEXIST:
        return "File exists";
    case ENOSPC:
        return "No space left on device";
    case ESPIPE:
        return "Illegal seek";
    case ERANGE:
        return "Numerical result out of range";
    case EDOM:
        return "Numerical argument out of domain";
    case ECHILD:
        return "No child process";
    case ENOTSUP:
        return "Operation not supported";
    default:
        return "Unknown error";
    }
}

extern void kprint(const char*);

void perror(const char *prefix) {
    printf("%s: %s\n", prefix, strerror(errno));
}
