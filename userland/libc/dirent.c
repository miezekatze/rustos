#include "dirent.h"
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <sys/stat.h>

typedef struct __dirstream {
    int fd;
    void *readdir_buf;
    size_t readdir_buf_size;
    size_t readdir_buf_pos;
} DIR;

DIR *opendir(const char *name) {
    DIR *dir;
    int fd;

    fd = open(name, O_RDONLY | O_DIRECTORY, 0);
    if (fd == -1) {
        return NULL;
    }

    dir = malloc(sizeof(DIR));
    if (dir == NULL) {
        close(fd);
        errno = ENOMEM;
        return NULL;
    }

    dir->fd = fd;
    dir->readdir_buf = NULL;
    dir->readdir_buf_size = 0;
    dir->readdir_buf_pos = 0;
    return dir;
}

static struct dirent *from_sys_dirent(struct sys_dirent *de) {
    struct dirent *dirent = malloc(sizeof(struct dirent));
    if (dirent == NULL) {
        return NULL;
    }
    *dirent = (struct dirent) {
        .d_ino = de->d_ino,
        .d_off = de->d_off,
        .d_reclen = de->d_reclen,
        .d_name = de->d_name,
        .d_type = *((uint8_t*)de + de->d_reclen - 1), 
    };
    return dirent;
}

#define READDIR_BUF_SIZE 64

struct dirent *readdir(DIR *dir) {
    if (dir->readdir_buf == NULL) {
        dir->readdir_buf = malloc(READDIR_BUF_SIZE);
        if (dir->readdir_buf == NULL) {
            return NULL;
        }
        dir->readdir_buf_size = READDIR_BUF_SIZE;
        dir->readdir_buf_pos = 0;
    } else if (dir->readdir_buf_pos < dir->readdir_buf_size) {
        struct sys_dirent *de = dir->readdir_buf + dir->readdir_buf_pos;
        dir->readdir_buf_pos += de->d_reclen;

        return from_sys_dirent(de);
    }
    struct sys_dirent *de = malloc(READDIR_BUF_SIZE);
    if (de == NULL)
        return NULL;

    int ret = getdents(dir->fd, de, READDIR_BUF_SIZE);
    if (ret == -1) {
        free(de);
        return NULL;
    } else if (ret == 0) {
        free(de);
        free(dir->readdir_buf);
        dir->readdir_buf = NULL;
        dir->readdir_buf_size = 0;
        dir->readdir_buf_pos = 0;
        return NULL;
    }

    dir->readdir_buf = de;
    dir->readdir_buf_size = ret;
    dir->readdir_buf_pos = de->d_reclen;

    return from_sys_dirent(de);
}

