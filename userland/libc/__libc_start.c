#include <sys/syscall.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

extern int main(int argc, char **argv);

char **envp;

__attribute__((naked))
void _start(void) {
    asm("__start:\n"
        "movq %rsp, %rdi\n"
        "jmp _libc_start\n");
}

void _libc_start(long rsp) {
    // get argc and argv
    int argc = *(long *)(rsp + 0);
    char **argv = (char **)(rsp + 8);
    envp = *(char ***)(rsp + 16);

    int ret = main(argc, argv);
    syscall(SYS_exit, ret);
}
