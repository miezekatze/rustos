extern void exit(int a);

void _fini() {
    exit(1);
}
