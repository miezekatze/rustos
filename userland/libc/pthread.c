#include "pthread.h"
#include <errno.h>
#include <sys/syscall.h>

#include <stdio.h>

int pthread_create(pthread_t *restrict thread,
                   const pthread_attr_t *restrict attr,
                   void *(*start_routine)(void *), void *restrict arg) {
    if (attr != NULL) {
        errno = ENOSYS;
        return ENOSYS;
    }

    pthread_t tid = syscall(SYS_pthread_create, start_routine, arg);
    *thread = tid;
    return 0;
}
