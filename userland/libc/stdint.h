#ifndef _STDINT_H_
#define _STDINT_H_

#include <assert.h>
#include <stddef.h>

typedef unsigned long int uint64_t;
static_assert(sizeof(uint64_t) == 8, "uint64_t is 64 bits");

typedef unsigned long int int64_t;
static_assert(sizeof(int64_t) == 8, "int64_t is 64 bits");

typedef unsigned int uint32_t;
static_assert(sizeof(uint32_t) == 4, "uint32_t is 32 bits");

typedef signed int int32_t;
static_assert(sizeof(int32_t) == 4, "int32_t is 32 bits");

typedef unsigned short uint16_t;
static_assert(sizeof(uint16_t) == 2, "uintp16_t is 16 bits");

typedef unsigned char uint8_t;
static_assert(sizeof(uint8_t) == 1, "uint8_t is 8 bits");

typedef signed char int8_t;
static_assert(sizeof(int8_t) == 1, "int8_t is 8 bits");

typedef unsigned long uintptr_t;
static_assert(sizeof(uintptr_t) == 8, "uintptr_t is 32 bits");

typedef long intptr_t;
static_assert(sizeof(intptr_t) == 8, "intptr_t is 32 bits");

typedef int64_t intmax_t;

#define UINT64_C(c) __UINT64_C(c)

#endif // _STDINT_H_
