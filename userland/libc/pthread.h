#ifndef _PTHREAD_H_
#define _PTHREAD_H_

#include <stdint.h>

typedef size_t pthread_t;
typedef void* pthread_attr_t;

/** create a new thread */
int pthread_create(pthread_t *restrict thread,
                          const pthread_attr_t *restrict attr,
                          void *(*start_routine)(void *),
                          void *restrict arg);

#endif // _PTHREAD_H_
