#include <string.h>
#include <stdlib.h>
#include <stdint.h>

size_t strlen(const char *s) {
    size_t len;
    for (len = 0; s[len] != 0; len++);
    return len;
}

void *memcpy(void *restrict dest, const void *restrict src, size_t n) {
    for (size_t pos = 0; pos < n; ((char*)dest)[pos] = ((char*)src)[pos], pos++);
    return dest;
}

char *strndup(const char *s, size_t n) {
    char *ret = malloc(n + 1);
    memcpy(ret, s, n);
    ret[n] = 0;
    return ret;
}

char *strdup(const char *s) {
    return strndup(s, strlen(s));
}

char *strcpy(char *restrict dest, const char *src) {
    size_t i;
    size_t n = strlen(src);
    for (i = 0; i < n && src[i] != '\0'; i++)
        dest[i] = src[i];
    for ( ; i <= n; i++)
        dest[i] = '\0';

    return dest;
}

int strncmp(const char *s1, const char *s2, size_t n) {
    for (size_t i = 0; i < n; i++) {
        uint8_t a = (uint8_t)s1[i];
        uint8_t b = (uint8_t)s2[i];
        if (a == b && b == 0) return 0;
        if (a == 0) return -1;
        if (b == 0) return 1;
        if (a == b) continue;
        if (a < b) return -1;
        if (a > b) return 1;
    }
    return 0;
}

int strcmp(const char *s1, const char *s2) {
    size_t n = strlen(s1);
    if (strlen(s2) > n) n = strlen(s2);
    return strncmp(s1, s2, n);
}

char *strncat(char *restrict dest, const char *restrict src, size_t n) {
    for (size_t i = 0; i < n; i++) {
        if (src[i] == 0) break;
        dest[strlen(dest)] = src[i];
    }
    return dest;
}

char *strcat(char *restrict dest, const char *restrict src) {
    return strncat(dest, src, strlen(src));
}

char *strchr(const char *s, int c) {
    for (size_t i = 0; i < strlen(s); i++)
        if (s[i] == c) return (char*)s + i;
    return NULL;
}

void *memset(void *s, int c, size_t n) {
    for (char *ptr = s; ptr < (char*)s + n; ptr++) {
        *ptr = c;
    }
    return s;
}
