#ifndef _DIRENT_H_
#define _DIRENT_H_

#include <stdint.h>

enum {
    DT_UNKNOWN = 0,
    DT_FIFO = 1,
    DT_CHR = 2,
    DT_DIR = 4,
    DT_BLK = 6,
    DT_REG = 8,
    DT_LNK = 10,
    DT_SOCK = 12,
    DT_WHT = 14
};

typedef struct __dirstream DIR;
DIR *opendir(const char *name);

struct dirent {
    size_t d_ino;
    ssize_t   d_off;
    uint16_t d_reclen;
    uint8_t d_type;
    char *d_name;
};

struct sys_dirent {
    size_t d_ino;
    ssize_t   d_off;
    uint16_t d_reclen;
    char d_name[];
};

struct dirent *readdir(DIR *dirp);

#endif // _DIRENT_H_
