#include "unistd.h"
#include <sys/syscall.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

pid_t fork(void) {
    return syscall(SYS_fork);
}

int execve(const char *path, char *const argv[], char *const envp[]) {
    return syscall(SYS_execve, path, argv, envp);
}

extern void *envp;
int execv(const char *path, char *const argv[]) {
    return execve(path, argv, envp);
}

int execvp(const char *name, char *const argv[]) {
    puts("execvp() does not currently not search for paths.\nTODO: implement");
    return execv(name, argv);
}

ssize_t write(int fd, const void *buf, size_t count) {
    return syscall(SYS_write, fd, buf, count);
}

ssize_t read(int fd, void *buf, size_t count) {
    return syscall(SYS_read, fd, buf, count);
}

char *getcwd(char *buf, size_t size) {
    int ret = syscall(SYS_getcwd, buf, size);
    if (ret < 0) {
        return NULL;
    }
    return buf;
}

ssize_t getdents(int fd, void *dirp, size_t count) {
    return syscall(SYS_getdents, fd, dirp, count);
}

int chdir(const char *path) {
    return syscall(SYS_chdir, path);
}

pid_t getpid(void) {
    return syscall(SYS_getpid);
}

void _exit(int code) {
    exit(code);
}

uid_t geteuid(void) {
    return syscall(SYS_geteuid);
}

off_t lseek(int fd, off_t offset, int whence) {
    return syscall(SYS_lseek, fd, offset, whence);
}

int usleep(useconds_t usec) {
    struct timespec ts = {
        .tv_sec = usec / 1000000,
        .tv_nsec = (usec % 1000000) * 1000,
    };

    return syscall(SYS_nanosleep, &ts, NULL);
}
