#include "unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main(void) {
    char file[100];
    if (getcwd(file, 100) == NULL) {
        perror("getcwd");
        exit(1);
    }
    printf("%s\n", file);
}
