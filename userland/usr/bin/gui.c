#include <string.h>
#include <sys/syscall.h>

#include <fcntl.h>
#include <unistd.h>

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <pthread.h>

static uint32_t mouse_x = 0;
static uint32_t mouse_y = 0;

static uint32_t mouse_fd = 0;

void *mouse_loop(void *arg) {
    (void)arg;
    for (;;) {
        if (mouse_fd == 0) continue;
        printf("MOUSE: %d\n", mouse_x);
        uint8_t mbuf[3];
        read(mouse_fd, mbuf, 3);
        uint8_t flags = mbuf[0];

        uint8_t x_negative = flags & 0x10;
        uint8_t y_negative = flags & 0x20;

        int dx = 0x00000000 | mbuf[1];
        int dy = 0x00000000 | mbuf[2];

        if (x_negative) dx |= 0xFFFFFF00;
        if (y_negative) dy |= 0xFFFFFF00;

        mouse_x += dx;
        mouse_y -= dy;

        mouse_x = (mouse_x+1920)%1920;
        mouse_y = (mouse_y+1080)%1080;
    }
}

uint8_t *load_cursor() {
    FILE *file = fopen("/usr/share/images/cursor.ppm", "rb");
    if (file == NULL) {
        perror("load_cursor: fopen");
        exit(1);
    }
    char *line = malloc(2);
    size_t n = 2;
    int nread = getline(&line, &n, file);

    if (nread != 3 || (line[0] != 'P' || line[1] != '6')) {
        printf("load_cursor: Cursor is not a PPM (P6) file: '%s'\n", line);
        exit(1);
    }

    nread = getline(&line, &n, file);
    int w = -1, h = -1;
    for (size_t i = 0; i < (size_t)nread; i++) {
        if (line[i] == ' ') {
            line[i] = 0;
            line[nread-1] = '\0';
            w = atoi(line);
            h = atoi(line + i + 1);
            break;
        }
    }

    if (w > 1920 || h > 1080) {
        printf("gui: [%d x %d] out of bounds of [1920 x 1080] display\n", w, h);
        exit(1);
    }

    // ingore next line
    getline(&line, &n, file);
    uint8_t *file_buf = malloc(w * h * 3);
    fread(file_buf, w * h * 3, 1, file);
    return file_buf;
}

#ifdef __os_syscalls__

int main(void) {
    mouse_fd = open("/dev/input/mouse0", O_RDONLY);

    pthread_t thread;
    int ret = pthread_create(&thread, NULL, mouse_loop, NULL);
    if (ret != 0) {
        fprintf(stderr, "pthread_create: %s\n", strerror(ret));
        exit(1);
    }

    uint16_t *buf = malloc(1920 * 1080 * sizeof(uint16_t));

    uint8_t *cursor_buf = load_cursor();
    uint32_t last_mouse_x = 0;
    uint32_t last_mouse_y = 0;
    
    while (true) {
        // clear old rect
        size_t rx = last_mouse_x;
        size_t ry = last_mouse_y;
        for (size_t x = rx-16; x < rx+16; x++) {
            for (size_t y = ry-16; y < ry+16; y++) {
                buf[(y*1920 + x) % (1920*1080)] = 0x0000;
            }
        }

        last_mouse_x = mouse_x;
        last_mouse_y = mouse_y;

        rx = mouse_x;
        ry = mouse_y;
        for (size_t x = rx-16, i = 0; x < rx+16; x++, i++) {
            for (size_t y = ry-16, j = 0; y < ry+16; y++, j++) {
                uint8_t r = cursor_buf[(j * 32 + i) * 3 + 0];
                uint8_t g = cursor_buf[(j * 32 + i) * 3 + 1];
                uint8_t b = cursor_buf[(j * 32 + i) * 3 + 2];

                if (r != 0 || g != 0 || b != 0)
                    buf[(y*1920 + x) % (1920*1080)] = ((r & 0b11111000) << 8) | 
                        ((g & 0b11111100) << 3) | ((b & 0b11111000) >> 3);
            }
        }

        syscall(SYS_render_buf, buf);
    }

    close(mouse_fd);
}


#else
int main(void){return 255;}
#endif
