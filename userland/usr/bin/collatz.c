#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>

FILE *f;
char *output_path = "./output.bin";
char seperator = 32;

void output(char *c) {
	fprintf(f, "%s", c);
}

void collatz(int i, bool binary) {
	long int num = i;
	unsigned long int n = 0;
	while (num != 1) {
		if (num%2 == 0) {
			num /= 2;
		} else {
			num = 3*num+1;
		}
		n++;
	}
	if (!binary) {
        //		char buf[20];
        //		snprintf(buf, 19, "%ld\n", n);
        //		output(buf);
	} else {
		char *origin = (char*)(&n);
		int len = strlen(origin);
        
		char *str = malloc(len+1);
		strcpy(str, origin);
        
		output(str);
		free(str);
	}
}

void print_usage() {
	printf("Simple collatz solver.\nCaclulates collatz steps for a given count of numbers and stores it into a binary file with a SPACE [0x20] character as speretor\n\n");
    printf("Usage: collatz [OPTIONS] COUNT [START]\n");
    printf("START: start value to calculate collatz values\n");
    printf("COUNT: count of values to calculate\n");
    printf("\nOptions:\n");
    printf("\t-h | --help\tshows this message\n");
    printf("\t-o | --output\tset output file (default: ./output.bin)\n");
    printf("\t-a | --ascii\toutput ascii instead of binary [\033[1;33mWARNING\033[0m: this can produce very large files]\n");
}

void error(char *s) {
    fprintf(stderr, "\033[1;31mERROR: %s\033[0m\n", s );
    print_usage();
    exit(-1);
}

int main(int argc, char *argv[]) {
    int start = 1;
    int count = 0;
    bool binary = true;
	size_t pos = 0;

	size_t args = 1;
	
    while (++pos < (size_t)argc) {
        if (strncmp(argv[pos], "-", 1) == 0) {
            char* cmd = argv[pos];
            if (strcmp(cmd, "-h") == 0 || strcmp(cmd, "--help") == 0) {
                print_usage();
                exit(1);
            } else if(strcmp(cmd, "-o") == 0 || strcmp(cmd, "--output") == 0) {
                output_path = argv[pos+1];
                pos++;
            } else if(strcmp(cmd, "-a") == 0 || strcmp(cmd,  "--ascii") == 0) {
                binary = false;
            } else if (strlen(cmd) > 1) {
                char msg[17] = "Unknown option: ";
                strncat(msg, cmd, 16);
                error(msg);
            }
        } else {
			if (args == 1) {
				args++;
				count = atoi(argv[pos]);
				if (count == 0) {
					error("Please enter a valid number for COUNT");
				}
			} else if (args == 2) {
				args++;
				start = atoi(argv[pos]);
				if (start == 0) {
					error("Please enter a valid number for START");
				}
			} else {
				error("Too many arguments!");
			}
		}
    }

	if (args == 1) {
		error("No COUNT argument given");
	}

	f = fopen(output_path, "w");

	if (f == NULL) {
	    perror("Error opening file");
	    exit(1);
	}
	for (int i = start; i < start+count; i++) {
		collatz(i, binary);
	}

	fclose(f);
}
