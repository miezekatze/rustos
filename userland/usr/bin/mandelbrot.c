#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/syscall.h>

#define RGB_TO_32BIT(r, g, b) (r << 16) | \
                              (g << 8)  | \
                              (b << 0)

uint32_t calc(size_t x, size_t y) {
    float zx = 0, zy = 0;
    float cx = 4 * (x / 540.0 - 0.5);
    float cy = 3 * (y / 540.0 - 0.5);

    for (size_t it = 0; it < 50; it++) {
        float ozx = zx;
        zx = zx * zx - zy * zy + cx;
        zy = 2 * ozx * zy + cy;

        if (zx * zx + zy * zy > 2 * 2) {
            int r = (it * 20) % 256;
            int g = (it * 10) % 256;
            int b = (it * 5) % 256;
            return RGB_TO_32BIT(r, g, b);
        }
    }
    return 0xffff;
}

int main(void) {
    // assume 1920x1080 (for now)
    uint32_t *buf = malloc(1920 * 1080 * sizeof(uint32_t));

    puts("HERE!");

    for (size_t y = 0; y < 1080; y++) {
        for (size_t x = 0; x < 1080; x++) {
            buf[y*1920+x] = calc(x, y);
        }
    }

    puts("THERE!");
#ifdef __os_syscalls__
    syscall(SYS_render_buf, buf);
#else
    puts("rendering is not supported on linux");
#endif
    free(buf);
}
