#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>

char *get_str_alloc_colon_sep(const char *line, size_t len, int num_colons) {
    int colons = 0;
    char *str = NULL;
    for (size_t i = 0; i < len-1; i++) {
        if (line[i] == ':') colons++;
        else if (colons == num_colons) {
            // get end of number
            int j;
            for (j = i; line[j] != ':'; j++);
            // alloc string
            str = malloc(j - i + 1);
            memcpy(str, line+i, j-i);
            // set null terminator
            str[j-i] = '\0';
            break;
        }
    }
    assert(colons == num_colons);
    return str;
}

int main(void) {
    int uid = geteuid();
    FILE *file = fopen("/etc/passwd", "r");
    if (file == NULL) {
        perror("fopen");
        exit(1);
    }

    char *line = malloc(10);
    size_t sz = 10;
    int len = getline(&line, &sz, file);
    if (len < 0) {
        perror("getline");
        exit(1);
    } else if (len > 1024) {
        fprintf(stderr, "line too long\n");
        exit(1);
    }

    // change
    line[len - 1] = '\0';

    char *name = NULL;
    for (;;) {
        char *num_str = get_str_alloc_colon_sep(line, len, 2);
        int num = atoi(num_str);
        free(num_str);

        if (num == uid) {
            name = get_str_alloc_colon_sep(line, len, 0);
            break;
        } else {
            line = realloc(line, 10);
            len = getline(&line, &sz, file);
            if (len < 0) {
                perror("getline");
                exit(1);
            } else if (len == 0) {
                break;
            }
            line[len - 1] = '\0';
        }
    }

    printf("%s\n", name != NULL ? name : "u r nameless");
    free(name);

    free(line);

//    printf("uid: %d\n", );
}
