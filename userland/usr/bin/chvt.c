#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>

#ifndef __os_syscalls__
int main(void) {return 255;};
#else
int main(int argc, char **argv) {
    if (argc < 2) {
        puts("Not enough arguments.");
        return 1;
    }

    const char *num_str = argv[1];
    int num = atoi(num_str);

//    syscall_chvt(num); // TODO: change to ioctl
    (void)num;
    return 0;
}
#endif
