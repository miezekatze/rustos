#include <pthread.h>
#include <stdio.h>

#include <sys/syscall.h>

void *second_thread(void *a) {
    for (;;) {
        puts("Thread 1");
    }
    return a;
}

int main(void) {
    printf("MAIN THREAD\n");
    pthread_t thread;
    pthread_create(&thread, NULL, second_thread, NULL);
    printf("Thread id = %d\n", (int)thread);
    for (;;) {
        puts("Thread 2");
    }
    return 0;
}
