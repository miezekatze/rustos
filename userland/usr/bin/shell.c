// c standard
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
// POSIX
#include <unistd.h>
// os specific
#include <sys/wait.h>
#include <sys/syscall.h>

int readline(char **buf, size_t *buf_size, const char *prompt) {
    if ((*buf) == NULL) {
        *buf = malloc(10);
        *buf_size = 10;
    }

    write(STDOUT_FILENO, prompt, strlen(prompt));
    
    size_t cursor_pos = 0;
    size_t line_len = 0;
    bool escape = false, escape_ext = false;
    for (;;) {
        char c;
        int ret = fread(&c, 1, 1, stdin);
        if (ret < 0) {
            perror("shell; fread");
            exit(1);
        }
        if (ret == 0) break;
#define UPDATE                                  \
        for (size_t i = cursor_pos; i <= line_len; i++) {   \
            write(STDOUT_FILENO, " ", 1);                   \
        }                                                   \
        write(STDOUT_FILENO, "\r", 1);                      \
        write(STDOUT_FILENO, prompt, strlen(prompt));       \
        write(STDOUT_FILENO, *buf, line_len);               \
        for (size_t i = line_len; i > cursor_pos; i--)      \
            write(STDOUT_FILENO, "\b", 1);                  \
        
#define OUT(c)                                                  \
        if (line_len + 2 >= *buf_size) {                        \
            *buf_size = line_len + 10;                          \
            *buf = realloc(*buf, *buf_size);                    \
        }                                                       \
        for (size_t i = line_len + 1; i > cursor_pos; i--) {    \
            (*buf)[i] = (*buf)[i - 1];                          \
        }                                                       \
        (*buf)[cursor_pos++] = c;                               \
        (*buf)[cursor_pos] = '\0';                               \
        line_len++;                                             \
        UPDATE;
        
        switch (c) {
        case 1:
            // ctrl + a
            cursor_pos = 0;
            UPDATE;
            break;
        case 5:
            // ctrl + e
            cursor_pos = line_len;
            UPDATE;
            break;
        case '\b':
            if (cursor_pos <= 0) break;
            write(1, "\b", 1);
            cursor_pos--;
            line_len--;
            for (size_t i = cursor_pos; i < line_len; i++) {
                (*buf)[i] = (*buf)[i + 1];
            }
            (*buf)[line_len] = '\0';

            UPDATE;
            break;
        case '\n':
            cursor_pos = line_len;
            goto broken;
            break;
        case '\033':
            escape = true;
            break;
        case '[':
            if (escape) {
                escape_ext = true;
            } else {
                OUT(c);
            }
            break;
        case 'A':
            if (escape_ext) {
                escape = escape_ext = false;
            } else {
                OUT(c);
            }
            break;
        case 'B':
            if (escape_ext) {
                escape = escape_ext = false;
            } else {
                OUT(c);
            }
            break;
        case 'C':
            if (escape_ext) {
                escape = escape_ext = false;
                if (cursor_pos >= line_len) break;
                char c = (*buf)[cursor_pos];
                write(1, &c, 1);
                cursor_pos++;
            } else {
                OUT(c);
            }
            break;
        case 'D':
            if (escape_ext) {
                escape = escape_ext = false;
                if (cursor_pos <= 0) break;
                cursor_pos--;
                write(1, "\b", 1);
            } else {
                OUT(c);
            }
            break;
        default:
            OUT(c);
            break;
        }
    }
    broken:
    
    if (line_len + 2 >= *buf_size) {
        *buf_size += 10;
        *buf = realloc(*buf, *buf_size);
    }
    
    (*buf)[line_len++] = '\n';
    write(STDOUT_FILENO, "\n", 1);
    (*buf)[line_len] = '\0';
    
    return line_len;
}

int main(int argc, char **argv) {
    // switch to raw mode
#ifdef __os_syscalls__
//    syscall_term_attributes(0, 0);
#endif
    
    size_t buf_sz = 10;
    char *buf = malloc(buf_sz);
    for (;;) {
        int ret = readline(&buf, &buf_sz, "$ ");
        if (ret < 0) {
            perror("shell: readline");
            exit(1);
        }
        if (ret == 0) continue;
        buf[ret-1] = '\0';

        // parse the command
        size_t arg_start = ret;
        for (;*buf==' ';buf++,ret--);

        for (size_t i = 0; i < (size_t)ret; i++) {
            if (buf[i] == ' ') {
                buf[i] = '\0';
                arg_start = i + 1;
                break;
            }
        }
        
        if (strlen(buf) == 0) continue;

        if (strcmp(buf, "exit") == 0) {
            exit(/*atoi(buf + arg_start)*/0);
        } else if (strcmp(buf, "cd") == 0) {
            if (chdir(buf + arg_start) < 0) {
                perror("cd");
            }
            continue;
        }

        // check if buf contains a '/'
        bool has_slash = false;
        for (size_t i = 0; buf[i] != 0; i++) {
            if (buf[i] == '/') {
                has_slash = true;
                break;
            }
        }

#define PREFIX "/usr/bin"
        char *path = NULL;
        if (!has_slash) {
            path = malloc(strlen(PREFIX) + 1 + strlen(buf) + 1);
            strcpy(path, PREFIX);
            path[strlen(PREFIX)] = '/';
            strcpy(path + strlen(PREFIX) + 1, buf);
        } else {
            path = buf;
        }

        if (fork() == 0) {
#ifdef __os_syscalls__
//            syscall_term_attributes(1, 1);
#endif
            char *nargv[] = {
                path,
                (strlen(buf + arg_start) > 0) ?
                buf + arg_start : NULL,
                NULL,
            };
            char *envp[] = {
                buf,
                NULL,
            };
            execve(path, nargv, envp);

            // if here, error has occured
            perror("shell: exec");
            exit(1);
        } else {
            wait(NULL);
#ifdef __os_syscalls__
//            syscall_term_attributes(0, 0);
#endif
        }

        (void)(argv + argc);
    }
}
