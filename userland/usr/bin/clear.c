#include <unistd.h>

#define CLEAR_STR "\033[H\033[2J\033[3J"

int main(void) {
    write(STDOUT_FILENO, CLEAR_STR, sizeof(CLEAR_STR) - 1);
    return 0;
}
