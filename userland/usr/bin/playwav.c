#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>

typedef struct {
    uint8_t riff[4];
    uint32_t size;
    uint8_t wave[4];

    uint8_t fmt[4];
    uint32_t fmt_size;
    uint16_t format;
    uint16_t channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t block_align;
    uint16_t bits_per_sample;

    uint8_t data[4];
    uint32_t data_size;
} __attribute__((packed)) wavhdr_t;

typedef struct {
    uint8_t magic[4];
    uint32_t size;
    uint32_t num_samples;
    uint32_t sample_rate;
    uint8_t channels;
    uint8_t bits_per_sample;
    void *data;
} dsp_header_t;

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }

    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) {
        printf("Failed to open file: %s: %s\n", argv[1], strerror(errno));
        return 1;
    }

    struct stat st;
    if (fstat(fd, &st) < 0) {
        printf("Failed to stat file: %s\n", argv[1]);
        return 1;
    }

    char *buf = malloc(st.st_size);
    ssize_t n = read(fd, buf, st.st_size);
    if (n < st.st_size) {
        printf("Failed to read file: only read %d of %d bytes: %s\n", (int)n, (int)st.st_size, strerror(errno));
    }

    wavhdr_t *hdr = (wavhdr_t *) buf;

    if (strncmp((char*)hdr->riff, "RIFF", 4) != 0) {
        printf("Invalid WAV file (no RIFF): %s\n", argv[1]);
        return 1;
    }

    if (strncmp((char*)hdr->wave, "WAVE", 4) != 0) {
        printf("Invalid WAV file (no WAVE): %s\n", argv[1]);
        return 1;
    }

    if (strncmp((char*)hdr->fmt, "fmt ", 4) != 0) {
        printf("Invalid WAV file (no fmt): %s\n", argv[1]);
        return 1;
    }

    if (hdr->format != 1) {
        printf("Invalid WAV file (not PCM): %s\n", argv[1]);
        return 1;
    }

    if (hdr->bits_per_sample != 8 && hdr->bits_per_sample != 16) {
        printf("Invalid WAV file (not 8 or 16 bit): %s\n", argv[1]);
        return 1;
    }

    if (hdr->channels != 1 && hdr->channels != 2) {
        printf("Invalid WAV file (not mono or stereo): %s\n", argv[1]);
        return 1;
    }

    dsp_header_t dsp_hdr = {
        .magic = {'D', 'S', 'P', 'H'},
        .size = sizeof(dsp_header_t) + hdr->data_size,
        .num_samples = hdr->data_size / (hdr->bits_per_sample / 8),
        .sample_rate = hdr->sample_rate,
        .channels = hdr->channels,
        .bits_per_sample = hdr->bits_per_sample,
    };

    void *chunk = buf + 12;

    while (strncmp((char*)chunk, "data", 4) != 0) {
        uint32_t chunk_size = *(uint32_t*)(chunk + 4);
        chunk += 8 + chunk_size;
    }

    chunk += 8;
    size_t chunk_size = *(uint32_t*)(chunk - 4);

    printf("chunk size: %d\n", (int)chunk_size);
    dsp_hdr.data = chunk;
//    printf("chunk data: %p\n", chunk);

    int dsp_fd = open("/dev/dsp", O_WRONLY);
    write(dsp_fd, &dsp_hdr, sizeof(dsp_hdr));

    close(dsp_fd);
    close(fd);
    return 0;
}
