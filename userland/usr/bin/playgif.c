#include <stdbool.h>
#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "fcntl.h"
#include "sys/mman.h"
#include "sys/syscall.h"

typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} __attribute__((packed)) ct_entry_t;

typedef struct {
    uint8_t magic[6];
    uint16_t width;
    uint16_t height;
    uint8_t flags;
    uint8_t bgcolor;
    uint8_t aspect;
    ct_entry_t gct[256];
} __attribute__((packed)) gif_header_t;

typedef struct {
    uint8_t byte;
    int32_t prev;
    size_t len;
} table_entry_t;

typedef struct {
    table_entry_t entries[4096];
} table_t;

void output_color(uint8_t idx, uint32_t *buf, size_t *buf_idx, gif_header_t header, size_t width, size_t left) {
    ct_entry_t entry = header.gct[idx];
    (void)entry;

    if (*buf_idx % 1920 >= (width + left)) {
        *buf_idx += 1920 - width;
    }

    buf[*buf_idx] = entry.b | (entry.g << 8) | (entry.r << 16) | (0xff << 24);
    (*buf_idx)++;
}

void output_string(uint16_t incoming_code, table_t *table, uint32_t *buf, size_t *buf_idx, gif_header_t header, size_t width, size_t left) {
    size_t string_len = table->entries[incoming_code].len;
    if (string_len == 1) {
        output_color(table->entries[incoming_code].byte, buf, buf_idx, header, width, left);
    } else {
        output_string(table->entries[incoming_code].prev, table, buf, buf_idx, header, width, left);
        output_color(table->entries[incoming_code].byte, buf, buf_idx, header, width, left);
    }
}

uint8_t first_byte(table_t table, uint16_t code) {
    int32_t init = table.entries[code].prev;
    while (init >= 0) {
        code = init;
        init = table.entries[code].prev;
    }
    return table.entries[code].byte;
}

uint16_t next_code(size_t code_length, uint8_t **input, uint32_t *mask, size_t *input_length) {
    // get {bits} bits from the subblock starting at idx_bits
    uint16_t code = 0;
    for (size_t i = 0; i < code_length + 1; i++) {
        uint8_t bit = (**input & *mask) ? 1 : 0;
        *mask <<= 1;

        if (*mask == 0x100) {
            *mask = 0x01;
            (*input)++;
            (*input_length)--;
        }
        code = code | (bit << i);
    }
    return code;
}

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }

    FILE *f = fopen(argv[1], "r");
    if (!f) {
        printf("Failed to open file: %s\n", argv[1]);
        return 1;
    }

    gif_header_t header;
    ssize_t nread = fread(&header, sizeof(gif_header_t), 1, f);

    if (nread < 10) {
        printf("Failed to read file: %s\n", argv[1]);
        return 1;
    }

    if (header.magic[0] != 'G' || header.magic[1] != 'I' || header.magic[2] != 'F'  ||
            header.magic[3] != '8' || (header.magic[4] != '7' && header.magic[4] != '9') ||
            header.magic[5] != 'a') {
        printf("File is not a GIF: %s\n", argv[1]);
        return 1;
    }

    uint8_t *img_buf;

    int fd = open("/dev/fb0", O_RDWR);
    if (fd != 0) {
        img_buf = mmap(NULL, 1920 * 1080 * sizeof(uint32_t), PROT_WRITE, MAP_SHARED, fd, 0);
    } else {
        img_buf = malloc(1920 * 1080 * sizeof(uint32_t));
    }

    uint8_t bit_depth = (header.flags & 0x07) + 1;
    bool has_gct = (header.flags & 0x80) != 0;

    uint8_t *frame_buf = malloc(1920 * 1080 * sizeof(uint32_t) * 20); // *should* be enough

    printf("Width: %d\n", header.width);
    printf("Height: %d\n", header.height);
    printf("Bit depth: %d\n", bit_depth);
    printf("Has GCT: %s\n", has_gct ? "yes" : "no");

    assert(has_gct && "Non-GCT GIFs are not supported");
    assert(bit_depth == 8 && "Only 8-bit GIFs are supported");

    uint16_t frame_delay = 0;
    (void)frame_delay;

    table_t table;
    uint8_t bg = header.bgcolor;
    (void)bg;

    for (;;) {
        uint8_t start_byte;
        nread = fread(&start_byte, 1, 1, f);
        if (nread < 1) {
            printf("Failed to read file: %s\n", argv[1]);
            return 1;
        }

        switch (start_byte) {
        case '!': {
            uint8_t block_type;
            fread(&block_type, 1, 1, f);

            for (;;) {
                uint8_t subblock_len;
                fread(&subblock_len, 1, 1, f);

                if (subblock_len == 0) {
                    break;
                }

                void *subblock = malloc(subblock_len);
                fread(subblock, subblock_len, 1, f);

                switch (block_type) {
                case 0xf9: {
//                    printf("Graphics control extension: ");
                    uint8_t flags = *(uint8_t *)subblock;
                    uint16_t delay = *(uint16_t *)(subblock + 1); // in 1/100ths of a second
                    (void)flags;
                    frame_delay = delay;
//                    printf("flags: %d\n", flags);
                    if (flags & 0x01) {
                        bg = *(uint8_t *)(subblock + 3);
                    } else {
                        bg = header.bgcolor;
                    }
//                    assert((flags & 0b11100) == 4 && "Disposal method not supported");
                } break;
                case 0xff: {
                    printf("Application extension: ");
                    write(1, subblock, subblock_len);
                    printf("\n");
                    // skip
                } break;
                default:
                    printf("Unknown block type: %d\n", block_type);
                    usleep(2000000);
                    break;
                }
    
                free(subblock);
            }
        } break;
        case ';': {
//            assert(false && "Trailing data not supported");
            goto out;
        } break;
        case ',': {
            char _buf[9];
            fread(_buf, 1, 9, f);
            uint16_t left = *(uint16_t *)_buf;
            uint16_t top = *(uint16_t *)(_buf + 2);
            uint16_t width = *(uint16_t *)(_buf + 4);
            uint16_t height = *(uint16_t *)(_buf + 6);
            (void)height;
            uint8_t flags = *(uint8_t *)(_buf + 8);
            /*printf("Left: %d\n", left);
            printf("Top: %d\n", top);
            printf("Width: %d\n", width);
            printf("Height: %d\n", height);
            printf("Flags: %d\n", flags);*/

            // clear with bg
            for (size_t x = 0; x < width; x++) {
                for (size_t y = 0; y < height; y++) {
                    ((uint32_t*)img_buf)[(top + y) * 1920 + (left + x)] = header.gct[bg].b | (header.gct[bg].g << 8) | (header.gct[bg].r << 16) | (0xff << 24);
                }
            }

            size_t buf_idx = top * 1920 + left;

            if (flags & (1 << 7)) {
                assert(false && "Local color table not supported");
            }
            if (flags & (1 << 6)) {
                assert(false && "Interlacing not supported");
            }
            if (flags & (1 << 5)) {
                assert(false && "Sort flag not supported");
            }

            // image start
            uint8_t lzw_min_code_size;
            fread(&lzw_min_code_size, 1, 1, f);
            assert(lzw_min_code_size == 8 && "Only 8-bit LZW is supported");
            size_t code_length = lzw_min_code_size;
            size_t table_idx = 0;

            for (table_idx = 0; table_idx < (1u << code_length); table_idx++) {
                table.entries[table_idx].byte = table_idx;
                table.entries[table_idx].prev = -1;
                table.entries[table_idx].len = 1;
            }
            table_idx++;
            table_idx++;

            size_t input_length = 0;
            size_t frame_buf_idx = 0;
            // subblocks
//            for (;;) {
            uint8_t subblock_len;
            fread(&subblock_len, 1, 1, f);

            if (subblock_len == 0) {
                break;
            }

            uint8_t *subblock = frame_buf + frame_buf_idx;
            fread(subblock, subblock_len, 1, f);
            frame_buf_idx += subblock_len;
//            }
            input_length = frame_buf_idx;

            uint16_t clear_code = 1 << (code_length);
            uint16_t stop_code = clear_code + 1;
            uint16_t reset_code_length = code_length;
            uint32_t mask = 0x01;

            uint8_t *input = frame_buf;

            bool read_all = false;
            size_t code_idx = 0;
            int prev = -1;
            int code = -1;
            while (input_length > 0) {
                code = next_code(code_length, &input, &mask, &input_length);
                //printf("%d/%d\n", (int)code, (int)prev);

                code_idx++;
                if (! read_all && input_length < 100) {
                    assert(input_length != 0);
                    uint8_t subblock_len;
                    fread(&subblock_len, 1, 1, f);

                    if (subblock_len == 0) {
                        read_all = true;
                    } else {
                        uint8_t *subblock = frame_buf + frame_buf_idx;
                        fread(subblock, subblock_len, 1, f);
                        frame_buf_idx += subblock_len;
                        input_length += subblock_len;
                    }
                }

//                printf("code: %d\n", code);
                if (code == clear_code) {
                    code_length = reset_code_length;
                    for (table_idx = 0; table_idx < (1u << code_length); table_idx++) {
                        table.entries[table_idx].byte = table_idx;
                        table.entries[table_idx].prev = -1;
                        table.entries[table_idx].len = 1;
                    } // reset

                    table_idx++;
                    table_idx++;
                    
                    assert(table_idx == 0x102 && "table_idx != 0x102");

                    prev = -1;
                    code_idx = 0;
                    continue;
                } else if (code == stop_code) {
                    if (input_length > 1) {
                        printf("malformed gif (early stop code)\n");
                        exit(1);
                    }
                    break;
                }

                if ( (prev > -1) && (code_length < 12 )) {
                    if (code > (int)table_idx) {
                        printf("malformed gif (code (%d) [nr. %d] > table_idx (%d)\n", code, (int)code_idx, (int)table_idx);
                        exit(1);
                    }

                    if (code == (int)table_idx) {
                        int ptr = prev;
                        while (table.entries[ptr].prev != -1) {
                            ptr = table.entries[ptr].prev;
                        }
                        table.entries[table_idx].byte = table.entries[ptr].byte;
                    } else {
                        int ptr = code;
                        while (table.entries[ptr].prev != -1) {
                            ptr = table.entries[ptr].prev;
                        }
                        table.entries[table_idx].byte = table.entries[ptr].byte;
                    }

                    table.entries[table_idx].prev = prev;
                    table.entries[table_idx].len = table.entries[prev].len + 1;
                    table_idx++;

                    if ((table_idx == (1u << (code_length + 1))) &&
                            (code_length < 11)) {
                        code_length++;
                    }
                }
                prev = code;
                
                output_string(code, &table, (uint32_t*)img_buf, &buf_idx, header, width, left);
            }
//            printf("delay: %d\n", frame_delay);
//            printf("img_buf_idx: %d\n", (int)img_buf_idx);
            syscall(SYS_render_buf, img_buf);
//            usleep(2000000);
        } break;
        default:
            printf("Unknown start byte: %d\n", start_byte);
            usleep(2000000);
            break;
        }
    }
    free(img_buf);
    free(frame_buf);
out:
    fclose(f);
    return 0;
}
