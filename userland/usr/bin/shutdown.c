#include <sys/syscall.h>

#ifndef __os_syscalls__
int main(void){return -1;}
#else
int main(void) {
    syscall(SYS_shutdown);
    __builtin_unreachable();
}
#endif
