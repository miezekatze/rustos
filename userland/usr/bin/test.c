#include <stdio.h>
#include <errno.h>

int main(void) {
    FILE *file = fopen("/usr/bin/test", "w");
    if (file == NULL) {
        perror("fopen");
        return 1;
    }

    if (fwrite("test", 4, 1, file) == 0) {
        perror("fwrite");
        return 1;
    }
    fclose(file);
    return 0;
}
