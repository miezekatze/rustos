#include <stddef.h>
#include <stdio.h>
#include <errno.h>

int main(int argc, char **argv) {
    const char *path = "/root/testfile";
    if (argc > 1) {
        path = argv[1];
    }

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        perror("fopen");
        return 1;
    }

    char cb[256];
    for (size_t i = 0;;i++) {
        if (i % 256 == 0) {
            int res = fread(cb, 256, 1, file);
            if (res == 0) {
                break;
            } else if (res < 0) {
                perror("fread");
                return 1;
            }
        }

        char c = cb[i % 256];
        if (c != (char)(i % 256)) {
            printf("Expected %d(%c), got %d(%c) at offset %d\n", (char)(i % 256), (char)(i % 256), c, c, (int)i);
            return 1;
        }
    }
}
