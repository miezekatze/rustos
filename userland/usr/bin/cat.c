#include <stdio.h>
#include <errno.h>

int main(int argc, char **argv) {
    FILE *file = stdin;
    if (argc > 1) {
        file = fopen(argv[1], "rb");
    }

    if (file == NULL) {
        perror("cat");
        return 1;
    }

#define BLOCK_SIZE 8192 // TODO: check if file does not support seeking, if yes
                        // use smaller buffer, because data will be read byte by
                        // byte
    char buf[BLOCK_SIZE];
    ssize_t len;
    while ((len = fread(buf, BLOCK_SIZE, 1, file)) > 0) {
        fwrite(buf, len, 1, stdout);
    }

    if (len < 0) {
        perror("read");
        return 1;
    }

    if (file != stdout) {
        fclose(file);
    }
}
