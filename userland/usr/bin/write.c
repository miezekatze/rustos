#include <stdio.h>
#include <errno.h>

int main() {
    FILE *file = fopen("/test/new.txt", "w");

    if (file == NULL) {
        perror("fopen");
        return 1;
    }

    if (fputs("Hello, World!\n", file) < 0) {
        perror("fputs");
        return 1;
    }

    if (fclose(file) < 0) {
        perror("fclose");
        return 1;
    }

    return 0;
}
