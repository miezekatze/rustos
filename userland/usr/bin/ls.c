#include <dirent.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    DIR *d;
    if (argc > 1) {
        d = opendir(argv[1]);
    } else {
        d = opendir(".");
    }

    if (d == NULL) {
        perror("ls: opendir");
        return 1;
    }

    struct dirent *dir;
    while ((dir = readdir(d)) != NULL) {
        printf("%s ", dir->d_name);
    }

    puts("");

    free(d);
    return 0;
}
