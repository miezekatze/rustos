#include "fcntl.h"
#include <sys/syscall.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#ifndef __os_syscalls__
int main(void) {
    puts("this program is not supported on this platform");
    return 1;
}
#else

int main(int argc, char **argv) {
    if (argc < 2) {
        puts("Usage: imgview <image>");
        return 1;
    }

    const char *filename = argv[1];
    static uint16_t buf[1920 * 1080];

    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        perror("graphics: fopen");
        return 1;
    }
    char *line = malloc(2);
    size_t n = 2;
    int nread = getline(&line, &n, file);

    if (nread != 3 || (line[0] != 'P' || line[1] != '6')) {
        printf("graphics: %s: Not a PPM (P6) file\n", filename);
        return 1;
    }

    nread = getline(&line, &n, file);
    int w = -1, h = -1;
    for (size_t i = 0; i < (size_t)nread; i++) {
        if (line[i] == ' ') {
            line[i] = 0;
            line[nread-1] = '\0';
            w = atoi(line);
            h = atoi(line + i + 1);
            break;
        }
    }

    if (w > 1920 || h > 1080) {
        printf("graphics: [%d x %d] out of bounds of [1920 x 1080] display\n", w, h);
        return 1;
    }

    // ingore next line
    getline(&line, &n, file);
    uint8_t *file_buf = malloc(w * h * 3);
    fread(file_buf, w * h * 3, 1, file);

    int x = 0;
    int y = 0;
    for (;x < w && y < h;) {
        size_t index = y * w + x;
        buf[y * 1920 + x] = ((file_buf[3 * index] & 0b11111000) << 8) |
            ((file_buf[3 * index + 1] & 0b11111100) << 3) |
            ((file_buf[3 * index + 2] & 0b11111000) >> 3);

        x++;
        if (x >= w) {
            x = 0;
            y++;
        }
    }

    syscall(SYS_render_buf, buf);
    return 0;
}

#endif

