#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/wait.h>

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;
    //    assert(argc > 0);

    char *string = malloc(20);
    sprintf(string, "TEST: %d\n", 42);
    puts(string);

    // for now, start /usr/bin/shell processes
    for (size_t i = 0; i < 12; i++) {
        pid_t pid = fork();
        if (pid < 0) {
            perror("init: fork");
        } else if (pid == 0) {
            // child
            char *nargv[] = {
                "/usr/bin/shell",
                NULL,
            };

            char *envp[] = {
                NULL
            };

            execve("/usr/bin/shell", nargv, envp);
            // only gets reached, if error occured
            perror("init: execve");
            return 1;
        } else {
            // parent
            wait(NULL);
            puts("[init] shell exited");
            return 1;
        }
    }
    
    return 0;
}
