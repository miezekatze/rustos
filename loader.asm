global entry

bits 32

%define VESA

section .multiboot_header
header_start:
    dd 0xe85250d6                ; magic number (multiboot 2)
    dd 0                         ; architecture 0 (protected mode i386)
    dd header_end - header_start ; header length
    ; checksum
    dd 0x100000000 - (0xe85250d6 + 0 + (header_end - header_start))

    align 8
    dw 1
    dw 0
    dd 12
    dd 8

    ;; align 8
    ;; dw 4
    ;; dw 0
    ;; dd 12
    ;; dd 8
%ifdef VESA
    align 8
    dw 5
    dw 0
    dd 20
    dd 1920
    dd 1080
    dd 32
%endif

    align 8
    dw 0    ; type
    dw 0    ; flags
    dd 8    ; size
header_end:

section .text
extern main
extern stack_top
extern stack_bottom
entry:
    mov esp, stack_top
    mov ebp, stack_top
    mov [boot_information_ptr], ebx
    add ebx, 8

    cmp eax, 0x36d76289
    jne .no_multiboot
    call read_tags

    jmp switch_long_mode
.no_multiboot:
    mov dword [0xb8000], 0x4f524f45
    hlt

switch_long_mode:
.check_cpuid:
    pushfd
    pop eax

    mov ecx, eax              ; save original flags
    xor eax, 1 << 21          ; flip ID bit, if flipped back, no cpuid

    push eax
    popfd

    pushfd
    pop eax                   ; eax is now flipped, if no cpuid

    push ecx                  ; restore flags
    popfd

    cmp eax, ecx
    je fatal

.check_longmode:
    mov eax, 0x80000000
    cpuid                       ; highest supported argument
    cmp eax, 0x80000001
    jl fatal

    mov eax, 0x80000001
    cpuid
    test edx, 1 << 29
    jz fatal

.setup_pagetable:
    mov eax, p4_table
    or eax, 0b11                ; present + write
    mov [p4_table + 511 * 8], eax

    mov eax, p3_table
    or eax, 0b11                ; present + write
    mov [p4_table], eax

    mov eax, p2_table
    or eax, 0b11                ; present + write
    mov [p3_table], eax

    mov ecx, 0
.map_p2:
    mov eax, 0x200000
    mul ecx
    or eax, 0b10000011          ; present + write + huge page
    mov [p2_table + ecx * 8], eax

    inc ecx
    cmp ecx, 512
    jne .map_p2

.enable_paging:
    mov eax, p4_table
    mov cr3, eax

    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax

    ; enable write protection
    mov eax, cr0
    or eax, 1 << 16
    mov cr0, eax

.switch:
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    or eax, 1 << 11 ; exe *cute* disable (XD)
    wrmsr

    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

.load_gdt:
    lgdt [gdt64.pointer]

    jmp gdt64.code:long_mode_start
    cli
    hlt

read_tags:
    mov eax, [ebx]

    mov edi, [elf_addr]
    cmp eax, 9
    cmove edi, ebx
    mov [elf_addr], edi

    mov edi, [mmap_addr]
    cmp eax, 6
    cmove edi, ebx
    mov [mmap_addr], edi

    cmp eax, 8
    je .fb_info
    test eax, eax
    je .type_zero
.align_and_continue:
    mov eax, [ebx]
    add ebx, [ebx + 4]

    mov ecx, ebx
    shr ecx, 3
    shl ecx, 3

    xor eax, eax
    mov edi, 8
    cmp ecx, ebx
    cmovne eax, edi
    add ecx, eax
    mov ebx, ecx

    jmp read_tags
.type_zero:
    mov eax, [ebx+4]
    cmp eax, 8
    je .done
    jmp .align_and_continue
.fb_info:
    mov eax, [ebx + 8] ; address lo
    mov [vesa.address], eax
    mov eax, [ebx + 12] ; address hi
    mov [vesa.address + 4], eax

    mov eax, [ebx + 16] ; pitch
    mov [vesa.pitch], eax

    mov eax, [ebx + 20] ; width
    mov [vesa.width], eax

    mov eax, [ebx + 24] ; height
    mov [vesa.height], eax

    mov eax, [ebx + 28] ; bpp
    mov [vesa.bpp], al

    mov eax, [ebx + 32] ; type
    mov [vesa.type], al

    jmp .align_and_continue
.done:
    ret

fatal:
    mov eax, [vesa.address]
.a:
    mov dword [eax], 0xffffffff
    add eax, 4
    jmp .a
    hlt

section .text
bits 64
long_mode_start:
    mov ax, 0
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

.enable_sse:
    mov rax, cr0
    and rax, ~(1 << 2)
    or rax, 1 << 1
    mov cr0, rax

    mov rax, cr4
    or rax, 1 << 9
    or rax, 1 << 10
    mov cr4, rax

.start_kernel:
    mov rdi, vesa
    mov rsi, [mmap_addr]
    mov rdx, [elf_addr]
    call main

    cli
    hlt
    
section .bss
    align 4096
p4_table:
    resb 4096
p3_table:
    resb 4096
p2_table:
    resb 4096
p2_table2:
    resb 4096
align 4096
stack_bottom:
    resb 327680
stack_top:
align 4096

section .data
boot_information_ptr:
    dd 0

align 8
vesa:
.address: dq 0
.pitch:  dd 0
.width:  dd 0
.height: dd 0
.bpp: db 0
.type: db 0

mmap_addr: dq 0
elf_addr: dq 0

section .rodata
gdt64:
    dq 0 ; zero entry
.code: equ $ - gdt64 
    dq (1<<43) | (1<<44) | (1<<47) | (1<<53) ; code segment
.pointer:
    dw $ - gdt64 - 1
    dq gdt64
