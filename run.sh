#!/bin/sh

set -x
sudo ./umount

./build.sh release && qemu-system-x86_64 img $(cat ./qemuopts | head -c -1) -accel kvm 
